name := "LectureDoc"

version := "0.0.0"

scalaVersion := "2.10.3"

scalacOptions in (Compile,compile) += "–deprecation"

scalacOptions in (Compile, compile) += "-target:jvm-1.7"

scalacOptions in Compile += "-feature"

libraryDependencies += "junit" % "junit" % "4.10" % "test"

libraryDependencies += "org.scalatest" % "scalatest_2.10" % "2.1.0" % "test"


// Less Settings
seq(lessSettings : _*)

// Adds generated CSS to managed resource to be packaged into .jar
(resourceGenerators in Compile) <+= (LessKeys.less in Compile)

// Add javafx
unmanagedJars in Compile += Attributed.blank( file(scala.util.Properties.javaHome) / "lib" / "jfxrt.jar")

// Adding javafx from Java 8 jre for backwards compatibility
unmanagedJars in Compile += Attributed.blank( file(scala.util.Properties.javaHome) / "lib" / "ext" / "jfxrt.jar")

fork in run := true

// Execute "less" task with "compile" task
(compile in Compile) <<= compile in Compile dependsOn (LessKeys.less in Compile)

// Source Directory for less files. If not specified, it appends the path after /src/main to the target directory
(sourceDirectory in (Compile, LessKeys.less)) <<= (sourceDirectory in Compile)( _ / "less" )

// Target Directory for compiled css files.
// WARNING: "sbt clean" deletes in some cases all files under /src/main/ + this path.
(resourceManaged in (Compile, LessKeys.less)) <<= (resourceManaged in Compile)( _ / "Library" / "LectureDoc" / "css")

// one-jar
seq(com.github.retronym.SbtOneJar.oneJarSettings: _*)

//set de.lecturedoc.tool.Launcher for the GUI version
//set de.lecturedoc.tool.LectureDoc for the commandline version
mainClass in oneJar := Some("de.lecturedoc.tools.Launcher")
