+~slide
#slideOut direction left
![Logo](Images/logo.png)
~+

+~[SlideOut Left]slide{transition:slideOut;duration:2000;direction:left}
#slideOut direction right
![Logo](Images/logo.png)
~+

+~[SlideOut Right]slide{transition:slideOut;duration:1000;direction:right}
#slideOut direction up
![Logo](Images/logo.png)
~+
+~[SlideOut Up]slide{transition:slideOut;duration:2000;direction:up}
#slideOut direction down
![Logo](Images/logo.png)
~+
+~[SlideOut Down]slide{transition:slideOut;duration:1000;direction:down}
#blendOver 1
![Logo](Images/DP-Decorator-AccountExample_1.png)
~+
+~[Blend Over 1]slide{transition:blendOver;duration:2000}
#blendOver 2
![Logo](Images/DP-Decorator-AccountExample_2.png)
~+
+~[Blend Over 2]slide{transition:blendOver;duration:2000}
#blendOver 3
![Logo](Images/DP-Decorator-AccountExample_3.png)
~+
+~[Dissolve]slide{transition:dissolve;duration:2000}
#dissolve
![Logo](Images/logo.png)
~+
