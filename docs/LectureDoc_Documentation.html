<!DOCTYPE html><html><head><meta charset='utf-8'><meta name=generator content='LectureDoc'><meta name='viewport' content='width=1024, user-scalable=yes, initial-scale=1.0'>

    <!-- START [HEADER] - Automatically inserted by LectureDoc in generated HTML files.  -->
    <script src="Library/Mousetrap-1.4.6/mousetrap.min.js" type="text/javascript"></script>
    <script src="Library/Mousetrap-1.4.6/mousetrap-pause.min.js" type="text/javascript"></script>
    <script src="Library/Gator-1.2.2/gator.min.js" type="text/javascript"></script>
    <script src="Library/Gator-1.2.2/gator-pause.js" type="text/javascript"></script>

    <script src="Library/Timeline-1.0.0/Timeline.js" type="text/javascript"></script>
    <link href="Library/Timeline-1.0.0/Timeline.css" type="text/css" rel="stylesheet">

    <script src="Library/MultipleChoice-1.0.0/MultipleChoice.js" type="text/javascript"></script>
    <link href="Library/MultipleChoice-1.0.0/MultipleChoice.css" type="text/css" rel="stylesheet">

    <script src="Library/SimpleJSEditor-1.1.0/SimpleJSEditor.js" type="text/javascript"></script>
    <link href="Library/SimpleJSEditor-1.1.0/SimpleJSEditor.css" type="text/css" rel="stylesheet" />

    <link rel="stylesheet" href="Library/highlight.js-8.0/styles/default.css">
    <script src="Library/highlight.js-8.0/highlight.pack.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>

    <link href="Library/LectureDoc/css/LectureDoc.css" type="text/css" rel="stylesheet">
    <script src="Library/LectureDoc/LectureDoc.js" type="text/javascript"></script>

    <script src="Library/MathJax-2.3/MathJax.js?config=TeX-AMS_HTML-full&delayStartupUntil=configured" type="text/javascript"></script>
    <script>
        LectureDoc.registerRenderCallBack(function(){
            MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
            SimpleJSEditor.create();
        });
        LectureDoc.registerSwitchModeCallBack(function(){
            SimpleJSEditor.saveState();
        });
    </script>
    <!-- END -->

</head><body data-ldjs-last-modified="1396528073177">
<div id='body-content'>
<h1 data-title="LectureDocDocumentation">LectureDoc Main Documentation</h1>
<p>Note: Throughout this document, LectureDoc is abbreviated as LD.</p>
<hr />
<h2>Overall architecture</h2>
<p>LD implements the MVC pattern as shown in the following figure.</p>
<p><img src="mvc.svg" alt="MVC" /></p>
<p>A typical update cycle works as follows:</p>
<ol>
<li>The user sends a signal to a UI component (e.g. presses a button).</li>
<li>Each UI component is associated with an Action.
  This signal triggers the <code>execute</code> function associated with this action.</li>
<li>This function may have several <code>update</code> calls.</li>
<li>Each <code>update</code> comes with a list of key-value pairs to override the internal state.</li>
<li>This list is validated, filtered and then merged into the current state.</li>
<li>The state notifies listeners that are interested in the changes applied to the state.
  Actions that are registered as listeners suppress notifications that do not affect the respective Actions.</li>
<li>Each listener may return a list of key-value pairs to change the state again.
  These lists are merged to a single list of new (pending) updates.</li>
<li>When all listeners have been notified about the applied changes to the state, the pending updates are applied for another update pass.
  This loop is repeated until no listener requests any changes to the state.</li>
</ol>
<hr />
<h2>Using the State object</h2>
<h3>Overview and persistency</h3>
<p>The State is an object that encapsulates variables necessary for LD to communicate with its environment, especially the UI and the web storage.
This set of variables can semantically be split into two parts called <code>sessionState</code> and <code>applicationState</code>.
The session state is volatile: it is lost every time the web browser discards the variables used in the script. (This usually happens if the web browser or its window is closed, for example.)
Furthermore, the current implementation uses the session state as a cache for miscellaneous data.</p>
<p>The application state is persistently stored using the web storage.
That is, LD saves the application state across multiple web browser sessions. 
The variables which belong to the application state are:</p>
<ul>
<li><code>mode</code></li>
<li><code>currentSlide</code></li>
<li><code>currentAnimationStep</code></li>
<li><code>doNotShowMenuHint</code></li>
<li><code>lightTableZoomFactor</code></li>
<li><code>presentationZoomFactor</code></li>
<li><code>isEnhancedPresentationModeActive</code></li>
<li><code>clientNotes</code></li>
<li><code>widgets</code></li>
</ul>
<p>Note that the state object itself does not make use of the web storage and, as a consequence, does not store any variables persistently.
In order to actually store the application state persistently, a method called <code>State.getApplicationState</code> is provided to create a copy of the current application state.
LD uses this copy to store the application state in the web storage.</p>
<p>Apart from their different volatility, both the session state and the application state work the same way.
That is, the programmer that uses the state object usually does not need to distinguish between these two disjoint subsets.</p>
<h3>Reading</h3>
<p>The current state can be read using the <code>State.get</code> function or the <code>State.getApplicationState</code> function.
<code>State.get(X)</code> returns the value of the variable identified by X.
<code>State.getApplicationState()</code> returns a copy of the application state as a Javascript object with the keys representing the identifiers of the variables and the values representing their respective values.
Modifying this object does not affect the application state.
Reading an unknown variable returns <code>undefined</code>.</p>
<h3>Writing</h3>
<p>The state can be modified using the <code>State.update</code> method.</p>
<p>If <code>State.update</code> is called, all state variables are overridden by their respective new values as determined by the object passed to the <code>update</code> function.
Variables that are not used in this object are not changed.
If a state variable is set to the same value it already has (according to <code>===</code>), it is not changed.
If the modification of a variable does not pass the test (that is, the <code>validate</code> function returns <code>false</code>), this variable is not changed.
Note that this might lead to an inconsistent state.
See <a href="#debugging">this section</a> to detect such cases.
If a variable is not found in the application state, it is assumed that it belongs to the session state.
That is, new variables are always put into the session state.</p>
<p>For the callers of <code>State</code> functions, all variables that have changed are set to their new values at once (atomically).</p>
<p>Thereafter, these changes are propagated to the listeners.
Only changes are considered when calculating which listeners will be notified.</p>
<p>Calling <code>update</code> while events are dispatched is deprecated as this might lead to an invalid state.
See <a href="#debugging">this section</a> to detect such cases.
Instead, the listener should return an object that reflects the requested changes the same way it would pass them to the <code>update</code> function.
Those pending updates are not visible to other listeners.
Instead, those pending updates are collected and then merged to one big update.
If this merged update set is empty, the update loop terminates.
Otherwise, the update process is repeated in a new update pass for this merged update set.</p>
<p>However, if two listeners request conflicting changes (i.e. the requested new values for a single variable are not equal according to <code>===</code>), the conflicting changes requested by the first listener will be overridden by the corresponding changes requested by the second listener.
This might also lead to an invalid state.
Again, see <a href="#debugging">this section</a> to detect such cases.</p>
<p>Calling <code>State.update({})</code> does not have any effect.</p>
<h3>Validation</h3>
<p>There is a function called <code>State.addConstraint</code> which can be used to install restrictions to state variables.
Validation takes place during an update pass before state listeners are notified about changes.
Each variable is validated individually and only if the value of the respective variable is about to be changed (according to <code>===</code>).</p>
<p>Currently, it is not necessary (and thus, not implemented) to validate state changes as a whole (i.e. across multiple variable changes).</p>
<h3>Installing listeners and how events are dispatched</h3>
<p>A functionality that is interested in changes to the state (e.g. Actions) can register listeners (observers).
A listener can be registered using the <code>State.addListener</code> method in one of two different ways with slightly different semantics:</p>
<ul>
<li><p>Along with a set of state variable identifiers. This set must be specified as an array, e.g. <code>State.addListener(aFunction, [&quot;anIdentifier&quot;, &quot;anotherIdentifier&quot;]);</code>
  A listener registered this way is notified (that is, <code>aFunction</code> is called) every time <code>State.update</code> is called and at least one of the variables specified has changed since the last update, if any.
  If the listener is interested in only one variable, the identifier of this variable can be specified directly as a string (i.e. no array is necessary in this case).</p>
</li>
<li><p>Without any set of state variable identifiers, e.g.</p>
<p><code>State.addListener(aFunction);</code></p>
</li>
</ul>
<p>  A listener registered this way is notified (that is, <code>aFunction</code> is called) every time <code>State.update()</code> is called and there is at least one variable that has been changed since the last update, if any.</p>
<p>All listeners registered to the state are notified only once per update regardless of how many variables they are interested in.
The current implementation guarantees a partial order in which listeners are notified:
If a listener <code>A</code> is registered before a listener <code>B</code>, then <code>A</code> will be notified before <code>B</code>.</p>
<h3>Exchange with other LectureDoc Instances</h3>
<p>The state can be updated from outside of LectureDoc via calls to <code>LectureDoc.updateState()</code> which uses <code>State.getExchangeState</code> to determine
which variables to exclude. For additional information exchange see <a href="#presentationwindow">this section</a></p>
<p>The variables that are currently excluded are:</p>
<ul>
<li><code>presentationZoomFactor</code></li>
<li><code>mode</code></li>
<li><code>isEnhancedPresentationModeActive</code></li>
<li><code>isMenubarVisible</code></li>
<li><code>isMarionette</code></li>
<li><code>isModalDialogActive</code></li>
<li><code>doNotShowMenuHint</code></li>
</ul>
<h3>Thread-safety</h3>
<p>The current implementation of the MVC pattern assumes that Javascript is single-threaded.
That is, thread safety precautions are neither necessary nor implemented.</p>
<hr />
<h2>Using Actions</h2>
<h3>Overview</h3>
<p>An Action encapsulates</p>
<ul>
<li>an ID,</li>
<li>the actual function which is called when the action is triggered, and</li>
<li>all features that are necessary to make this function available to the UI user (e.g., labels, icons, keystrokes).</li>
</ul>
<p>These features are usually determined by functions which calculate the actual values when called.
E.g., if you use</p>
<p><code>myActionBuilder.setVisible(function(){ return State.get(&quot;abc&quot;) == 2 });</code></p>
<p>the UI components associated with the action built from <code>myActionBuilder</code> are said to be visible to the user if and only if <code>State.get(&quot;abc&quot;)</code> returns <code>2</code>.
Since the value may change over time, an update mechanism has been implemented, see <a href="#actionupdate">this section</a> for details.</p>
<p>If an action is disabled, nothing will happen if the action&apos;s <code>execute</code> function is called.</p>
<h3>Creating new Actions</h3>
<p>In order to create an Action, you can make use of an <code>ActionBuilder</code> (as shown in the example) by calling its setters first and calling <code>buildAction()</code> afterwards.</p>
<p>Note that, instead of passing functions to the setters, you can also use constants.
For example, instead of</p>
<p><code>myActionBuilder.setEnabled(function(){ return true; });</code></p>
<p>you can simply write</p>
<p><code>myActionBuilder.setEnabled(true);</code></p>
<p>to make sure that this action will always be enabled.</p>
<h3>Installing Actions</h3>
<p>The created Action can be added to the UI depending on how and where this Action should be made available to the user.
For example, use <code>Events.bindKeys()</code> to make this Action available via keystrokes.</p>
<p>In the current implementation, the menu bar itself adds buttons to make its Actions available to the user.</p>
<h3><a name="actionupdate">Installing listeners and the updating mechanism for Actions</a></h3>
<p>You can register listeners to an Action in order to get notified about changes that affect this Action (e.g. <code>isEnabled</code> now returns <code>true</code> instead of <code>false</code>).</p>
<p>Since Actions may have values that change over time, an <code>update</code> function is provided.
You do not need to call this function explicitly since when an Action is created, it implicitly registers itself as a listener to the <code>State</code> object (and this listener calls this <code>update</code> function for you).
Similar to the <code>update</code> function of <code>State</code> object, this <code>update</code> function calls the listeners to this Action and automatically suppresses notifications about changes that are not actual changes (according to <code>===</code>).</p>
<p>That is, you do not need to know the state variables a specific Action depends on.
Instead, you can simply register listeners directly to the Action of interest.</p>
<hr />
<h2>Widgets</h2>
<h3>Overview</h3>
<p>An Widget encapsulates</p>
<ul>
<li>an ID,</li>
<li>the initialization function which is called when the widget is added to the document,</li>
<li>a parent element, to which the widget is added,</li>
<li>a position, and a</li>
<li>visibility</li>
</ul>
<h3>Adding new Widgets</h3>
<p>In order to create a Widget, you call <code>Widgets.addWidget</code> with parameters: id, initialize-Function and a parent element.</p>
<h3>Showing/Hiding Widgets</h3>
<p>The created Widget can be shown/hidden via calls to <code>Widgets.show(parent)</code> and <code>Widgets.hide(parent)</code> </p>
<p>You can call it with the immediate parent or any other parent above, up to window.document</p>
<p>A Widget is not removed from its parent when it is set to invisible.</p>
<h3>Drag and Drop</h3>
<p>To add Drag-and-Drop-functionality to a widget, call <code>Widgets.addDragAndDrop(id, dropArea)</code>, where id is the
ID specified in addWidget.
Note that dropArea can be a HTMLElement or an array of HTMLElements</p>
<hr />
<h2><a name="presentationwindow">PresentationWindow</a></h2>
<p>The PresentationWindow is technically a seperate instance of LectureDoc in a seperate window, but using the same HTML-Source.
It receives State-Updates from a Listener that is registered in the origin window.</p>
<p>To exchange data that is not kept in the State is send to the PresentationWindow via <code>LectureDoc.sendData()</code>
<code>PresentationWindow.acceptData</code> processes the known data, which is identified by a string, and ignores unknown data.</p>
<p>Every new feature that is not kept in the State, but should be tranferred to the PresentationWindow has to be send and processes accordingly.</p>
<p>Currently the following features are send:</p>
<ul>
<li>ASides</li>
<li>MultipleChoice-Questions</li>
<li>SimpleJS</li>
<li>Laserpointer</li>
<li>Canvas</li>
</ul>
<p>But <code>LectureDoc.sendData()</code> can also used for other purposes, like hiding the Presentation-Menu-Hint.</p>
<hr />
<h2>Initialization</h2>
<p>LD is initialized right after the DOM content has been (re-)loaded.
The current implementation takes four major steps in the following order to initialize LD,
after every step an event on the window is dispatched:</p>
<ul>
<li><p><code>analyzeDocument()</code></p>
<ul>
<li>The slides are decorated with automatically generated content. (Note: The current implementation simply paginates the document.)</li>
<li>The document is cloned for internal use.</li>
<li>The state object (&quot;state&quot;) is updated for the first time in order to incorporate the properties as implied by the document.</li>
<li>fires a <code>LDDocumentInitialized</code>-Event</li>
</ul>
</li>
<li><p><code>loadState()</code></p>
<ul>
<li>The state is updated using the currently stored locale storage data, if any.</li>
<li>The state is updated to incorporate the key-value pairs as specified by the URL.</li>
<li>An update mechanism is installed to keep the URL and the local storage in sync with the state.</li>
<li>fires a <code>LDStateInitialized</code>-Event</li>
</ul>
</li>
<li><p><code>setupConstraints()</code></p>
<ul>
<li>Constraints are set and installed to facilitate debugging.</li>
<li>fires a <code>LDConstraintsInitialized</code>-Event</li>
</ul>
</li>
<li><p><code>startUI()</code></p>
<ul>
<li>The event handling mechanisms (e.g. Mousetrap, Gator) are set up.</li>
<li>All UI components are initialized.</li>
<li>All predefined actions are installed (e.g. setting up key bindings).</li>
<li>fires a <code>LDUIInitialized</code>-Event</li>
</ul>
</li>
</ul>
<p><strong>After a complete initialization an <code>LDInitialized</code>-Event is fired.</strong></p>
<hr />
<h2><a name="debugging">Pitfalls, troubleshooting and the debug mode</a></h2>
<p>LD has a few built-in functions to facilitate debugging:</p>
<ul>
<li><code>log(something, loglevel)</code></li>
<li><code>debugString(key, oldValue, newValue)</code></li>
<li><code>stringify(object)</code></li>
</ul>
<p>The <code>log</code> function generates output if and only if a variable called <code>debug</code> is set to a value smaller than or equal to the parameter <code>loglevel</code>.
If <code>loglevel</code> is not specified, it is implicitly set to <code>debug_INFO</code>.
There are several constants defined in LD to describe the different levels of importance of a debug message:</p>
<ul>
<li><code>debug_OFF = 0</code></li>
<li><code>debug_ERROR = -1</code></li>
<li><code>debug_WARNING = -2</code></li>
<li><code>debug_INFO = -3</code></li>
<li><code>debug_TRACE = -4</code></li>
<li><code>debug_ALL = -2147483648</code></li>
</ul>
<p>If quite complex code is necessary to generate a useful debug message or if you want to install listeners only for debugging purposes, you should use an appropriate <code>if</code>-statement around it, e.g.:</p>
<p><code>if (debug &amp;lt;= debug_WARNING) { /* code to generate a warning */ }</code></p>
<p>LD already makes use of the <code>log</code> function, whereas the most important log lines inform you about the following events.</p>
<ul>
<li><p>A <code>debug_ERROR</code> message is generated in the following cases:</p>
<ul>
<li>If you attempt to read a variable that does not exist in the state, the result will be <code>undefined</code>.
  Make sure that you did not accidently misspell variables.</li>
<li>If validation failed for a specific variable, this variable is not updated and an appropriate message appears in the logs.</li>
</ul>
</li>
<li><p>A <code>debug_WARNING</code> message is generated in the following cases:</p>
<ul>
<li>Most likely, calling <code>update</code> within an update is a bug and might result in an undefined or illegal state.
  Both updates will be performed, though.</li>
<li>If you update the state with new variables, these variables are incorporated into the session state. Watch out for such messages because you might have accidently added misspelled variables.</li>
<li>Two listeners request conflicting changes.</li>
</ul>
</li>
<li><p>A <code>debug_INFO</code> message is generated in the following cases:</p>
<ul>
<li>A constraint is registered.</li>
<li>Listeners are notified.</li>
<li>Listeners request changes.</li>
<li>The update section is being entered.</li>
<li>The number of the current update pass is logged.</li>
<li>A variable of the application state is updated.</li>
<li>A variable of the session state is updated.</li>
<li>If a listener wants to update the state, it must return an appropriate object in order to enqueue the requested changes.
  Note, however, that this might result in an infinite loop if two listeners mutually trigger themselves.
  Therefore, it is always a good idea to keep the number of update passes per <code>update</code> call as low as possible.
  The number of update passes is logged each time the update section is left.</li>
<li>A property of an action has changed (i.e. its value is not equal to the prior one according to <code>===</code>).</li>
</ul>
</li>
<li><p>A <code>debug_TRACE</code> message is generated in the following cases:</p>
<ul>
<li>Changing the value of a variable triggers validation.</li>
<li>A variable of the application state is read.</li>
<li>A variable of the session state is read.</li>
<li>An update of a variable is suppressed because its new value is equal to the prior one (according to <code>===</code>).</li>
<li>An Action is about to notify its listeners about changes to its properties.</li>
<li>An Action suppresses an update for one of its properties because the new value is equal to the prior one (according to <code>===</code>).</li>
<li>Two listeners request another update pass and they both request the same new value for a single variable (according to <code>===</code>).</li>
</ul>
</li>
</ul>
<p>If <code>debug</code> does not evaluate to a value equivalent to <code>false</code>, an appropriate message will appear just before the actual initialization of LD.<br />
<strong>Make sure that debugging is turned off when you share your documents or start a lecture.
Otherwise, this message will reappear, lagging might occur and other inventive debugging messages might pop up when it is not appropriate.</strong></p>
<hr />
<h2>Third-party software</h2>
<h3>jsdoc</h3>
<p><code>jsdoc</code> extracts and compiles the comments for API documentation.
See the official websites for details:
<a href="https://github.com/jsdoc3/jsdoc">https://github.com/jsdoc3/jsdoc</a> and <a href="http://usejsdoc.org/">http://usejsdoc.org/</a>
The most recent version used here is:</p>
<p><code>Version: JSDoc 3.2.0-dev (Fri, 19 Apr 2013 22:04:23 GMT)</code></p>
<p>In this version of <code>jsdoc</code>, error messages generated by <code>jsdoc</code> are sometimes difficult to read.
Note that, if you find line numbers in said messages, those line numbes usually refer to the code of <code>jsdoc</code> itself (pointing to the piece of code that is confused by your input) rather than the line numbers of the code you asked it to extract the documentation from.</p>
<p>If you want to use characters that are used by <code>jsdoc</code>, you should use escape sequences.
The most interesting characters here are <code>{</code> and <code>}</code> which can be escaped using <code>&amp;amp;#123;</code> and <code>&amp;amp;#125;</code>, respectively.</p>
<p>For some unknown reason, <code>jsdoc</code> does not accept an array of arrays as a type when written as <code>[][]</code>.
Because of that, all arrays (even one-dimensional ones) are written as <code>Array&amp;lt;…&amp;gt;</code>, e.g. <code>Array&amp;lt;Array&amp;lt;Number&amp;gt;&amp;gt;</code> instead of <code>Number[][]</code>.</p>
<p>Another issue is that <code>@memberof</code> sometimes works only if it is not used in a single-line comment.
That is, you need write</p>
<p><code>/** </code><br />
<code> * @memberof SomeMember</code><br />
<code> */</code></p>
<p>instead of</p>
<p><code>/** @memberof SomeMember */</code></p>
<p>It is unknown whether this applies only to this tag or other tags might be affected as well.
This bug might also have been fixed in the meantime.</p>
<p>For the current version of LD, <code>jsdoc</code> is called this way (working directory must be the one where the <code>src/</code> directory is located):</p>
<p><code>jsdoc -p -d docs/jsdoc/ -r src/</code></p>
<p>This ensures that private members are taken into account, the output directory is <code>docs/jsdoc/</code> and the source code to use are all Javascript files located in <code>src/</code> (recursively).
See <code>jsdoc -h</code> for details.</p>
<h3>js-beautify</h3>
<p><code>js-beautify</code> is the beautifier for Javascript used for the Javascript code of LD.
See the official website for details:
<a href="https://github.com/einars/js-beautify">https://github.com/einars/js-beautify</a>
The most recent version used here is:</p>
<p><code>Version: 1.4.2</code></p>
<p>In order to make sure that <code>js-beautify</code> does not accidentally scratch your work, you should make a backup of your code before you run the beautifier.
The following bash script automatically creates a backup and restores the files if something happens.
Since it is a bash script, it needs an appropriate environment (e.g. GNU/Linux or Cygwin) to work.
The working directory should be the one where the <code>src/</code> directory is located.
Otherwise, you need to modify this script accordingly.</p>
<pre><code>#!/bin/bash

function beautify {
    mkdir -p &quot;$JSBACKUPDIR&quot;
    while [ &quot;$1&quot; != &quot;&quot; ] ; do
        if [ -f &quot;$1&quot; ] ; then
            echo &quot;copying $1&quot;
            cp -a --parents &quot;$1&quot; &quot;$JSBACKUPDIR&quot;
            if [ &quot;$?&quot; != &quot;0&quot; ] ; then
                echo &quot;could not create a copy, $1 skipped&quot;
            else
                echo &quot;beautifying $1&quot;
                # using -o doesn&apos;t work, so we must redirect stdout
                js-beautify -j -k -s 4 -w 90 &quot;$JSBACKUPDIR/$1&quot; &gt; &quot;$1&quot;
                if [ &quot;$?&quot; != &quot;0&quot; ] ; then
                    echo &quot;something went wrong during beautifying, reverting $1&quot;
                    cp -a &quot;$JSBACKUPDIR/$1&quot; &quot;$1&quot;
                fi
            fi
        else
            echo &quot;$1 is not a regular file, skipped&quot;
        fi
        shift
    done
}

JSBACKUPDIR=&quot;js-beautify_BAK&quot;

beautify \
&quot;src/main/resources/Library/LectureDoc/LectureDoc-Help.js&quot; \
&quot;src/main/resources/Library/LectureDoc/LectureDoc.js&quot; \
&quot;src/main/resources/Library/Timeline-1.0.0/Timeline.js&quot; \
&quot;src/main/resources/Library/MultipleChoice-1.0.0/MultipleChoice.js&quot; \
&quot;&quot;
</code></pre>
<h3>Mousetrap</h3>
<p><code>Mousetrap</code> is a library for simplified, cross-browser handling of keyboard events. See the official website for details: <a href="http://craig.is/killing/mice">http://craig.is/killing/mice</a>. The most recent version used here is:</p>
<p><code>Version: 1.4.6</code></p>
<p><code>Mousetrap</code> is used to handle keyboard shortcuts that are defined by actions. The primary reason to integrate it was its built-in support for the <code>meta</code> key, which equals <code>ctrl</code> on Windows and Linux and <code>cmd</code> on Mac OS.</p>
<h3>Gator</h3>
<p><code>Gator</code> is a general purpose event dispatching library. See the official website for details: <a href="http://craig.is/riding/gators">http://craig.is/riding/gators</a>. The most recent version used here is:</p>
<p><code>Version: 1.2.2</code></p>
<p><code>Gator</code> is used mostly to reduce the number of click listeners in the application. Secondarily, <code>Gator</code> is used to handle several special cases of keyboard input that cannot easily be covered using <code>Mousetrap</code>.</p>
<p>Example: jumping to another slide by entering its number followed by the enter key requires capturing a dynamic key sequence. While it would be possible to implement the same behavior using an action and Mousetrap shortcuts, usage of the enter key as &quot;next slide&quot; when no slide number has been entered would have to be merged into that code.</p>
<p>Second example: triggering modes via shortcuts m+Letter is not possible via <code>Mousetrap</code> when at the same time m should trigger the modes sub menu. That is because <code>Mousetrap</code> cannot handle shortcuts that have a &quot;normal&quot; action and are the start of a static keyboard sequence.<br />
Setting the mode actions to enabled only if the modes sub menu is open and setting their shortcuts to the respective letter <code>(p,d,n,l,c)</code> would work for all cases except l, since the l key is already bound to the &quot;last slide&quot; action. Therefore it has been decided to implement the mode triggering second key with <code>Gator</code>.</p>
<h3>MathJax</h3>
<p><code>MathJax</code> is a cross-browser library for displaying mathematical notation in web browsers, using MathML, LaTeX and ASCIIMathML markup. See the official website for details: <a href="http://www.mathjax.org/">http://www.mathjax.org/</a>. The most recent version used here is:</p>
<p><code>Version: 2.3</code></p>
<p>Within the scope of LD <code>MathJax</code> is configured to allow specifying math in TeX or LaTeX notation (indicated by math delimiters like <code>$$ ... $$</code>). <code>MathJax</code> purpose is displaying math, therefore it only supports the subsets of TeX or LaTeX used to describe mathematical notation.</p>
<h3>Highlight.js</h3>
<p><code>Highlight.js</code> is a cross-browser library for highlighting syntax in code examples on web pages. See the official websites for details: <a href="https://github.com/isagalaev/highlight.js">https://github.com/isagalaev/highlight.js</a> and <a href="http://highlightjs.org/">http://highlightjs.org/</a>. The most recent version used here is:</p>
<p><code>Version: 8.0</code></p>
<p><code>Highlight.js</code> is used to highlight syntax within <code>&lt;code&gt;...&lt;/code&gt;</code> fragments. By specifying an optional language class after the start of a fencedCode definition marked by <code>``</code> in markdown documents, it is possible to set a code fragment&apos;s language explicitly. As recommended in the <a href="http://www.whatwg.org/specs/web-apps/current-work/multipage/text-level-semantics.html#the-code-element">HTML Living Standard</a> those language tokens should be prefixed with &quot;language-&quot;. A list of supported languages including their keywords can be found at <code>src/main/resources/Library/highlight.js-8.0/css-classes-reference.rst</code>.</p>
<p>First Example: Parsing<br /></p>
<code>``language-java<br>
...<br>
``
</code>
<p>would result in <code>&lt;pre&gt;&lt;code class=&quot;language-java&quot;&gt;...&lt;/code&gt;&lt;/pre&gt;</code>. In this case, <code>Highlight.js</code> would highlight the code fragment using <code>Java</code> syntax. To disable highlighting of a fragment altogether use &quot;no-highlight&quot;.</p>
<p>If no language class is specified, <code>Highlight.js</code> automatically finds blocks of code, detects their language based on heuristics and highlights them.</p>
<hr />
<p>Last Modified: 2014-03-23 16:24:00 CET<br />
Authors: Daniel Killer, Arne Lottmann, Tobias Becker, David Becker</p>
</div>
    <!-- START [FOOTER] - Automatically inserted by LectureDoc in generated HTML files.  -->
    <script type="text/javascript">SimpleJSEditor.create()</script>
    <script type="text/javascript">MathJax.Hub.Configured()</script>
    <script src="Library/LectureDoc/LectureDoc-Help.js" type="text/javascript"></script>

    <!-- END -->

</body></html>
