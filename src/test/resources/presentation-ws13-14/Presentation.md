﻿
# Einleitung #
## Team ##
+~slide
+~header
LectureDoc 
===
Bachelorpraktikum
---

~+

---

**Wintersemester 2013/2014**
                        
**Fachbereich Informatik**  
**Technische Universität Darmstadt**

David Becker  
**Tobias Becker**  
Andre Pacak  
Volkan Hacimüftüoglu

~+
## Projekt ##
+~[Präsentation LectureDoc]slide

Eine kurze Einführung  
===  

^*Hauptziel:* Folien und Skript aus **einem** Quelldokument 

* LectureDoc wurde Anfang 2013 von Dr. Michael Eichberg begonnen
* Primär für Vorlesungen gedacht, eignet es sich aber auch für Präsentationen
* Besteht aus 3 Teilen


~+

+~[Teil 1 - Markdown]slide

Eine kurze Einführung  
===  
Teil 1 - Markdown
--
* Dokument wird in einem Markdown-Dialekt erstellt
* So sieht die Seite in Markdown aus:

		+~[Teil 1 - Markdown]slide
		Eine kurze Einführung  
		===  
		Teil 1 - Markdown  
		--  
		* Dokument wird in einem Markdown-Dialekt erstellt
		~+

~+

+~[Teil 2 - Parser]slide

Eine kurze Einführung  
===  
Teil 2 - Parser
--
* geschrieben in Scala
* transformiert das Markdown-Dokument in HTML5
* nochmal die vorherige Seite, diesmal in HTML5:
		<section class="slide" data-title="Teil 1 - Markdown">
		<div class="section-body">
		<h1>Eine kurze Einführung</h1>
		<h2>Teil 1 - Markdown</h2>
		<ul>
		<li>Dokument wird in einem Markdown-Dialekt erstellt</li>
		<li><p>So sieht die Seite in Markdown aus:</p>
		</li>
		</ul>
		</div></section>
~+

+~[Teil 3 - Javascript und CSS]slide{transition:dissolve;time:500}

Eine kurze Einführung  
===  
Teil 3 - Javascript und CSS
--
* stellt verschiedene Ansichtsmodi zur Verfügung
* eine Druckfunktion
* diverse Funktionen zur Präsentation:
* Laserpointer
* Stift zum Zeichnen
+~[Example Aside]aside
* **Asides** Um zusätzliche Informationen einzublenden
~+
~+
## Aufgaben ##
+~[Unser Part]slide

Eine kurze Einführung  
===  
Unser Teil
--
**Bisher:**
<li class="animatedElement"> Sequentielles Zuschalten von Elementen</li>
<li class="animatedElement"> Table of Contents</li>
<li class="animatedElement"> Folientransitionen</li>
<li class="animatedElement"> Unterstützung von mobilen Geräten</li>
<div class="animatedElement">
<hr>
<strong>In Bearbeitung:</strong>
<ul>
<li> 2-Bildschirmmodus </li>
<li> Alle Features auf mobilen Geräten unterstützen</li>
<li> Backend für Folientransitionen anpassen</li>
<li> Userguide zur Erstellung von Markdown-Datien</li>
<ul>
</div>
~+
# Qualitätssicherung #
+~[QS-Ziele]slide

Qualitätssicherungs-Ziele
===  
 
---  
<br>
#1. Funktionalität  
<br>
#2. Code Qualität
~+
## Funktionalität ##
+~[QS-Ziele]slide

Qualitätssicherungs-Ziele
===
1. Funktionalität
---
**Kein Fokus auf viele Features**  
**Aber: Alle Vorhandenen müssen weiterhin funktionieren, da LectureDoc aktiv verwendet wird**

Zielplattformen
* Browser(Frontend): Chrome / Firefox / Safari 
* Backend: Windows 7 / Mac OS / Linux 

Maßnahmen:
* Regelmäßige manuelle Tests mittels Testplänen
* Erweitern der Testpläne um unsere neuen Features

~+
## Code Qualität ##
+~[QS-Ziele]slide

Qualitätssicherungs-Ziele
===
2. Code Qualität
---

**Die Entwicklung wird auch nach dem Bachelorpraktikum weitergehen.**
**Außerdem wird es aktuell in Vorlesungen eingesetzt.**
**Der Code muss daher wartbar und dokumentiert sein.**

Maßnahmen:

* Code Reviews am Ende jeder Iteration mittels fester Checkliste
* Konsistente Benennung von Variablen etc.
* Formatierung  des Codes durch JSBeautifier
* Dokumentation wird aktuell gehalten

~+
# Arbeitsauswertung #
+~[Zahlen]slide
Zahlen & Fakten
===
* Aktuelle Iteration: 5
* Gesamtaufwand: **382,75** Stunden
* Davon:
* * Sonstiges: 179,55 Stunden  
* * UserStories: 203,2 Stunden

<img src="us-details.png" title="User Stories im Detail" style="float:center">
~+
+~[Stunden pro Person]slide

<img src="h-pro-pers.png" title="Stunden pro Person" style="float:center">

~+


+~[Stunden pro Iteration]slide

<img src="h-pro-iteration.png" title="Stunden pro Iteration" style="float:center">

~+

+~[1. Iteration]slide
1. Iteration
===

User-Stories der 1. Iteration:
* Wischgesten auf mobilen Endgeräten
* Sequentielles Zuschalten von Slide-Inhalten
* Folientransition
* Machbarkeit von Aufzeichnungen 

<table><tr><td>Tatsächliche Entwicklung:</td> 				<td>47,2 Stunden</td></tr>  
<tr><td>Einarbeitung in vorhandenen Code/Javascript:</td>	<td>31,1 Stunden</td>  </tr>
<tr><td>Organisation:</td> 									<td>16,2 Stunden</td> </tr>
<tr style="border-top:2px solid; margin:0px;"><td></td>		<td>**94,5 Stunden**</td></tr></table>
~+


+~[Fragen]slide

^**Noch Fragen?**

<img src="NochFragen.png" title="Noch Fragen?" style="float:center">

~+

+~[Vielen Dank]slide

^**Vielen Dank für Ihre Aufmerksamkeit**

~+