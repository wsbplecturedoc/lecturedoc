#LectureDoc

* * *

**Qualitätssicherungsdokument**

<dl class="as-table">
<dt>Gruppe 22:</dt>
<dd>Daniel Killer &lt;<a href="mailto:daniel.killer@stud.tu-darmstadt.de">daniel.killer@stud.tu-darmstadt.de</a>&gt;</dd>
<dd>Arne Lottmann &lt;<a href="mailto:arne.lottmann@stud.tu-darmstadt.de">arne.lottmann@stud.tu-darmstadt.de</a>&gt;</dd>
<dd>Kerstin Reifschläger &lt;<a href="mailto:kerstin.reifschlaeger@stud.tu-darmstadt.de">kerstin.reifschlaeger@stud.tu-darmstadt.de</a>&gt;</dd>
<dd>Simone Wälde &lt;<a href="mailto:simone.waelde@stud.tu-darmstadt.de">simone.waelde@stud.tu-darmstadt.de</a>&gt;</dd>
<dt>Teamleiter:</dt>
<dd>Henriette Röger &lt;<a href="mailto:h_roeger@rbg.informatik.tu-darmstadt.de">h_roeger@rbg.informatik.tu-darmstadt.de</a>&gt;</dd>
<dt>Auftraggeber:</dt>
<dd>Dr. Michael Eichberg &lt;<a href="mailto:eichberg@st.informatik.tu-darmstadt.de">eichberg@st.informatik.tu-darmstadt.de</a>&gt;</dd>
<dd>Fachgebiet Softwaretechnik</dd>
<dd>Fachbereich Informatik</dd>
<dt>Abgabedatum:</dt>
<dd>06.09.2013</dd>
</dl>

* * *

+~[Logo]frontpagelogo

![Athene](Athene.png)

<small>Bachelor-Praktikum SoSe 2013</small>  
<small>Fachbereich Informatik</small>

~+

+~page

##Table of Contents

1. Introduction <span class="align-right">2</span>

2. Quality Ensurance <span class="align-right">3</span>

	2.1. Goal: Portability <span class="align-right">4</span>

	2.2. Goal: Code Quality <span class="align-right">7</span>

3. Appendix <span class="align-right">9</span>

~+

+~page

##1. Introduction

[LectureDoc][0] was created in early 2013 by our customer Dr. Eichberg, lecturer in Software Engineering at TU Darmstadt.
His motivation was to create a system to produce documents that combine lecture slides and a lecture script within a single file.
These different kinds of information could then be accessed through different display modes.
An example of LectureDoc in active use can be seen in his current lecture [Software Engineering: Design and Construction][1].

At its core, LectureDoc consists of three separate parts.
Part one is a [Markdown][2] dialect in which the content is written.
Part two is a parser, written in [Scala][3], that transforms Markdown content into valid HTML5.
Part three consists of several JavaScript modules and CSS files that provide the documents' user interface.
The user interface offers several display modes specialized for presenting, printing and showing additional information.

LectureDoc achieves ease of use by extending Markdown, which is a lightweight markup language.
It adds syntax elements to define HTML5 section elements and to attribute them with CSS classes.
This syntax extension allows a person creating slides to place additional, e.g. script-like, information between slides.
Such information will only be displayed in some display modes.

In the scope of the Bachelorpraktikum, it is our main task to design and implement modifications to the front end with the goal to improve usability and modernize LectureDoc's visual appearance.
Therefore we have to adapt LectureDoc's HTML5, CSS and JavaScript front end which has to be compliant with standards implemented by the majority of modern web browsers. 
Furthermore, part of our work will be reused later on in the further development of LectureDoc.
~+

+~page

##2. Quality Assurance

####Overview Goals

The first quality goal our customer wants to be met is portability, since LectureDoc has to run on a well-defined set of operating systems and browsers.
The second goal is to deliver high quality and easily maintainable code, since work on LectureDoc will continue after our project ends.

To ensure these two goals are met, we perform a quality assurance process that takes place at the end of each iteration.

####Overview QA Process

Every user story whose implementation is declared to be finished by its assigned developer(s) undergoes the testing and review processes.
Only then it is considered to be presented to the customer.

Testing of a user story must be completed, at the latest, two days before the next customer meeting to have enough time for the following code review process.
This review process must be completed, at the latest, on the day before the next customer meeting.
A user story whose implementation did not pass its tests or the review process in time will be deferred until the next iteration.
Neither testing nor reviewing happens on the day of the customer meeting.

Exceptions to this are only allowed after explicit coordination between the user story's developer(s) and its designated tester/reviewer, and cannot take place on the day of a customer meeting.

The actual testing and review processes as well as the goals they shall ensure, are detailed in the following subsections.

~+

+~page

###2.1 Goal: Portability

The customer uses LectureDoc for presenting information in lectures and providing a script and slides for the students.
Since students may use different browsers and operating systems the customer wants to ensure portability of LectureDoc.
The LectureDoc user interface is implemented in compliance with modern web standards for HTML5, CSS (versions 1, 2 and 3), and JavaScript.
In addition to already adopted standards, we use standard proposals that are widely supported by modern web browsers to achieve portability.

In this context, "modern web browsers" refers to Mozilla Firefox, Google Chrome, and Safari; the supported versions are the respective most recent stable releases until August 18th, 2013. That are:

* Mozilla Firefox 23.0.1
* Google Chrome 28.0.1500.95
* Safari 6.0.5

Since we use only adopted standards and standard proposals, it is likely that other modern browsers (e.g version 10 of the Microsoft Internet Explorer) will be able to correctly render the LectureDoc user interface.
However, the above named three browsers are the only ones that we test the user interface on.

~+

+~page

####QA Process

To ensure the correct operation and integration of our implemented user stories, we define manual test plans for the interface.

A test plan consists of a list of operations and expected results of those operations.
The tester executes the operations and notes down if the observed result matches the expectation.
Each test plan reflects the requirements laid down in its corresponding user story's acceptance criteria and description.
There is one test plan for every user story under development.
Each test plan considers every platform our customer wants to be supported in order to ensure portability.
This means that each test operation must have the same result on all tested browsers across all tested operating systems.
Sensible exceptions to this point (e.g. the use of the Cmd key instead of the Ctrl key on Mac OS) are mentioned in the description of the operation.

For the testing of a user story, one team member other than the user story's developer(s) is chosen as the tester.
The tester, together with at least one other team member, then creates an individual test plan for the  user story consisting of the acceptance criteria and expected behavior.
The tester executes the test plan and notes down the individual operations' successes and/or failures.
If there are operations the user story's implementation did not pass, the tester adds a remark to the test plan detailing the problems.
If a user story's implementation passes all its test, it is put up for code review.
If not, the team, and in particular the story's developer, are notified and the filled-out test plan is made available to them.
With these results at hand, any team member other than the tester may work on fixing the identified problems.
If a team member that is not the tester provides fixes, the testing process of the user story begins anew at the start of this paragraph.
If no fixes are provided in time, the user story will be deferred until the next iteration.

Test documents are provided in the project directory `src/test/resource/sample-docs`. Each test plan states the documents for which it has to be executed.
Usually test plans are executed for the following documents:

* `Slides_all_features.md`
	This document contains all features offered by LectureDoc, including slides.
* `Empty_doc.md`
	This document is completely empty.
* `Md_doc_all_features.md`
	This document contains all features offered by LectureDoc except slides.

The documents, along with the folder `src/main/resources/Library`, are copied to a temporary location prior to testing.
There, the documents are converted to HTML with an up-to-date version of the LectureDoc parser.

~+

+~page

Here is an excerpt of a test plan:

<img src="testplan_excerpt_big.png" width="650">

~+

+~page

###2.2 Goal: Code Quality

Since development on LectureDoc will continue after the Bachelorpraktikum concludes, the customer requires that we deliver high quality code, i.e. code that is well structured and easily readable, and thus easily maintainable.

A first step to obtain easily readable code is to ensure consistent, clear formatting.
To that end we use the code formatting tool [JS Beautifier][4] and the code formatter from the [Eclipse plugin for LESS][5].

We use the following [command line options][6] for JS Beautifier:

	-j -k -s 4 -w 90

We use the default settings for the code formatter from the Eclipse plugin for LESS.

As a second step we use JavaScript's strict mode to prevent common JavaScript bugs.

Finally, we perform code reviews to ensure that our code meets the following set of quality criteria.

Javascript:

* All code is placed inside of the LectureDoc namespace
* Every statement is terminated with a semicolon
* Identifiers are named consistently with CSS classes/ids
* Generated elements have id prefix ldjs
* Generated elements only have id/class if necessary
* Abbreviations are spelled out
* Comments are up to date
* Function parameters are documented
* Each functions has one well defined responsibility

CSS/LESS:

* Classes/ids are named consistently with JS identifier
* Class/id values reflect the element's responsibility
* Selectors are logically and structurally grouped
* Indentation is consistent
* One statement per line
* Single whitespace after colons in style statements
* All styles are used
* Only CSS 1/2/3 standards/standard proposals supported and treated equally by all target browsers are used
* Prefixes for standard proposals (as required): -moz, -webkit and without prefix (if, for example, Firefox already adopts the prefixless notation, -moz is not required)

~+

+~page

####QA Process

Each team member can choose from all tested user stories that have not yet been reviewed and in whose development they have not participated.
That team member works through the checklist and notes down all locations within the reviewed source files that are not in alignment with the criteria defined in the checklist.
If all files of the user story pass the review, the user story is ready to be submitted to the customer.
Otherwise it is passed back to its developer along with the filled out code review checklist, so that the developer may fix the problems.
If a team member that is not the reviewer provides fixes, the testing process of the user story begins anew at the start of this paragraph.
If no fixes are provided in time, the user story is deferred until the next iteration.
This process continues until each implemented user story is reviewed.

~+

+~page

##3. Appendix

~+

[0]: http://bitbucket.org/delors/lecturedoc
[1]: http://mms.se.informatik.tu-darmstadt.de/sedc/index.html
[2]: http://daringfireball.net/projects/markdown
[3]: http://scala-lang.org
[4]: http://jsbeautifier.org
[5]: http://www.normalesup.org/~simonet/soft/ow/eclipse-less.en.html
[6]: https://github.com/einars/js-beautify#options
