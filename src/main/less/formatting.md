##Code Formatting

Less files are formatted with the code formatter that is shipped with the
[Eclipse plugin for LESS][1]. The default settings are used.

[2]: http://www.normalesup.org/~simonet/soft/ow/eclipse-less.en.html
