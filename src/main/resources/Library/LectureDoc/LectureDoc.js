/*
 *   Copyright 2012 Michael Eichberg et al - www.michael-eichberg.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
/*
 * The following script turns a browser in a fully-fledged presentation
 * program (if the browser has a presentation mode/a chromeless mode).
 * The basic idea is that we render a standard HTML document that uses some
 * predefined CSS classes as a set of slides.
 *
 * The idea is that all SECTION elements where the CLASS attribute contains
 * "slide" are rendered as single slides and the user can change the slides
 * using the standard controls (left, right keys etc.) known from other
 * presentation programs.
 *
 * @author Michael Eichberg
 * @author Marco Jacobasch
 * @author Arne Lottmann
 * @author Daniel Killer
 * @author Kerstin Reifschläger
 * @author Simone Wälde
 * @author David Becker
 * @author Tobias Becker
 * @author Andre Pacak
 * @author Volkan Hacimüftüoglu
 */
"use strict";

/**
 * The LectureDoc object. Takes care of initializing state, registering global
 * listeners, and starting the UI in the right order.
 * @namespace LectureDoc
 */
var LectureDoc = function () {
    /*
     * This is deliberately kept out of the state so that in case there's a problem
     * with the state, debugging won't be affected.
     */
    var debug_OFF = 0,
        debug_ERROR = -1,
        debug_WARNING = -2,
        debug_INFO = -3,
        debug_TRACE = -4,
        debug_ALL = -2147483648;
    var debug = debug_OFF;

    /**
     * Logs something (to the console).
     * @param {*} something whatever you want
     * @param {?Number} loglevel the log level for this message.
     * @memberof LectureDoc
     */

    function log(something, loglevel) {
        if (loglevel == undefined) {
            loglevel = debug_INFO;
        }
        if (debug > loglevel) return;
        console.log(something);
    }

    /**
     * Transforms a triple of key, old value, and new value to a string.
     * @param {*} key the key
     * @param {*} oldValue the old value
     * @param {*} newValue the new value
     * @return [String]
     */

    function debugString(key, oldValue, newValue) {
        return key + ": " + oldValue + " --> " + newValue;
    }

    /**
     * Transforms an object into a JSON style string, but does not recurse
     * (unlike JSON.stringify). Needed because JSON.stringify fails with
     * self-referencing structures like HTML nodes.
     */

    function stringify(object) {
        return "{" +
            Object.keys(object).map(function (key) {
                return '"' + key + '":"' + object[key] + '"';
            }).join(",") + "}";
    }

    /**
     * A collection that does not contain duplicate values.
     * @param initialItems the initial content of the new Set.
     * Contained duplicate elements will only be added once.
     * @class Set
     */

    function Set(initialItems) {
        /**
         * The actual container of the elements of this set.
         * @memberof Set
         * @instance
         */
        var items = [];
        /**
         * Adds the given item to this Set unless it is already in it.
         * @param item the item to add
         * @memberof Set
         * @instance
         */

        function add(item) {
            if (!set.contains(item)) {
                items.push(item);
            }
        };
        this.add = add;
        /**
         * Check if the given item is in this Set.
         * @param value the item to check
         * @return {Boolean} true if <code>item</code> is in this Set, false otherwise.
         * @memberof Set
         * @instance
         */

        function contains(item) {
            return items.reduce(function (found, i) {
                return found || i === item;
            }, false);
        };
        this.contains = contains;
        /**
         * Returns the number of elements in this set.
         * @return {Number} the number of elements in this set.
         * @memberof Set
         * @instance
         */

        function size() {
            return items.length;
        };
        this.size = size;
        /**
         * Executes the given function for each item in the set.
         * @param {Function} f
         * @memberof Set
         * @instance
         */

        function forEach(f) {
            items.forEach(f);
        };
        this.forEach = forEach;
        var set = this;
        if (Array.isArray(initialItems)) {
            initialItems.forEach(function (item) {
                set.add(item);
            });
        }
    };

    /**
     * Offers methods for registering and notifying listeners for arbitrary properties.
     * @class ListenerHelper
     */

    function ListenerHelper() {
        // listeners interested in changes to all properties are stored here
        var allPropertiesListeners = [];
        // listeners interested in changes to only some properties are stored
        // here; keys are the property names, values the lists of listeners
        // interested in changes to that specific property
        var specificPropertiesListeners = {};

        /**
         * Represents a changed property.
         * @typedef ModifiedProperty
         * @type {object}
         * @property {*} newValue the property's new value
         * @property {*} oldValue the property's old value
         * @memberof ListenerHelper
         * @inner
         */
        /**
         * Represents a set of altered properties.
         * All properties must be pairs of <code>keyname</code> : {@link ModifiedProperty}.
         * @typedef ModifiedProperties
         * @type {object}
         * @memberof ListenerHelper
         * @inner
         */
        /**
         * A Listener is notified of changes by a ListenerHelper.
         * @callback Listener
         * @param source the object on which the change occurred
         * @param {ModifiedProperties} modifiedProperties the source's properties that have changed
         * @memberof ListenerHelper
         * @inner
         */
        /**
         * Registers a listener for a set of properties.
         * @param {Listener} listener the listener
         * @param {{undefined|string|Array<string>}} properties the properties the listener is interested in.
         * if undefined, the listener will be notified of all property changes.
         * @memberof ListenerHelper
         * @instance
         */

        function addListener(listener, properties) {
            if (typeof listener !== "function") return;
            if (typeof properties === "string") properties = [properties];
            if (Array.isArray(properties)) {
                properties.forEach(function (p) {
                    if (p in specificPropertiesListeners) {
                        specificPropertiesListeners[p].push(listener);
                    } else {
                        specificPropertiesListeners[p] = [listener];
                    }
                });
            } else {
                allPropertiesListeners.push(listener);
            }
        };
        this.addListener = addListener;
        /**
         * Notifies all listeners that are interested in changes to any of the given properties.
         * @param source the object whose properties changed
         * @param {ModifiedProperties} modifiedProperties
         * @memberof ListenerHelper
         * @instance
         */

        function notifyListeners(source, modifiedProperties) {
            if (Object.keys(modifiedProperties).length === 0) return;
            var listenersToNotify = new Set(allPropertiesListeners);
            for (var key in modifiedProperties) {
                if (key in specificPropertiesListeners) {
                    specificPropertiesListeners[key].forEach(function (listener) {
                        listenersToNotify.add(listener);
                    });
                }
            }
            if (debug <= debug_INFO) {
                if (Object.keys(modifiedProperties).length > 0) {
                    log("ListenerHelper: notifying " + listenersToNotify.size() +
                        " listener(s) about the following " +
                        Object.keys(modifiedProperties).length +
                        " change(s):\n" + stringify(modifiedProperties));
                }
            }
            var results = {};
            var debug_requestingListenersCounter = 0;
            listenersToNotify.forEach(function (listener) {
                var changes = listener(source, modifiedProperties);
                if (debug <= debug_INFO) {
                    if (changes && Object.keys(changes).length > 0) {
                        debug_requestingListenersCounter++;
                    }
                }
                results = Util.merge(results, changes);
            });
            if (debug <= debug_INFO) {
                if (results && Object.keys(results).length > 0) {
                    log("ListenerHelper: " + debug_requestingListenersCounter +
                        " out of " + Object.keys(listenersToNotify).length +
                        " listener(s) request(s) the following " +
                        Object.keys(results).length + " change(s):\n" +
                        stringify(results));
                }
            }
            return results;
        };
        this.notifyListeners = notifyListeners;
    };

    /**
     * This is LectureDoc's global state. You can set and get properties and
     * register listeners to be notified when some (or all) properties change.
     * You can also add constraints to limit the possible values of properties.
     * <br>
     * Property changes are made up by passing a map of properties to the update
     * method.
     * <br>
     * If listeners wish to update some properties based on a processed event,
     * they need to return a map of the changes they want to make. The changes
     * requested by all listeners will be merged and applied after all listeners
     * have been notified of the previous changes.
     * @namespace State
     */
    var State = function () {
        /**
         * The application state. Persistent data is located here.
         * @memberof State
         * @namespace applicationState
         */
        var applicationState = {
            /**
             * LectureDoc's current rendering mode.
             * @type {String}
             * @memberof State.applicationState
             * @inner
             */
            mode: "document",
            /**
             * The slide that is (or would be) shown in presentation mode.
             * @type {Number}
             * @memberof State.applicationState
             * @inner
             */
            currentSlide: -1,
            /**
             * The number of the last animation-step element per slide that is already shown in presentation mode.
             * Represented as string, can be read via Util.getCurrentAnimationStep or converted by Util.StateStringToArray
             * @type (String)
             * @see Util.getCurrentAnimationStep
             * @see Util.StateStringToArray
             * @memberof State.applicationState
             * @inner
             */
            currentAnimationStep: undefined,
            /**
             * Indicates whether the user has acknowledged the position of the menu button.
             * @type {Boolean}
             * @memberof State.applicationState
             * @inner
             */
            doNotShowMenuHint: false,
            /**
             * The factor to scale slides on the light table by.
             * @type {Number}
             * @memberof State.applicationState
             * @inner
             */
            lightTableZoomFactor: 0.25,
            /**
             * The amount that slides are (or would be) zoomed by in presentation mode.
             * 1=100%.
             * @type {Number}
             * @memberof State.applicationState
             * @inner
             */
            presentationZoomFactor: 1,
            /**
             * Indicates whether the enhanced presentation mode is active or not.
             * @type {Boolean}
             * @memberof State.applicationState
             * @inner
             */
            isEnhancedPresentationModeActive: false,
            /**
             * Is an array which contains all notes which are created by users.
             * @type {Array[Object]}
             * @memberof State.applicationState
             * @inner
             */
            clientNotes: [],
            /**
             * A list of widgets with their properties
             * @type {String}
             * @memberof State.applicationState
             * @inner
             */
            widgets: "",
        };
        /**
         * The application state. Volatile data is located here.
         * @memberof State
         * @namespace sessionState
         */
        var sessionState = {
            /**
             * A clone of the unmodified document's body element.
             * @type {HTMLElement}
             * @memberof State.sessionState
             * @inner
             */
            theDocumentBody: undefined,
            /**
             * A list of slide elements ("section.slide") that are defined on
             * {@link theDocumentBody}.
             * @type {Array<HTMLElement>}
             * @memberof State.sessionState
             * @inner
             */
            theSlides: undefined,
            /**
             * A list of headers outside slides, the table of contents
             * @type {Array<HTMLElement>}
             * @memberof State.sessionState
             * @inner
             */
            theTableOfContents: undefined,
            /**
             * The number of elements for every slide that can be hidden and sequentally blend in (presentation mode only)
             * The indices of the list correspond to slide indices.
             * @type (Array<Number>)
             * @memberof State.sessionState
             * @inner
             */
            theAnimationStepElements: undefined,
            /**
             * Unix timestamp of the current document's last modification.
             * @type {Number}
             * @memberof State.sessionState
             * @inner
             */
            lastModified: null,
            /**
             * A list of lists of HTML a elements that represent links to HTML
             * aside elements.
             * The indices of the outer list correspond to slide indices.
             * @type {Array<Array<HTMLElement>>}
             * @memberof State.sessionState
             * @inner
             */
            slideAsides: undefined,
            /**
             * The HTML content that is shown in the help dialog.
             * @type {HTMLElement}
             * @memberof State.sessionState
             * @inner
             */
            helpNode: undefined,
            /**
             * Internal state used for entering arbitrary number sequences to
             * jump to slides.
             * @type {?String}
             * @memberof State.sessionState
             * @inner
             */
            enteredSlideNumber: null,
            /**
             * Indicates whether the overlay layer is active or not.
             * @type {Boolean}
             * @memberof State.sessionState
             * @inner
             */
            isOverlayActive: false,
            /**
             * The color of the overlay layer when active. Must be either
             * "black" or "white".
             * @type {String}
             * @memberof State.sessionState
             * @inner
             */
            overlayLayerColor: "black",
            /**
             * Indicates whether a modal dialog is shown or not.
             * @type {Boolean}
             * @memberof State.sessionState
             * @inner
             */
            isModalDialogActive: false,
            /**
             * The width of a single slide.
             * @type {Number}
             * @memberof State.sessionState
             * @inner
             */
            slideWidth: 0,
            /**
             * The height of a single slide.
             * @type {Number}
             * @memberof State.sessionState
             * @inner
             */
            slideHeight: 0,
            /**
             * The aspect ratio of a single slide (slideWidth/slideHeight).
             * @type {Number}
             * @memberof State.sessionState
             * @inner
             */
            slideAspectRatio: 0,
            /**
             * Indicates whether the menu bar is visible.
             * @type {Boolean}
             * @memberof State.sessionState
             * @inner
             */
            isMenubarVisible: true,
            /** Indicates whether all elements are visible, regardless of a hidden-tag
             * @type {Boolean}
             * @memberof State.sessionState
             * @inner
             */
            isAllVisible: false,
            /**
             * Indicates whether the defined transitions
             * between slides are shown or not.
             * @type {Boolean}
             * @memberof State.sessionState
             * @inner
             */
            isTransitionEnabled: true,
            /**
             * Indicates if drawing is enabled. Is only used for touchscreen devices.
             * @type {Boolean}
             * @memberof State.sessionState
             * @inner
             */
            isTouchDrawingEnabled: false,
            /**
             * Indicates whether the client notes are shown.
             * @type {Boolean}
             * @memberof State.sessionState
             * @inner
             */
            areClientNotesVisible: true,
            /**
             * Indicates whether the client notes that cant be
             * associated with a slide are shown or not.
             * @type {Boolean}
             * @memberof State.sessionState
             * @inner
             */
            isLostClientNotesActive: false,
            /**
             * Indicates whether this lecturedoc instance is a presentation window
             * @type {Boolean}
             * @memberof State.sessionState
             * @inner
             */
            isMarionette: false,
        };

        var excludedProperties = {
            presentationZoomFactor: true,
            mode: true,
            isEnhancedPresentationModeActive: true,
            isMenubarVisible: true,
            isMarionette: true,
            isModalDialogActive: true,
            doNotShowMenuHint: true,
        };

        var listeners = new ListenerHelper();
        var constraints = {};
        /**
         * Used to implement constraints on properties.
         * @typedef Constraint
         * @type {Object}
         * @property {Constraint#Validate} validate the function used to
         * determine validity
         * @property {Constraint#Fail} onFail the function to call if validation
         * fails (e.g. to display a custom error message)
         * @memberof State
         * @inner
         */
        /**
         * Validates an impending change to a property.
         * @callback Validate
         * @param {String} key the name of the property to be changed
         * @param {*} newValue the value intended to be assigned to the property
         * @param {*} oldValue property's current value
         * @return {Boolean} true if the new value is valid, false if not
         * @memberof State
         * @inner
         */
        /**
         * Is called when the corresponding validation fails.
         * @callback Fail
         * @param {String} key the name of the property to be changed
         * @param {*} newValue the value intended to be assigned to the property
         * @param {*} oldValue property's current value
         * @memberof State
         * @inner
         */
        /**
         * Validates the new value for the named property, based on the given
         * constraints.
         * @param {String} key the property's name
         * @param {*} newValue the property's intended new value
         * @param {*} oldValue the property's current value
         * @param {Array<Constraint>} constraints the constraints to check
         * @return {Boolean} true if all constraints accept the new value, false
         * if at least one does not
         * @memberof State
         * @inner
         */

        function validate(key, newValue, oldValue, constraints) {
            log("State: validating " + debugString(key, oldValue, newValue), debug_TRACE);
            return !constraints || constraints.reduce(function (result, constraint) {
                var validate = constraint.validate || constraint;
                var pass = validate(key, newValue, oldValue);
                if (!pass && constraint.onFail) {
                    constraint.onFail(key, newValue, oldValue);
                }
                return result && pass;
            }, true);
        }

        var runningUpdatesCounter = 0;
        /**
         * @namespace State
         */
        var object = {
            /**
             * @see ListenerHelper#addListener
             * @memberof State
             * @inner
             */
            addListener: listeners.addListener,
            /**
             * Registers a new constraint.
             * @param {String} key the name of the property to register the
             * constraint for
             * @param {Constraint#Validate} validate the validation function
             * @param {Constraint#Fail} onFail the failure function
             * @memberof State
             * @inner
             */
            addConstraint: function (key, validate, onFail) {
                if (typeof validate !== "function") return;
                var constraint = onFail && (typeof onFail === "function") ? {
                    validate: validate,
                    onFail: onFail
                } : validate;
                if (key in constraints) {
                    constraints[key].push(constraint);
                } else {
                    constraints[key] = [constraint];
                }
                log("State: registered constraint for property " + key);
            },
            /**
             * Returns the value of the named property.
             * @param key the property's name
             * @return the property's value (possibly undefined)
             * @memberof State
             * @inner
             */
            get: function (key) {
                if (key in applicationState) {
                    var value = applicationState[key];
                    log("State: reading application state: " + key + " = " +
                        value, debug_TRACE);
                    return applicationState[key];
                } else {
                    var value = sessionState[key];
                    if (!(key in sessionState)) {
                        log("State: reading unknown variable " + key,
                            debug_ERROR);
                    }
                    log("State: reading session state: " + key + " = " + value,
                        debug_TRACE);
                    return value;
                }
            },
            /**
             * Returns a copy of the application state object.
             * @memberof State
             * @inner
             */
            getApplicationState: function () {
                return Util.merge({}, applicationState);
            },
            /**
             * Applies the property changes contained in the given settings object.
             * Each property change is validated by its constraints prior to
             * being applied.
             * <br>
             * When all changes have been made, registered listeners are notified
             * of the changes.
             * Their return values are then collected and, if any of them requested
             * more changes, applied.
             * @param {Object} settings the property changes
             * @memberof State
             * @inner
             */
            update: function (settings) {
                log("State: entering update section");
                if (runningUpdatesCounter) {
                    log("State: re-entering update section while updating",
                        debug_WARNING);
                }
                runningUpdatesCounter++;
                var debug_passCounter = 0;
                while (settings) {
                    log("State: update pass " + (++debug_passCounter));
                    var modifiedProperties = {};
                    for (var key in settings) {
                        var newValue = settings[key];
                        var oldValue = this.get(key);
                        if (newValue === oldValue) {
                            log("State: update suppressed (unchanged value): " +
                                debugString(key, oldValue, newValue), debug_TRACE);
                            continue;
                        }
                        if (!validate(key, newValue, oldValue, constraints[key])) {
                            log("State: update suppressed (validation failed): " +
                                debugString(key, oldValue, newValue), debug_ERROR);
                            continue;
                        }
                        modifiedProperties[key] = {
                            oldValue: oldValue,
                            newValue: newValue
                        };
                        if (key in applicationState) {
                            log("State: updating application state: " +
                                debugString(key, oldValue, newValue));
                            applicationState[key] = newValue;
                        } else { // new variables are stored in the session state
                            if (!(key in sessionState)) {
                                log("State: new variable introduced: " + key,
                                    debug_WARNING);
                            }
                            log("State: updating session state: " +
                                debugString(key, oldValue, newValue));
                            sessionState[key] = newValue;
                        }
                    }
                    settings = listeners.notifyListeners(this, modifiedProperties);
                }
                runningUpdatesCounter--;
                log("State: leaving update section after " + debug_passCounter +
                    " pass(es)");
            },
            /**
             * Returns a subset of the state
             * @return {Integer} as described
             * @see State#getExcludedExchangeProperties
             * @memberof State
             * @inner
             */
            getExchangeState: function () {
                return Util.difference(Util.merge(sessionState, applicationState), excludedProperties);
            },
        };
        return object;
    }();


    /**
     * If this callback is triggered through an event (e.g. keypress or mouseclick),
     * that event will be the parameter. Programmatic calls may pass arbitrary
     * parameters or even none.
     * @callback Execute
     * @memberof Action
     * @inner
     */
    /**
     * An Action encapsulates several display and activity attributes together
     * with key strokes and a function.
     * <br>
     * All constructor parameters can be either constant values or functions. If
     * they are functions, they will be reevaluated whenever a change to the global
     * {@link State} occurs. If these reevaluations result in changes, registered
     * {@link ListenerHelper#Listener}s will be notified of these changes.
     * <br>
     * Hint: it is similar to Java's <code>AbstractAction</code>.
     *
     * @param {Boolean|Function} isEnabled indicates whether the Action can be
     * executed
     * @param {String|Function} getLabel what should be the Action's label, e.g.
     * in a button
     * @param {String|Function} getIcon the name or path of the icon that should
     * be used to represent this Action
     * @param {String|Function} getId the Action's identifying name
     * @param {Action.Execute} execute the Action's <em>action</em>
     * @param {Array<String>|Function} getKeyStrokes a list of key strokes that
     * should trigger the Action
     * @param {Boolean|Function} isVisible indicates whether the Action's visual
     * representation should be visible
     * @param {Boolean|Function} isActive indicates whether the Action's visual
     * representation should be rendered as active
     * @class Action
     */

    function Action(isEnabled, getLabel, getIcon, getId, execute, getKeyStrokes,
        isVisible, isActive) {
        var listeners = new ListenerHelper();

        function getValue(producer) {
            return (typeof producer === "function") ? producer() : producer;
        }
        this.isEnabled = getValue(isEnabled);
        this.isVisible = getValue(isVisible);
        this.isActive = getValue(isActive);
        this.getLabel = getValue(getLabel);
        this.getIcon = getValue(getIcon);
        this.getId = getValue(getId);
        this.getKeyStrokes = getValue(getKeyStrokes);
        var action = this;
        this.execute = function () {
            if (action.isEnabled) {
                execute.apply(action, arguments);
            }
        };
        this.addListener = listeners.addListener;
        /**
         * Checks if any properties have changed and if so, notifies registered listeners.
         * @memberof Action
         */
        this.update = function () {
            function updateIfChanged(propertyName, producer, modifiedProperties) {
                if (typeof producer !== "function") {
                    return;
                }
                log("Action: prepare to update " + propertyName + " of action " + getId,
                    debug_TRACE);
                var newValue = producer();
                if (action[propertyName] !== newValue) {
                    modifiedProperties[propertyName] = {
                        oldValue: action[propertyName],
                        newValue: newValue
                    };
                    action[propertyName] = newValue;
                    log("Action: updating action state: " + debugString(propertyName,
                        modifiedProperties[propertyName].oldValue, newValue));
                } else {
                    log("Action: update suppressed (unchanged value): " +
                        debugString(propertyName, action[propertyName], newValue),
                        debug_TRACE);
                }
            }
            var modifiedProperties = {};
            updateIfChanged("isEnabled", isEnabled, modifiedProperties);
            updateIfChanged("getLabel", getLabel, modifiedProperties);
            updateIfChanged("getIcon", getIcon, modifiedProperties);
            updateIfChanged("getId", getId, modifiedProperties);
            updateIfChanged("getKeyStrokes", getKeyStrokes, modifiedProperties);
            updateIfChanged("isVisible", isVisible, modifiedProperties);
            updateIfChanged("isActive", isActive, modifiedProperties);
            listeners.notifyListeners(action, modifiedProperties);
        };
        State.addListener(this.update);
    };

    /**
     * Implementation of the builder pattern for {@link Action}s.
     * @param {String} id the Action's id
     * @see Action
     * @class ActionBuilder
     */

    function ActionBuilder(id) {
        var m_enabled = true;
        var m_label = "";
        var m_icon = "";
        var m_id = id || "";
        var m_execute = function () {};
        var m_keyStrokes = undefined;
        var m_visible = true;
        var m_active = false;

        /**
         * Sets the function that determines whether the action is enabled or
         * not at a particular time.
         * @param {function} enabled the enabled function.
         * @memberof ActionBuilder
         * @instance
         */

        function setEnabled(enabled) {
            m_enabled = enabled;
            return this;
        };
        this.setEnabled = setEnabled;
        /**
         * Sets the function that determines the label to use at a particular
         * time.
         * @param {function} label the label function.
         * @memberof ActionBuilder
         * @instance
         */

        function setLabel(label) {
            m_label = label;
            return this;
        };
        this.setLabel = setLabel;
        /**
         * Sets the function that determines the icon to use at a particular
         * time.
         * @param {function} icon the icon function.
         * @memberof ActionBuilder
         * @instance
         */

        function setIcon(icon) {
            m_icon = icon;
            return this;
        };
        this.setIcon = setIcon;
        /**
         * Sets the id of the action to build.
         * @param {function} id the id.
         * @memberof ActionBuilder
         * @instance
         */

        function setId(id) {
            m_id = id;
            return this;
        };
        this.setId = setId;
        /**
         * Sets the function that is executed if the action is executed.
         * @param {function} execute the function to execute.
         * @memberof ActionBuilder
         * @instance
         */

        function setExecute(execute) {
            m_execute = execute;
            return this;
        };
        this.setExecute = setExecute;
        /**
         * Sets the function that determines the available keystrokes at a
         * particular time.
         * @param {function} keyStrokes the key strokes function.
         * @memberof ActionBuilder
         * @instance
         */

        function setKeyStrokes(keyStrokes) {
            m_keyStrokes = keyStrokes;
            return this;
        };
        this.setKeyStrokes = setKeyStrokes;
        /**
         * Sets the function that determines whether the UI components for this
         * action are visible or not at a particular time.
         * @param {function} visible the visible function.
         * @memberof ActionBuilder
         * @instance
         */

        function setVisible(visible) {
            m_visible = visible;
            return this;
        };
        this.setVisible = setVisible;
        /**
         * Sets the function that determines whether the UI components for this
         * action are highlighted as active or not at a particular time.
         * @param {function} active the active function.
         * @memberof ActionBuilder
         * @instance
         */

        function setActive(active) {
            m_active = active;
            return this;
        };
        this.setActive = setActive;
        /**
         * Builds a new Action from this ActionBuilder.
         * @memberof ActionBuilder
         * @instance
         */

        function buildAction() {
            return new Action(m_enabled, m_label, m_icon, m_id, m_execute, m_keyStrokes,
                m_visible, m_active);
        };
        this.buildAction = buildAction;
    };

    var ClientNotes = function () {
        function getNote(element) {
            var hash = element.getAttribute("data-hash");
            var slideNr = element.getAttribute("data-slidenr");
            var title = element.getAttribute("data-title");
            var id = element.getAttribute("data-id");
            var note = Util.getClientNote(hash, slideNr, title, id);
            return note;
        }
        var addTextChangeListener = function (element) {
            element.addEventListener("keyup", function (event) {
                var notes = State.get("clientNotes");
                var value = element.value;
                var note = getNote(element.parentNode);
                var index = notes.indexOf(note);
                if (index > -1) {
                    note.text = value;
                    notes.splice(index, 1);
                    notes.push(note);
                }
                State.update({
                    clientNotes: notes
                });
                Actions.StateWriter.execute();
                event.stopPropagation();
            }, false);
        }
        var addClientNote = function (noteObject, parent) {
            var title = noteObject.title || "null";
            var id = "ldjs-client-note-" + noteObject.hash + "-" + noteObject.slideNr +
                "-" + title.replace(" ", "_") + "-" + noteObject.id;
            Widgets.addWidget(id, function (widget) {
                var outerDiv = document.createElement("div");
                widget.appendChild(outerDiv);

                var label = document.createElement("div");
                label.innerHTML = "Note";
                label.id = "label";
                outerDiv.appendChild(label);

                var input = document.createElement("textarea");
                input.value = noteObject.text;
                addTextChangeListener(input);
                widget.appendChild(input);

                var deleteButton = document.createElement("div");
                deleteButton.innerHTML = "Remove";
                deleteButton.id = "button";
                var deleteButtonClickEvent = function (event) {
                    var note = getNote(outerDiv.parentNode);
                    var notes = State.get("clientNotes");
                    notes = notes.filter(function (item) {
                        return note !== item;
                    });
                    State.update({
                        clientNotes: notes
                    });
                    widget.style.visibility = "hidden";
                    Widgets.removeWidget(id);
                    Actions.StateWriter.execute();
                    deleteButton.removeEventListener("click", deleteButtonClickEvent,
                        false);
                };
                //we need to add an touchlistener here to make the deleteButton work.
                deleteButton.addEventListener("click", deleteButtonClickEvent, false);
                outerDiv.appendChild(deleteButton);

                widget.classList.add("ldjs-client-note");
                widget.setAttribute("data-hash", noteObject.hash);
                widget.setAttribute("data-slideNr", noteObject.slideNr);
                widget.setAttribute("data-title", noteObject.title);
                widget.setAttribute("data-id", noteObject.id);
                widget.style.left = noteObject.x + "px";
                widget.style.top = noteObject.y + "px";

                Widgets.addDragAndDrop(widget, parent);
            }, parent);
        }
        return {
            /**
             * Add a note to the corresponding slide if the slide exists.
             * @param {Object} noteObject the note you want to add.
             * @memberof ClientNotes
             * @inner
             */
            addNoteToSlide: function (noteObject) {
                var slideIndex = Util.getSlideIndexOfClientNote(noteObject);
                if (slideIndex === undefined) {
                    return;
                }
                var content = Util.bodyContent().querySelectorAll("section.slide")[slideIndex].querySelector(
                    ".section-body");
                addClientNote(noteObject, content);
            },
            /**
             * Add a note to an element.
             * @param {Object} noteObject the note you want to add.
             * @param {HTMLElement} parent the element you want the note add to.
             * @memberof ClientNotes
             * @inner
             */
            addNoteToElement: function (noteObject, parent) {
                addClientNote(noteObject, parent);
            },
            /**
             * Makes all notes visible on the slide.
             * @param {Number} slideIndex the position of the slide.
             * @memberof ClientNotes
             * @inner
             */
            show: function (slideIndex) {
                var slideBody = Util.bodyContent().querySelectorAll("section.slide")[slideIndex].querySelector(
                    ".section-body")
                Widgets.show(slideBody);
            },
            /**
             * Makes all notes invisible on the slide.
             * @param {Number} slideIndex the position of the slide.
             * @memberof ClientNotes
             * @inner
             */
            hide: function (slideIndex) {
                var slideBody = Util.bodyContent().querySelectorAll("section.slide")[slideIndex].querySelector(
                    ".section-body");
                Widgets.hide(slideBody);
            },
        };
    }();

    var Widgets = function () {
        var widget_properties = {};

        function addDragAndDrop(element, dropArea) {
            function dragStart(event) {
                var scaling = Util.getScaling(element.parentNode);
                var transform = Util.getTransformation(element);
                event.dataTransfer.setData("text/plain", event.clientX + ',' + event.clientY +
                    ',' + scaling.scaleX + ',' + scaling.scaleY + ',' +
                    transform.translateX + ',' + transform.translateY);
                event.dataTransfer.setData("id", element.id);
                event.dataTransfer.dropEffect = 'move';
                event.dataTransfer.effectAllowed = 'move';
                element.classList.add("moving");
            }

            function dragOver(event) {
                event.preventDefault();
                return false;
            }

            function drop(event) {
                if (element.id === event.dataTransfer.getData("id")) {
                    var data = event.dataTransfer.getData("text/plain").split(',');
                    var moveByX = ((event.clientX - parseInt(data[0], 10)) / parseFloat(
                        data[2]));
                    var moveByY = ((event.clientY - parseInt(data[1], 10)) / parseFloat(
                        data[3]));
                    var x = moveByX + parseFloat(data[4]);
                    var y = moveByY + parseFloat(data[5]);
                    log("Widgets: Moving " + element.id + " by " + moveByX + "/" +
                        moveByY + ", moved from original position: " + x + "/" + y);
                    var transform = 'translate(%moveXpx,%moveYpx)'.replace(/%moveX/g, x).replace(
                        /%moveY/g, y);
                    element.style.transform = transform;
                    element.style.WebkitTransform = transform;
                    element.classList.remove("moving");
                }
                updateElementProperties(element);
                event.preventDefault();
                return false;
            }

            function dragEnd(event) {
                var widget = Util.bodyContent().querySelector("#" + element.id);
                if (widget !== null) {
                    widget.classList.remove("moving");
                }
            }

            //Once the iPad supports the HTML5 drag and drop API add the listeners here
            element.addEventListener('dragstart', dragStart, false);

            if (dropArea instanceof HTMLElement) dropArea = [dropArea];
            if (Array.isArray(dropArea)) {
                dropArea.forEach(function (p) {
                    p.addEventListener('dragover', dragOver, false);
                    p.addEventListener('drop', drop, false);
                    p.addEventListener('dragend', dragEnd, false);
                });
            } else {
                document.body.addEventListener('dragover', dragOver, false);
                document.body.addEventListener('drop', drop, false);
                document.body.addEventListener('dragend', dragEnd, false);
            }
            element.setAttribute("draggable", "true");
        }

        function updateElementProperties(element) {
            var element_properties = {};
            var transform = Util.getTransformation(element);
            element_properties.posX = transform.translateX;
            element_properties.posY = transform.translateY;
            element_properties.visible = element.classList.contains("visible");
            widget_properties[element.id] = element_properties;
            storeProperties();
        }

        function loadElementProperties(element) {
            loadProperties();
            var element_properties = widget_properties[element.id];
            if (element_properties === undefined) {
                return;
            }
            // element is not undefined, should have position & visibility
            var transform = 'translate(%moveXpx,%moveYpx)'.replace(/%moveX/g,
                element_properties.posX).replace(/%moveY/g, element_properties.posY);
            element.style.transform = transform;
            element.style.WebkitTransform = transform;
            if (element_properties.visible) {
                element.classList.add("visible");
            } else {
                element.classList.remove("visible");
            }
            log("Succesfully loaded Element \"" + element.id + "\" on Position (" +
                element_properties.posX + "," + element_properties.posY + ")");
        }

        function loadProperties() {
            var property_string = State.get("widgets");
            if (property_string === "" || undefined)
                return;
            widget_properties = JSON.parse(property_string);
        }

        function storeProperties() {
            var property_string = JSON.stringify(widget_properties);
            State.update({
                widgets: property_string
            });
        }

        loadProperties();

        return {
            /**
             * adds a new widget with the given id to the given parent element
             * @param {String} id the unique widget id
             * @param {Function} the initialization function, it is called with the created widget as parameter
             * @param {HTMLElement} parent element
             * @memberof Widgets
             * @inner
             */
            addWidget: function (id, initialize, parent) {
                var widget = window.document.createElement("div");
                widget.id = id;
                widget.classList.add("ldjs-widget");
                parent.appendChild(widget);
                initialize(widget);
                loadElementProperties(widget);
                updateElementProperties(widget);
            },
            /**
             * removes the widget with the given id
             * @param {String} the unique widget id
             * @memberof Widgets
             * @inner
             */
            removeWidget: function (id) {
                var widget = window.document.querySelector("#" + id);
                widget.parentNode.removeChild(widget);
                widget_properties[id] = {};
                storeProperties();
                return true;
            },
            /**
             * adds HTML5-native drag and drop moving to the given html element
             * @param {HTMLElement} element
             * @param {HTMLElement|Array<HTMLElement>} one ore more dropAreas that are targets for the moving
             * @memberof Widgets
             * @inner
             */
            addDragAndDrop: addDragAndDrop,
            /**
             * sets all widgets within the given element to visible
             * @param {HTMLElement} parent
             * @memberof Widgets
             * @inner
             */
            show: function (parent) {
                if (arguments.length == 0) {
                    parent = window.document;
                }
                var widgets = parent.querySelectorAll(".ldjs-widget");
                for (var i = 0; i < widgets.length; i++) {
                    widgets[i].classList.add("visible");
                }
            },
            /**
             * sets all widgets within the given element to invisible
             * @param {HTMLElement} parent
             * @memberof Widgets
             * @inner
             */
            hide: function (parent) {
                if (arguments.length == 0) {
                    parent = window.document;
                }
                var widgets = parent.querySelectorAll(".ldjs-widget");
                for (var i = 0; i < widgets.length; i++) {
                    widgets[i].classList.remove("visible");
                }
            },
            /**
             * resets the position of all widgets within the given element to their initial position
             * @param {HTMLElement} parent
             * @memberof Widgets
             * @inner
             */
            resetPositions: function (parent) {
                if (arguments.length == 0) {
                    parent = window.document;
                }
                var widgets = parent.querySelectorAll(".ldjs-widget");
                for (var i = 0; i < widgets.length; i++) {
                    var widget = widgets[i];
                    var element_properties = widget_properties[widget.id];
                    if (element_properties === undefined)
                        return;
                    element_properties.posX = 0;
                    element_properties.posY = 0;
                    element_properties.visible = widget.classList.contains("visible");
                    widget_properties[widget.id] = element_properties;
                    storeProperties();
                    loadElementProperties(widget);
                }
            }
        };
    }();
    /**
     * Manages LectureDoc's different rendering modes.
     * @namespace Renderer
     */
    var Renderer = function () {
        /**
         * An array of callback function which are executed when render is called.
         * @memberof Renderer
         */
        var renderCallBackList = [];

        /**
         * Resets the document and any transient state.
         * @memberof Renderer
         */

        function clear() {
            Menubar.hideAllSubmenus();
            Util.bodyContent().innerHTML = "";
            Util.bodyContent().classList.remove("presenter");
            document.body.className = "";
            document.body.style.minWidth = "";
            document.body.style.height = "";
            State.update({
                isOverlayActive: false,
                enteredSlideNumber: null
            });
        }

        /**
         * @namespace Renderer
         */
        return {
            /**
             * render renders the mode's representation
             * @typedef Mode
             * @type {Object}
             * @property {String} className the class name to set on the HTML
             * body element
             * @property {String} name the Mode's human readable name
             * @property {Function}
             * @memberof Renderer
             * @inner
             */
            /**
             * All values must be of type {@link Renderer.Mode}.
             * @type {Object}
             * @memberof Renderer
             * @inner
             * @namespace modes
             */
            modes: {},
            /**
             * Renders the mode's representation.
             * @param {String} mode the name of the mode to render
             * @memberof Renderer
             * @inner
             */
            render: function (mode) {
                var renderer = this.modes[mode];
                log("Renderer: rendering " + renderer.name);
                clear();
                document.body.className = renderer.className;
                renderer.render();
                Util.toArray(Util.bodyContent().querySelectorAll(".ldjs-client-note")).forEach(
                    function (note) {
                        note.parentNode.removeChild(note);
                    });
                if (mode !== "lighttable") {
                    var slides = Util.bodyContent().querySelectorAll("section.slide");
                    Util.toArray(slides).forEach(function (slide, slideIndex) {
                        var notes = Util.getClientNotesOfSlide(slideIndex);
                        notes.forEach(function (note) {
                            ClientNotes.addNoteToSlide(note);
                        });
                        if (mode !== "presentation" && State.get(
                            "areClientNotesVisible")) {
                            ClientNotes.show(slideIndex);
                        }
                    });
                }
                if (mode === "presentation") {
                    Util.toArray(Util.bodyContent().querySelectorAll("section.slide")).forEach(
                        function (slide, index) {
                            ClientNotes.hide(index);
                        });
                    if (State.get("areClientNotesVisible") && !State.get("isMarionette")) {
                        ClientNotes.show(State.get("currentSlide"));
                    }
                }
                renderCallBackList.forEach(function (callBack) {
                    callBack();
                });
            },
            /**
             * Returns the list of render callback functions.
             * @memberof Renderer
             * @inner
             */
            getRenderCallBackList: function () {
                return renderCallBackList;
            },
            /**
             * Initializes the renderer.
             * @memberof Renderer
             * @inner
             */
            initialize: function () {
                log("Renderer: initializing renderer")
                /**
                 * @memberof Renderer.modes
                 * @namespace document
                 */
                Renderer.modes.document = function () {
                    var contentClone = State.get("theDocumentBody").querySelector(
                        "#body-content").cloneNode(true);
                    // moves asides with titles that are placed on slides (and normally
                    // would be hidden) into the document flow behind the slide they
                    // were defined on
                    Util.toArray(contentClone.querySelectorAll("section.slide")).forEach(
                        function (slide, i) {
                            var slideParent = slide.parentNode;
                            var toInsertBefore = slide.nextSibling;
                            Util.toArray(slide.querySelectorAll(
                                ".section-body>aside[data-title]"))
                                .forEach(function (aside) {
                                    slide.parentNode.insertBefore(aside,
                                        toInsertBefore);
                                });
                        });

                    function moveLecturerNotes() {
                        Util.toArray(contentClone.querySelectorAll("section.slide")).forEach(
                            function (slide, i) {
                                var slideParent = slide.parentNode;
                                var toInsertBefore = slide.nextSibling;
                                Util.toArray(slide.querySelectorAll(".note[data-title], .note"))
                                    .forEach(function (note) {
                                        note.classList.remove("visible");
                                        slide.parentNode.insertBefore(note,
                                            toInsertBefore);
                                    });
                            });
                    }
                    return {
                        className: "",
                        name: "Document",
                        toggleLecturerNotes: function () {
                            moveLecturerNotes();
                            Util.toArray(contentClone.querySelectorAll("section.note")).forEach(function (
                                note) {
                                note.classList.toggle("visible");
                            });
                            Renderer.render("document");
                        },
                        render: function () {
                            Util.bodyContent().innerHTML = contentClone.innerHTML;
                        }
                    };
                }();
                /**
                 * @memberof Renderer.modes
                 * @namespace presentation
                 */
                Renderer.modes.presentation = function () {
                    /**
                     * Sets the list of links to asides as described in the
                     * State property <code>asidesList</code>.
                     * @see State
                     * @memberof Renderer.modes.presentation
                     */

                    function initializeAsides(slides) {
                        var asidesList = [];
                        slides.forEach(function (slide) {
                            var asides = Util.toArray(slide.querySelectorAll(
                                ".section-body>aside[data-title]"));
                            asidesList.push(asides.map(function (aside) {
                                var asideButton = Menubar.createTextButton(
                                    new ActionBuilder().setLabel(function () {
                                        if (aside.classList.contains(
                                            "visible")) {
                                            return aside.dataset.title +
                                                " ✦";
                                        } else {
                                            return aside.dataset.title;
                                        }
                                    }).setExecute(function () {
                                        aside.classList.toggle("visible");
                                        this.update();
                                    }).buildAction());
                                return asideButton;
                            }));
                        });
                        State.update({
                            "slideAsides": asidesList
                        });
                    };

                    function initializeLaserPointer() {
                        var theLaserPointer = document.createElement("div");
                        theLaserPointer.id = "ldjs-laserpointer";
                        document.body.insertBefore(theLaserPointer, document.body.firstChild);
                        var zoomLaserPointer = function (state, modifiedProperties) {
                            var radius = State.get("presentationZoomFactor");
                            var diameter = 2 * radius;
                            theLaserPointer.style.width = diameter + "em";
                            theLaserPointer.style.height = diameter + "em";
                            theLaserPointer.style.marginLeft = (-radius) + "em";
                            theLaserPointer.style.marginTop = (-radius) + "em";
                            theLaserPointer.style.borderRadius = diameter + "em";

                        };
                        zoomLaserPointer();
                        State.addListener(zoomLaserPointer, "presentationZoomFactor");
                        var radius = State.get("presentationZoomFactor");
                        var diameter = 2 * radius;
                        theLaserPointer.style.width = diameter + "em";
                        theLaserPointer.style.height = diameter + "em";
                        theLaserPointer.style.marginLeft = (-radius) + "em";
                        theLaserPointer.style.marginTop = (-radius) + "em";
                        var scrolled = Util.getScrollPosition();
                        var laserFunction = function (event) {
                            if (event.ctrlKey && Util.isInPresentationMode()) {
                                // When the slide was changed the laser pointer may belong to
                                // the wrong section element; i.e. a section element belonging
                                // to a slide that is not shown; repair if necessary.
                                if (theLaserPointer.parentNode !== document.body) {
                                    document.body.insertBefore(theLaserPointer,
                                        document.body.firstChild);
                                }

                                theLaserPointer.style.left = (event.clientX + scrolled.left) + "px";
                                theLaserPointer.style.top = (event.clientY + scrolled.top) + "px";
                                theLaserPointer.style.visibility = "visible";
                                event.preventDefault();
                            } else {
                                theLaserPointer.style.visibility = "hidden";
                            }
                        };
                        document.body.addEventListener('mousemove', laserFunction, false);
                        document.body.addEventListener('touchmove', function (event) {

                            if (PresentationTouchHandler.isLaserPointerActive() &&
                                Util.isInPresentationMode()) {
                                // When the slide was changed the laser pointer may belong to
                                // the wrong section element; i.e. a section element belonging
                                // to a slide that is not shown; repair if necessary.

                                if (theLaserPointer.parentNode !== document.body) {
                                    document.body.insertBefore(theLaserPointer,
                                        document.body.firstChild);
                                }
                                theLaserPointer.style.left = (event.touches[0].clientX + scrolled.left) +
                                    "px";
                                theLaserPointer.style.top = (event.touches[0].clientY + scrolled.top) +
                                    "px";
                                theLaserPointer.style.visibility = "visible";
                                event.preventDefault();
                            }

                        }, false);
                        document.body.addEventListener('touchend', function (event) {
                            theLaserPointer.style.visibility = "hidden";
                        }, false);
                    };

                    function addCanvasLayer(slide) {
                        var canvas = document.createElement("div");
                        canvas.className = "ldjs-canvas";
                        var slideBody = slide.querySelector("div.section-body");
                        slideBody.insertBefore(canvas, slideBody.firstChild);

                        var svg = document.createElementNS("http://www.w3.org/2000/svg",
                            "svg");
                        canvas.appendChild(svg);

                        canvas.dataset.strokeColor = "black";
                        var x = -1; // stores the x position of the mouse/finger (when we receive a mousemove/touchmove event)
                        var y = -1; // stores the y position of the mouse/finger (when we receive a mousemove/touchmove event)

                        // The mouse move event listener is added to the slide and not the canvas
                        // because the canvas(svg) is configured to not intercept mouse events to
                        // enable users to,e.g., still click on links even if the canvas is shown.
                        // CSS Property:     svg{ pointer-events: none }.
                        slide.addEventListener("mousemove", function (event) {
                            if (event.shiftKey && canvas.classList.contains("visible")) {
                                var scrollPosition = Util.getScrollPosition();
                                if (State.get("isEnhancedPresentationModeActive")) {
                                    var transformation = Util.getTransformation(Util.bodyContent());
                                    var scale = parseFloat(State.get(
                                        "presentationZoomFactor")) ||
                                        1;
                                    var scale = parseFloat(State.get("presentationZoomFactor")) || 1;
                                    var offSetLeft = ((window.innerWidth - 1024 * parseFloat(
                                        transformation.scaleX)) / 2);
                                    if (scale < 1)
                                        offSetLeft = offSetLeft + ((1024 * ((1 - scale) * parseFloat(
                                            transformation.scaleX))) / 2)

                                    var new_x = event.clientX - offSetLeft;
                                    var new_y = event.clientY;

                                    new_x = new_x / (scale * parseFloat(transformation.scaleX)) + (slide.scrollLeft /
                                        scale);
                                    new_y = new_y / (scale * parseFloat(transformation.scaleY)) + (slide.scrollTop /
                                        scale);
                                } else {
                                    var new_x = event.clientX + scrollPosition.left - slide.offsetLeft;
                                    var new_y = event.clientY + scrollPosition.top - slide.offsetTop;
                                    var scale = parseFloat(State.get(
                                        "presentationZoomFactor")) ||
                                        1;
                                    new_x = new_x / scale;
                                    new_y = new_y / scale;
                                }
                                if (x >= 0 && (new_x !== x || new_y !== y)) {
                                    var line = document.createElementNS(
                                        "http://www.w3.org/2000/svg", "line");
                                    line.setAttribute("stroke", canvas.dataset.strokeColor);
                                    line.setAttribute("stroke-width", "3");
                                    line.setAttribute("x1", x);
                                    line.setAttribute("y1", y);
                                    line.setAttribute("x2", new_x);
                                    line.setAttribute("y2", new_y);
                                    svg.appendChild(line);
                                }
                                x = new_x;
                                y = new_y;
                            } else {
                                x = -1;
                                y = -1;
                            }
                            return false;
                        });
                        //checks if the device has a touchscreen.
                        if ( !! ('ontouchstart' in window)) {
                            slide.addEventListener("touchend", function (event) {
                                x = -1;
                                y = -1;
                            });

                            slide.addEventListener("touchmove", function (event) {
                                var canvas = Util.bodyContent().querySelector(
                                    ".slide.current .ldjs-canvas");
                                if (!canvas) return;
                                var visible = canvas.classList.contains("visible");
                                if (visible && !Util.isNavigationableInPresentationMode() &&
                                    event.touches.length == 1) {
                                    var scrollPosition = Util.getScrollPosition();
                                    var new_x = event.touches[0].clientX +
                                        scrollPosition.left -
                                        slide.offsetLeft;
                                    var new_y = event.touches[0].clientY +
                                        scrollPosition.top -
                                        slide.offsetTop;
                                    var scale = parseFloat(State.get(
                                        "presentationZoomFactor")) ||
                                        1;
                                    new_x = new_x / scale;
                                    new_y = new_y / scale;
                                    if (x >= 0 && (new_x !== x || new_y !== y)) {
                                        var line = document.createElementNS(
                                            "http://www.w3.org/2000/svg", "line");
                                        line.setAttribute("stroke", canvas.dataset.strokeColor);
                                        line.setAttribute("stroke-width", "3");
                                        line.setAttribute("x1", x);
                                        line.setAttribute("y1", y);
                                        line.setAttribute("x2", new_x);
                                        line.setAttribute("y2", new_y);
                                        svg.appendChild(line);
                                    }
                                    x = new_x;
                                    y = new_y;
                                    event.preventDefault();
                                    event.stopPropagation();
                                } else {
                                    x = -1;
                                    y = -1;
                                }
                                return false;
                            });
                        }
                    };

                    function timeString(hours, minutes, seconds) {
                        return ((hours < 10) ? "0" + hours : hours) + ":" +
                            ((minutes < 10) ? "0" + minutes : minutes) + ":" +
                            ((seconds < 10) ? "0" + seconds : seconds);
                    }


                    var slides = State.get("theSlides").map(function (slide, index) {
                        var clone = slide.cloneNode(true);
                        addCanvasLayer(clone);
                        return clone;
                    });
                    var lastSlide = document.createElement("section");
                    lastSlide.id = "ldjs-end-of-presentation";
                    lastSlide.innerHTML = "<p>End of presentation</p>";
                    slides.push(lastSlide);

                    initializeLaserPointer();
                    initializeAsides(slides);
                    PresentationTouchHandler.initialize();

                    function scale(element, factor_w, factor_h, origin) {
                        if (arguments.length <= 3) {
                            origin = '0 0';
                        }
                        if (arguments.length <= 2) {
                            factor_h = factor_w;
                        }
                        var transform = 'scale(%zoomW,%zoomH)'.replace(/%zoomW/g, factor_w)
                            .replace(/%zoomH/g, factor_h);
                        element.style.transform = transform;
                        element.style.WebkitTransform = transform;
                        element.style.transformOrigin = origin;
                        element.style.WebkitTransformOrigin = origin;
                    }

                    function removeTransformation(element) {
                        element.style.transform = "";
                        element.style.WebkitTransform = "";
                    }

                    // initialize Widgets:
                    function createMiniClone(slideIndex) {
                        if (slideIndex === (slides.length - 1)) {
                            // end of presentation
                            var lastSlideBody = document.createElement("div");
                            lastSlideBody.classList.add("section-body");
                            lastSlideBody.innerHTML = "<p>End of presentation</p>";
                            lastSlideBody.firstChild.style.fontSize = "2.3em";
                            lastSlideBody.firstChild.style.color = "black";
                            lastSlideBody.firstChild.style.width = "30em";
                            lastSlideBody.firstChild.style.position = "relative";
                            lastSlideBody.firstChild.style.top = "1em";
                            lastSlideBody.firstChild.style.left = "50%";
                            lastSlideBody.firstChild.style.marginLeft = "-15em"
                            lastSlideBody.firstChild.style.textAlign = "center";
                            var clone = lastSlideBody;
                        } else {
                            var clone = slides[slideIndex].querySelector(".section-body").cloneNode(true);
                        }
                        var notes = clone.querySelectorAll(".ldjs-client-note");
                        Util.toArray(notes).forEach(function (note) {
                            note.classList.remove("visible");
                        });

                        // we need to remove simplejs ids
                        var editors = clone.querySelectorAll('div.simplejseditor');
                        for (var i = 0; i < editors.length; i++) {
                            editors[i].classList.remove("simplejseditor");
                            var editor;
                            var result;
                            if (editor = editors[i].querySelector("[id^=simplejseditor-editor-]")) {
                                editor.value = document.getElementById(editor.id).value;
                                editor.id = "clone-" + editor.id;
                            }
                            if (result = editors[i].querySelector("[id^=simplejseditor-result-]")) {
                                result.value = document.getElementById(result.id).value;
                                result.id = "clone-" + result.id;
                            }
                        }

                        var wrapper = window.document.createElement("section");
                        wrapper.classList.add("slide");
                        wrapper.appendChild(clone);
                        scale(wrapper, 0.4, 0.4, '0px 0px');
                        scale(clone, 1, 1, '0px 0px');
                        clone.style.marginLeft = "0px";
                        clone.style.left = "10px";
                        return wrapper;
                    }

                    var initializeStopwatch = function (stopwatchElement) {
                        var precision = 100; // how often the stopwatch will be updated in milliseconds
                        var stopwatch = 0; // the actual stopwatch (milliseconds)

                        var display = window.document.createElement("div");
                        display.id = stopwatchElement.id + "-display";
                        display.textContent = "00:00:00";
                        stopwatchElement.appendChild(display);
                        var pause = window.document.createElement("div");
                        pause.id = stopwatchElement.id + "-pause";
                        pause.textContent = "Start";
                        pause.classList.add("ldjs-widget-button");
                        stopwatchElement.appendChild(pause);
                        var reset = window.document.createElement("div");
                        reset.id = stopwatchElement.id + "-reset";
                        reset.textContent = "Reset";
                        reset.classList.add("ldjs-widget-button");
                        stopwatchElement.appendChild(reset);


                        var updateStopwatch = function () {
                            stopwatch += precision;
                            var x = stopwatch / 1000;
                            var seconds = Math.floor(x % 60);
                            x /= 60;
                            var minutes = Math.floor(x % 60);
                            x /= 60;
                            var hours = Math.floor(x % 24);
                            var currentTimeString = timeString(hours, minutes, seconds);
                            display.textContent = currentTimeString;
                        };
                        var stopwatchIntervalId;

                        pause.addEventListener("click", function (event) {
                            if (pause.textContent === "Start" || pause.textContent === "Unpause") {
                                stopwatchIntervalId = setInterval(updateStopwatch, precision);
                                pause.textContent = "Pause";
                            } else if (pause.textContent === "Pause") {
                                clearInterval(stopwatchIntervalId);
                                pause.textContent = "Unpause";
                            }
                        });
                        reset.addEventListener("click", function (event) {
                            if (pause.textContent === "Unpause") {
                                pause.textContent = "Start";
                                display.textContent = "00:00:00";
                            }
                            stopwatch = 0;
                        });
                        Widgets.addDragAndDrop(stopwatchElement);
                    };
                    var initializeClock = function (clockElement) {
                        var display = window.document.createElement("div");
                        display.id = clockElement.id + "-display";
                        var updateClock = function () {
                            var currentTime = new Date();
                            var currentHours = currentTime.getHours();
                            var currentMinutes = currentTime.getMinutes();
                            var currentSeconds = currentTime.getSeconds();

                            var currentTimeString = timeString(currentHours, currentMinutes, currentSeconds);
                            display.textContent = currentTimeString;
                        };
                        // update clock every second
                        var clockIntervalId = setInterval(updateClock, 1000);
                        clockElement.appendChild(display);
                        Widgets.addDragAndDrop(clockElement);
                    };
                    var initializePreviousSlide = function (previousSlide) {
                        var previousSlideHeader = window.document.createElement("div");
                        previousSlideHeader.classList.add("ldjs-widget-button", "ldjs-widget-header");
                        previousSlideHeader.textContent = "Previous Slide";
                        previousSlideHeader.addEventListener("click", function (event) {
                            Actions.PreviousSlideAction.execute(event);
                        });
                        previousSlide.appendChild(previousSlideHeader);
                        var previousSlideDisplay = window.document.createElement("div");
                        previousSlideDisplay.id = "ldjs-slide-display";
                        previousSlide.appendChild(previousSlideDisplay);
                        var changeMiniSlide = function (slideIndex) {
                            // removing old display:
                            if (previousSlideDisplay.childNodes[0])
                                previousSlideDisplay.removeChild(previousSlideDisplay.childNodes[0]);
                            // adding new display:
                            if (slideIndex > 0) {
                                previousSlideDisplay.appendChild(createMiniClone(slideIndex - 1));
                            }
                        }
                        changeMiniSlide(State.get("currentSlide"));
                        State.addListener(function (state, modifiedProperties) {
                            changeMiniSlide(State.get("currentSlide"));
                        }, ["currentSlide", "mode", "isEnhancedPresentationModeActive"]);
                        Widgets.addDragAndDrop(previousSlide);
                    };
                    var initializeNextSlide = function (nextSlide) {

                        var nextSlideHeader = window.document.createElement("div");
                        nextSlideHeader.classList.add("ldjs-widget-button", "ldjs-widget-header");
                        var setTitle = function () {
                            var slideIndex = State.get("currentSlide") + 1;
                            if (slideIndex < slides.length - 2) {
                                var type = slides[slideIndex].getAttribute("data-transition");
                                if (State.get("isTransitionEnabled") && (type === "blendOver" || type ===
                                    "slideOut" || type === "dissolve")) {
                                    nextSlideHeader.style.backgroundImage = 'url("' + Menubar.iconsFolder +
                                        'animation.svg")';
                                    nextSlideHeader.style.backgroundPosition = "left";
                                    nextSlideHeader.style.backgroundRepeat = "no-repeat";
                                    nextSlideHeader.style.backgroundSize = "40px 40px";
                                } else
                                    nextSlideHeader.style.backgroundImage = "";
                            } else
                                nextSlideHeader.style.backgroundImage = "";
                            var title = "Next Slide";

                            var remainingSteps = Util.remainingAnimationSteps() + 1;
                            if (State.get("isAllVisible"))
                                remainingSteps = 1;
                            if (remainingSteps > 0)
                                title = title + " in " + remainingSteps;
                            nextSlideHeader.textContent = title;
                        }
                        setTitle();
                        State.addListener(setTitle, ["currentSlide", "currentAnimationStep",
                            "isTransitionEnabled", "isAllVisible"]);
                        nextSlideHeader.addEventListener("click", function (event) {
                            Actions.NextSlideAction.execute(event);
                        });
                        nextSlide.appendChild(nextSlideHeader);
                        var nextSlideDisplay = window.document.createElement("div");
                        nextSlideDisplay.id = "ldjs-slide-display";
                        nextSlide.appendChild(nextSlideDisplay);
                        var changeMiniSlide = function (slideIndex) {
                            // removing old display:
                            if (nextSlideDisplay.childNodes[0])
                                nextSlideDisplay.removeChild(nextSlideDisplay.childNodes[0]);
                            // adding new display:
                            if (slideIndex < slides.length - 1) {
                                nextSlideDisplay.appendChild(createMiniClone(slideIndex + 1));
                            }
                        }
                        changeMiniSlide(State.get("currentSlide"));
                        State.addListener(function (state, modifiedProperties) {
                            changeMiniSlide(State.get("currentSlide"));
                        }, ["currentSlide", "mode", "isEnhancedPresentationModeActive"]);
                        Widgets.addDragAndDrop(nextSlide);
                    };
                    var initializePresenterNotes = function (notesElement) {
                        var moveNotes = function (slideIndex) {
                            if (slideIndex < 0)
                                return;
                            while (notesElement.firstChild) {
                                notesElement.removeChild(notesElement.firstChild);
                            }
                            var markers = slides[slideIndex].querySelectorAll(".ldjs-presenternote-marker");
                            Util.toArray(markers).forEach(function (marker, i) {
                                markers[i].parentNode.removeChild(markers[i]);
                            });
                            var notes = slides[slideIndex].querySelectorAll(".note[data-title], .note");
                            var lastMarker;
                            var lastMarkerIndex = -1;
                            Util.toArray(notes).forEach(function (note, i) {
                                var clone = note.cloneNode(true);
                                notesElement.appendChild(clone);
                                var previous = notes[i].previousSibling.previousSibling;
                                if (lastMarker && previous.classList !== undefined && previous.classList.contains(
                                    "note")) {
                                    lastMarker.textContent = "(" + (lastMarkerIndex + 1) + "-" + (i + 1) +
                                        ")";
                                } else {
                                    var marker = window.document.createElement("p");
                                    marker.classList.add("ldjs-presenternote-marker");
                                    marker.textContent = "(" + (i + 1) + ")";

                                    lastMarker = marker;
                                    lastMarkerIndex = i;
                                    notes[i].parentNode.insertBefore(marker, notes[i]);
                                }
                            });
                        };
                        moveNotes(State.get("currentSlide"));
                        State.addListener(function (state, modifiedProperties) {
                            moveNotes(modifiedProperties.currentSlide.newValue);
                        }, "currentSlide");
                        Widgets.addDragAndDrop(notesElement);
                    };
                    // presentermodelayout is were all widgets for presenter are placed
                    var presentermodelayout = window.document.createElement("div");
                    presentermodelayout.id = "ldjs-presentermodelayout";
                    if (presentermodelayout.parentNode !== window.document.body) {
                        window.document.body.insertBefore(presentermodelayout,
                            window.document.body.firstChild);
                    }

                    Widgets.addWidget("ldjs-stopwatch", initializeStopwatch, presentermodelayout);
                    Widgets.addWidget("ldjs-clock", initializeClock, presentermodelayout);
                    Widgets.addWidget("ldjs-previous-slide", initializePreviousSlide, presentermodelayout);
                    Widgets.addWidget("ldjs-next-slide", initializeNextSlide, presentermodelayout);
                    Widgets.addWidget("ldjs-presenter-notes", initializePresenterNotes, presentermodelayout);

                    function showSlide(slideIndex) {
                        var slide = slides[slideIndex];
                        log("Renderer: showing slide #" + slideIndex + ": " + slide,
                            debug_TRACE);
                        if (!slide) {
                            log("Renderer: slide #" + slideIndex +
                                " not found, abort", debug_ERROR);
                            return;
                        }
                        if (State.get("areClientNotesVisible") && slideIndex < State.get(
                            "theSlides").length && !State.get("isMarionette")) {
                            ClientNotes.show(slideIndex);
                        }
                        slide.classList.add("current");
                        return {
                            isOverlayActive: false
                        };
                    };

                    function hideSlide(slideIndex) {
                        var slide = slides[slideIndex];
                        log("Renderer: hiding slide #" + slideIndex + ": " + slide,
                            debug_TRACE);
                        if (!slide) {
                            return;
                        }
                        if (slideIndex < State.get("theSlides").length) {
                            ClientNotes.hide(slideIndex);
                        }
                        slide.classList.remove("current");
                        showElement(slideIndex, -1);
                    };

                    function showElement(slideIndex, elementIndex) {
                        var slide = slides[slideIndex];
                        var isAllVisible = State.get("isAllVisible"); // prevent reading the State multiple times
                        Util.toArray(slide.querySelectorAll("[data-animation-step]")).forEach(
                            function (element) {
                                var i = parseInt(element.getAttribute(
                                    "data-animation-step"));
                                if (i > elementIndex && !isAllVisible) {
                                    log("showElement: hiding element " + element);
                                    element.classList.add("hidden");
                                } else {
                                    log("showElement: revealing element " + element);
                                    element.classList.remove("hidden");
                                }
                            });
                    }

                    function addTransition(slide, type, duration) {
                        slide.style.webkitAnimation = type;
                        slide.style.webkitAnimationDuration = (duration / 1000) + "s";
                        slide.style.animation = type;
                        slide.style.animationDuration = (duration / 1000) + "s";
                    };

                    function removeTransitions(slide) {
                        slides[slide].style.webkitAnimation = "";
                        slides[slide].style.animation = "";
                        slides[slide].style.webKitAnimationDuration = "";
                        slides[slide].style.animationDuration = "";
                    };

                    function blendOverTransition(newSlide, oldSlide, duration) {
                        var changes = showSlide(newSlide);
                        var slide = slides[newSlide];
                        addTransition(slide, "fadeIn", duration);

                        Events.blockAndCountInput();
                        var endTransition = function () {
                            hideSlide(oldSlide);
                            removeTransitions(newSlide);
                            Events.permitInput();
                            slide.removeEventListener('webkitAnimationEnd', endTransition,
                                false);
                            slide.removeEventListener('animationend', endTransition,
                                false);
                        }
                        slide.addEventListener('webkitAnimationEnd', endTransition, false);
                        slide.addEventListener('animationend', endTransition, false);
                        return changes;
                    };

                    function slideOutTransition(newSlide, oldSlide, duration) {
                        var changes = {};
                        var slide = slides[oldSlide];
                        //if the direction is not set as one of the four cardinal directions use the default direction right.
                        var direction = slides[newSlide].getAttribute('data-direction');
                        direction = (direction !== "right" && direction !== "left" &&
                            direction !== "up" && direction !== "down") ? "right" :
                            direction;
                        direction = direction.charAt(0).toUpperCase() + direction.slice(1);
                        slide.style.border = "1px solid black";
                        slide.style.zIndex++;
                        changes = showSlide(newSlide);
                        addTransition(slide, "slideOut" + direction, duration);

                        Events.blockAndCountInput();
                        var endTransition = function () {
                            hideSlide(oldSlide);
                            slide.style.zIndex--;
                            slide.style.border = "0px solid #ccc";
                            removeTransitions(oldSlide);
                            Events.permitInput();
                            slide.removeEventListener('webkitAnimationEnd', endTransition,
                                false);
                            slide.removeEventListener('animationend', endTransition,
                                false);
                        }
                        slide.addEventListener('webkitAnimationEnd', endTransition, false);
                        slide.addEventListener('animationend', endTransition, false);
                        return changes;
                    };

                    function dissolveTransition(newSlide, oldSlide, duration) {
                        var changes = {};
                        var slide = slides[oldSlide];

                        slide.style.zIndex++;
                        changes = showSlide(newSlide);
                        addTransition(slide, "dissolve", duration);

                        Events.blockAndCountInput();
                        var endTransition = function () {
                            hideSlide(oldSlide);
                            slide.style.zIndex--;
                            Events.permitInput();
                            removeTransitions(oldSlide);
                            slide.removeEventListener('webkitAnimationEnd', endTransition,
                                false);
                            slide.removeEventListener('animationend', endTransition,
                                false);
                        }
                        slide.addEventListener('webkitAnimationEnd', endTransition, false);
                        slide.addEventListener('animationend', endTransition, false);
                        return changes;
                    };

                    function presenterTransition(newSlide, oldSlide, duration) {
                        var changes = {};

                        changes = showSlide(newSlide);
                        hideSlide(oldSlide);
                        if (PresentationWindow.isShown() && PresentationWindow.get("isTransitionEnabled")) {
                            Events.blockAndCountInput();
                            setTimeout(function () {
                                Events.permitInput();
                            }, duration);
                        }
                        return changes;
                    };

                    function processTransitions(oldSlide, newSlide) {
                        //if duration is not a number or is not bigger than zero set the default value 500ms.
                        var duration = parseInt(slides[newSlide].getAttribute(
                            "data-duration"));
                        duration = (isFinite(duration) && duration > 0) ? duration : 500;
                        var type = slides[newSlide].getAttribute("data-transition");
                        if (State.get("isEnhancedPresentationModeActive") && (type === "blendOver" || type ===
                            "slideOut" || type === "dissolve"))
                            type = "presenter";
                        var changes = {};
                        if (newSlide === oldSlide + 1 && State.get("isTransitionEnabled") &&
                            Util.isInPresentationMode()) {
                            switch (type) {
                            case "blendOver":
                                changes = blendOverTransition(newSlide, oldSlide,
                                    duration);
                                break;
                            case "slideOut":
                                changes = slideOutTransition(newSlide, oldSlide, duration);
                                break;
                            case "dissolve":
                                changes = dissolveTransition(newSlide, oldSlide, duration);
                                break;
                            case "presenter":
                                changes = presenterTransition(newSlide, oldSlide, duration);
                                break;
                            default:
                                changes = showSlide(newSlide);
                                hideSlide(oldSlide);
                            }
                        } else {
                            changes = showSlide(newSlide);
                            hideSlide(oldSlide);
                        }
                        State.update(changes);
                    };

                    function zoom() {
                        if (!Util.isInPresentationMode()) return;
                        var factor = State.get("presentationZoomFactor");
                        var widget_factor = Math.min(window.innerHeight / 1080, window.innerWidth / 1920);
                        log("Renderer: zoom to factor " + factor + " (presentation)");
                        var width = (State.get("slideWidth") * factor) + 'px';
                        var height = (State.get("slideHeight") * factor) + 'px';
                        // skip end of presentation slide (last one in slides array)
                        Util.toArray(slides.slice(0, slides.length - 1)).forEach(function (
                            slide) {
                            if (slide === lastSlide) return;
                            var slideBody = slide.querySelector('div.section-body');
                            scale(slideBody, factor);
                            if (State.get("isEnhancedPresentationModeActive") === false) {
                                slide.style.width = width;
                                slide.style.height = height;
                                slide.style.overflow = "visible";
                                if (parseInt(width) < window.innerWidth) {
                                    // only try to center if the slide is smaller than the window
                                    slide.style.marginLeft = (-parseInt(width) / 2) +
                                        "px";
                                    slide.style.left = "50%";
                                } else {
                                    // otherwise just align it top left
                                    slide.style.marginLeft = "0";
                                    slide.style.left = "0";
                                }
                                slideBody.style.marginLeft = "0";
                                slideBody.style.left = "0";
                                removeTransformation(slide);
                            } else {
                                //scale(slide, widget_factor, widget_factor, "50% 0");
                                slide.style.overflow = "auto";
                                slide.style.left = "50%";
                                slide.style.marginLeft = "-512px";
                                slide.style.width = "1024px";
                                slide.style.height = "768px";
                                if (parseInt(width) < 1024) {
                                    // only try to center if the slide is smaller than the window
                                    slideBody.style.marginLeft = (-parseInt(width) / 2) +
                                        "px";
                                    slideBody.style.left = "50%";
                                } else {
                                    // otherwise just align it top left
                                    slideBody.style.marginLeft = "0";
                                    slideBody.style.left = "0";
                                }
                            }
                        });

                        if (State.get("isEnhancedPresentationModeActive") === false) {
                            document.body.style.minWidth = width;
                            document.body.style.minHeight = height;
                            Util.manageScrollbars();
                        } else {
                            window.document.body.classList.remove("scroll");
                            scale(Util.bodyContent(), widget_factor, widget_factor, "50% 0");
                            document.body.style.minWidth = ((State.get("slideWidth") * factor) *
                                widget_factor) + 'px';
                            document.body.style.minHeight = ((State.get("slideHeight") * factor) *
                                widget_factor) + 'px';
                        }
                        scale(presentermodelayout, widget_factor);
                        presentermodelayout.style.marginLeft = (window.innerWidth - (1920 * widget_factor)) /
                            2 + "px";
                        Util.setScrollPosition(0, 0);
                    }

                    function resize() {
                        if (!Util.isInPresentationMode()) return;
                        var documentWidth = parseInt(document.body.style.minWidth);
                        var marginLeft = "0";
                        var left = "0";
                        if (window.innerWidth > documentWidth) { // we can center
                            marginLeft = "-" + (documentWidth / 2) + "px";
                            left = "50%";
                        }
                        slides.slice(0, slides.length - 1).forEach(function (
                            slide) {
                            slide.style.marginLeft = marginLeft;
                            slide.style.left = left;
                        });
                        zoom();
                    }

                    State.addListener(zoom, "presentationZoomFactor");

                    var renderEnhancedPresentationMode = function () {
                        if (Util.isInPresentationMode() && State.get("isEnhancedPresentationModeActive") ===
                            true) {
                            Widgets.show(presentermodelayout);
                            Util.bodyContent().classList.add("ldjs-presenter");
                            zoom();
                            Widgets.hide(presentermodelayout.querySelector("#ldjs-previous-slide"));
                            Widgets.hide(presentermodelayout.querySelector("#ldjs-next-slide"));
                        } else {
                            Widgets.hide(presentermodelayout);
                            Util.bodyContent().classList.remove("ldjs-presenter");
                            removeTransformation(Util.bodyContent());
                            zoom();
                        }
                    };
                    renderEnhancedPresentationMode();
                    State.addListener(renderEnhancedPresentationMode, ["isEnhancedPresentationModeActive",
                        "mode"]);

                    State.addListener(function (state, modifiedProperties) {
                        log("Renderer: switching slide: " +
                            modifiedProperties.currentSlide.oldValue + " --> " +
                            modifiedProperties.currentSlide.newValue);
                        showElement(modifiedProperties.currentSlide.newValue, Util.getCurrentAnimationStep());
                        processTransitions(modifiedProperties.currentSlide
                            .oldValue, modifiedProperties.currentSlide.newValue);
                    }, "currentSlide");

                    State.addListener(function (state, modifiedProperties) {
                        log("Renderer: switching element status: " +
                            modifiedProperties.currentAnimationStep.oldValue +
                            " --> " +
                            modifiedProperties.currentAnimationStep.newValue);
                        showElement(State.get("currentSlide"), Util.getCurrentAnimationStep());

                    }, "currentAnimationStep");

                    State.addListener(function (state, modifiedProperties) {
                        log("Renderer: switching animation step functionality to: " +
                            modifiedProperties.isAllVisible.newValue ? "ON" : "OFF");
                        showElement(State.get("currentSlide"), Util.getCurrentAnimationStep());

                    }, "isAllVisible");
                    // recalculate the slides' position when the window is resized
                    // center only if the window is wider than the slide; otherwise align
                    // the slide with the window's top left corner
                    Gator(window).on("resize", resize);

                    // when scrolling while table of contents / keyslides are open, update highlighting:
                    var updateHighlights = function (event) {
                        Menubar.highlightCurrentTableOfContent();
                        Menubar.highlightCurrentKeySlide(Util.indexOfCurrentSlide());
                    };
                    document.addEventListener("scroll", updateHighlights, false);

                    return {
                        className: "ldjs-slide",
                        name: "Presentation",
                        render: function () {
                            slides.forEach(function (slide) {
                                Util.bodyContent().appendChild(slide);
                            });
                            var slide = State.get("currentSlide");
                            if (slide == -1 || slide >= slides.length) {
                                log("Renderer: invalid slide index " + slide,
                                    debug_ERROR);
                                slide = 0;
                                State.update({
                                    currentSlide: 0
                                });
                            }
                            zoom(State.get("presentationZoomFactor"));
                            showSlide(slide);
                            showElement(slide, Util.getCurrentAnimationStep());
                            if (!State.get("doNotShowMenuHint") && !State.get("isMarionette")) {
                                var message =
                                    "<p><b>To open the menu bar, click on the page number in the lower right corner.</b></p>" +
                                    '<p><input type="checkbox" id="ldjs-menu-hint-do-not-show-again">' +
                                    '<label for="ldjs-menu-hint-do-not-show-again">Do not show again</label></input></p>';
                                // we need a timeout here to make sure that the
                                // message box does not alter state while we are
                                // in an update cycle.
                                // you do not want to adapt the message box for
                                // return value collection. you really don't.
                                setTimeout(function () {
                                    MessageBox.show("ldjs-menu-hint", "", "Info",
                                        message, {
                                            getLabel: "Ok",
                                            execute: function () {
                                                var selection = document.getElementById(
                                                    "ldjs-menu-hint-do-not-show-again"
                                                ).checked;
                                                State.update({
                                                    doNotShowMenuHint: selection
                                                });
                                            }
                                        }, null, true);
                                }, 1);
                            }
                        }
                    };
                }();
                /**
                 * @memberof Renderer.modes
                 * @namespace notes
                 */
                Renderer.modes.notes = function () {
                    var body = State.get("theDocumentBody").cloneNode(true).querySelector(
                        'body>div#body-content');
                    // 1. remove any whitespace or comment between the body tag
                    //    and the first non-text element
                    while (body.hasChildNodes()) {
                        var isComment = body.firstChild.noteType === Node.COMMENT_NODE;
                        var isWhitespaceText = body.firstChild.nodeType === Node.TEXT_NODE && !
                            /\S/.test(body.firstChild
                                .data)
                        if (isComment || isWhitespaceText) {
                            body.removeChild(body.firstChild);
                            log("whitespace removed from the beginning of the document");
                        } else {
                            break;
                        }
                    }

                    // 2. create the lecture notes representation
                    var lectureNotes = document.createElement("div");
                    lectureNotes.id = "ldjs-lecture-notes";

                    var subSection = document.createElement("div");
                    subSection.className = "ldjs-lecture-notes-subsection";
                    lectureNotes.appendChild(subSection);

                    var sidebar;

                    var content = document.createElement("div");
                    content.className = "ldjs-lecture-notes-content";
                    subSection.appendChild(content);

                    while (body.hasChildNodes()) {
                        var currentNode = body.firstChild;
                        body.removeChild(currentNode);

                        if (currentNode.tagName === 'SECTION') {
                            if (!/slide/.test(currentNode.className)) { // currentNode.className !== "slide"){
                                var nextNode = body.firstChild;
                                while (currentNode.hasChildNodes()) {
                                    var node = currentNode.firstChild;
                                    currentNode.removeChild(node);
                                    body.insertBefore(node, nextNode);
                                }

                                continue;
                            }

                            // we create a new section
                            subSection = document.createElement("div");
                            subSection.className = "ldjs-lecture-notes-subsection";
                            lectureNotes.appendChild(subSection);

                            sidebar = document.createElement("div");
                            sidebar.className = "ldjs-lecture-notes-sidebar";
                            subSection.appendChild(sidebar);
                            sidebar.appendChild(currentNode);

                            var sectionBody = currentNode.querySelector(
                                "section.slide>div.section-body")
                            if (sectionBody) {
                                var asides = sectionBody.querySelectorAll(
                                    "div.section-body>aside");
                                for (var i = 0; i < asides.length; i++) {
                                    var aside = asides[i];
                                    sectionBody.removeChild(aside);
                                    sidebar.appendChild(aside);
                                }
                            }

                            content = document.createElement("div");
                            content.className = "ldjs-lecture-notes-content";
                            subSection.appendChild(content);
                        } else {
                            content.appendChild(currentNode);
                        }
                    }

                    var footer = document.createElement("div");
                    footer.id = "ldjs-lecture-notes-footer";
                    footer.appendChild(document.createTextNode("Generated " + new Date()));

                    return {
                        className: "ldjs-lecture-notes",
                        name: "Compact",
                        render: function () {
                            Util.bodyContent().appendChild(lectureNotes);
                            Util.bodyContent().appendChild(footer);
                        }
                    };
                }();
                /**
                 * @memberof Renderer.modes
                 * @namespace lighttable
                 */
                Renderer.modes.lighttable = function () {
                    var slides = State.get("theSlides").map(function (slide, i) {
                        var lightTableSlide = document.createElement("div");
                        lightTableSlide._index = i;
                        lightTableSlide.className = "ldjs-light-table-slide";
                        lightTableSlide.appendChild(slide.cloneNode(true));
                        var slideNumberSpan = document.createElement("span");
                        slideNumberSpan.className = "ldjs-light-table-slide-number";
                        slideNumberSpan.innerHTML = i + 1;
                        lightTableSlide.appendChild(slideNumberSpan);
                        return lightTableSlide;
                    });
                    Events.onClick(".ldjs-light-table-slide", function (event) {
                        if (isFinite(this._index)) {
                            Util.jumpToSlide(this._index)();
                        }
                    });

                    /**
                     * Zooms all slides to the zoom factor set in
                     * ApplicationState.
                     * @memberof Renderer.modes.lighttable
                     * @inner
                     */

                    function zoom() {
                        var factor = State.get("lightTableZoomFactor");
                        log("Renderer: zoom to factor " + factor + " (light table)");
                        var transform = 'scale(%zoom,%zoom)'.replace(/%zoom/g, factor);
                        var width = (State.get("slideWidth") * factor) + 'px';
                        var height = (State.get("slideHeight") * factor) + 'px';
                        slides.forEach(function (slide) {
                            slide.style.width = width;
                            var section = slide.querySelector('section.slide');
                            section.style.width = width;
                            section.style.height = height;
                            var slideBody = section.querySelector('div.section-body');
                            slideBody.style.transform = transform;
                            slideBody.style.WebkitTransform = transform;
                        });
                    };

                    return {
                        className: "ldjs-light-table",
                        name: "Light Table",
                        render: function () {
                            slides.forEach(function (slide) {
                                Util.bodyContent().appendChild(slide);
                            });
                            zoom(State.get("lightTableZoomFactor"));
                            State.addListener(function (state) {
                                zoom(State.get("lightTableZoomFactor"));
                            }, "lightTableZoomFactor");
                        }
                    };
                }();
                /**
                 * @memberof Renderer.modes
                 * @namespace continuous
                 */
                Renderer.modes.continuous = function () {
                    return {
                        className: "ldjs-continuous-layout",
                        name: "Continuous",
                        render: function () {
                            State.get("theSlides").forEach(function (slide) {
                                Util.bodyContent().appendChild(slide);
                            });
                        }
                    };
                }();
            }
        };
    }();
    /**
     * Manages the Touch Events in the presentation mode like laserpointer or swipe motions.
     * @namespace PresentationTouchHandler
     */
    var PresentationTouchHandler = function () {

        /**
         * The x-coordinate from the starting point of the touch.
         * @memberof PresentationTouchHandler
         */
        var startX = 0;
        /**
         * The y-coordinate from the starting point of the touch.
         * @memberof PresentationTouchHandler
         */
        var startY = 0;
        /**
         * The difference between the current x-coordinate on the page and the starting x-coordinate
         * @memberof PresentationTouchHandler
         */
        var deltaX = 0;
        /**
         * The difference between the current y-coordinate on the page and the starting y-coordinate
         * @memberof PresentationTouchHandler
         */
        var deltaY = 0;
        /**
         * The Action which gets executed when a right swipe occured.
         * @memberof PresentationTouchHandler
         */
        var rightAction;
        /**
         * The Action which gets executed when a left swipe occured.
         * @memberof PresentationTouchHandler
         */
        var leftAction;
        /**
         * The Action which gets executed when a point was touched.
         * @memberof PresentationTouchHandler
         */
        var touchAction;
        /**
         * The minimum length of a swipe to get recognized.
         * The average width of a human index finger is 57px.
         * You have to move your finger double the width to active a swipe motion.
         * @memberof PresentationTouchHandler
         */
        var minLength = 114;
        /**
         * Holds information about the Swipemotion, if it is going vertical or not.
         * @memberof PresentationTouchHandler
         */
        var isScrolling;
        /**
         * Tracks if you touch the screen on a single point for more than a second.
         * @memberof PresentationTouchHandler
         */
        var laserPointerTracker;
        /**
         * The timestamp for the last touchstart events occurence
         * @memberof PresentationTouchHandler
         */
        var timestamp;
        /**
         * Signals if the laserpointer needs to be shown or not.
         * @memberof PresentationTouchHandler
         */
        var isLaserPointerActive = false;
        /**
         * Tracks how many times touch input occured.
         * @memberof PresentationTouchHandler
         */
        var events = {
            /**
             * Checks if the touchfunction is enabled for the content and only one finger is being used.
             * If thats the case it forwards the event to the corresponding function.
             * @memberof PresentationTouchHandler.events
             * @inner
             */
            handleEvent: function (event) {
                if (!Util.isNavigationableInPresentationMode())
                    return;
                switch (event.type) {
                case 'touchstart':
                    this.start(event);
                    break;
                case 'touchmove':
                    this.move(event);
                    break;
                case 'touchend':
                    this.end(event);
                    break;
                }
            },
            /**
             * Gets the x and y coordinate of the starting touch
             * and start the tracker for the laserpointer
             * @memberof PresentationTouchHandler.events
             * @inner
             */
            start: function (event) {
                //looks if a finger is hold on the same spot for one second.
                var tracker = function () {
                    var deltaTime = Date.now() - timestamp;
                    if (deltaTime >= 1000 && deltaX == 0 && deltaY == 0) {
                        isLaserPointerActive = true;
                        clearInterval(laserPointerTracker);
                    } else {
                        isLaserPointerActive = false;
                    }
                }
                timestamp = Date.now();
                startX = event.touches[0].pageX;
                startY = event.touches[0].pageY;
                laserPointerTracker = setInterval(tracker, 500);
                isScrolling = undefined;
                deltaX = 0;
                deltaY = 0;
            },
            /**
             * Keeps track of the difference between the current x and y coordinate
             * and the x and y coordinate of the starting touch when the finger moves.
             * @memberof PresentationTouchHandler.events
             * @inner
             */
            move: function (event) {
                if (laserPointerTracker !== undefined) {
                    clearInterval(laserPointerTracker);
                }
                if (event.touches.length > 1) {
                    isScrolling = true;
                    return;
                }
                deltaX = event.touches[0].pageX - startX;
                deltaY = event.touches[0].pageY - startY;
                //checks if it is a vertical motion
                if (typeof isScrolling == 'undefined') {
                    isScrolling = !! (isScrolling || Math.abs(deltaX) < Math.abs(deltaY));
                }
                if (!isScrolling) {
                    event.preventDefault();
                }

            },
            /**
             * Determines if the finger motion was a single touch or swipe and in which direction it went.
             * @memberof PresentationTouchHandler.events
             * @inner
             */
            end: function (event) {
                clearInterval(laserPointerTracker);
                if (isLaserPointerActive) {
                    isLaserPointerActive = false;
                    return;
                }
                //if the move event didnt occur(single touch) and the laserpointer wasnt triggered
                if (typeof isScrolling == 'undefined' && !isLaserPointerActive) {
                    //if the target element is the slide itself or the element is not an input
                    //and isAllVisible has to be false.
                    if ((/<div class="section-body"/g.test(event.target.outerHTML) || !
                            /<input type="\w+">/g.test(event.target.outerHTML)) && !State
                        .get("isAllVisible")) {
                        touchAction.execute();
                    }
                    return;
                }
                if (!isScrolling && Math.abs(deltaX) >= minLength) {
                    if (deltaX > 0) { //right direction
                        rightAction.execute();
                    } else {
                        leftAction.execute();
                    }
                }
            }
        }
        /**
         * @namespace PresentationTouchHandler
         */
        return {
            /**
             * Sets the Actions for the left swipe, right swipe and the single touch.
             * Also registers the eventlisteners for the touchevents.
             * @memberof PresentationTouchHandler
             * @inner
             */
            initialize: function () {
                if ( !! ('ontouchstart' in window)) {
                    leftAction = Actions.NextSlideAction;
                    rightAction = Actions.PreviousSlideAction;
                    touchAction = Actions.NextAnimationStepAction;
                    Util.bodyContent().addEventListener('touchstart', events, false);
                    Util.bodyContent().addEventListener('touchmove', events, false);
                    Util.bodyContent().addEventListener('touchend', events, false);
                }
            },
            /**
             * Returns if the laserpointer is active or not.
             * @memberof PresentationTouchHandler
             * @inner
             */
            isLaserPointerActive: function () {
                return isLaserPointerActive;
            }
        };
    }();

    /**
     * Encapsulates key stroke and mouse click events.
     * @namespace Events
     */
    var Events = function () {
        var countInputAction = 0;

        function blockAndCount(event) {
            countInputAction++;
            if (countInputAction >= 4) {
                if ( !! ("ontouchstart" in window)) {
                    Util.showFeedback("You can disable transitions", "info");
                } else {
                    Util.showFeedback("You can disable transitions by pressing z", "info");
                }
            }
            blockEvent(event);
        }

        function blockEvent(event) {
            event.preventDefault();
            event.stopPropagation();
        }

        /**
         * @namespace Events
         */
        return {
            /**
             * Binds the Action's key strokes to the Action's execute function.
             * @param {Action} action the action to bind
             * @param {String} [type=keyup] the kind of key event to react to
             * @memberof Events
             * @inner
             */
            bindKeys: function (action, type) {
                if (!action.getKeyStrokes) {
                    return;
                }
                type = type || "keyup";
                log("Events: binding " + action.getKeyStrokes.toString() + " to " +
                    action.getId + " for " + type);
                Mousetrap.bind(action.getKeyStrokes, action.execute, type);
                action.addListener(function (source, modifiedProperties) {
                    if (source !== action) return;
                    Mousetrap.unbind(modifiedProperties["getKeyStrokes"].oldValue);
                    Mousetrap.bind(modifiedProperties["getKeyStrokes"].newValue,
                        action.execute);
                }, ["getKeyStrokes"]);
            },
            /**
             * Adds the given callback as an onClick listener to the given element.
             * @param {String} selector css to select the element that should react to clicks
             * @param {Function} f the callback function (will be passed the event object)
             * @memberof Events
             * @inner
             */
            onClick: function (selector, f) {
                Gator(document).on("click", selector, f);
            },
            /**
             * Registers the onClick listener on the document that is then used to
             * receive and dispatch click events.
             * @memberof Events
             * @inner
             */
            initialize: function () {
                Gator(document).on("keyup", MessageBox.keyListener);
                Gator(document);
                Gator(document).on("keyup", function (event) {
                    var changes = {};
                    var keyCode = event.keyCode ? event.keyCode : event.which;
                    if (State.get("mode") !== "presentation" ||
                        State.get("isModalDialogActive")) return;
                    var entered = State.get("enteredSlideNumber");
                    if (keyCode >= 48 && keyCode <= 57) {
                        var digit = keyCode - 48;
                        entered = entered === null ? digit : entered * 10 + digit;
                    } else if (keyCode == 13 && entered !== null) {
                        entered = Math.min(entered, State.get("theSlides").length + 1);
                        changes["currentSlide"] = entered > 0 ? entered - 1 : entered;
                        entered = null;
                    } else if (keyCode == 8 && entered !== null) {
                        entered = entered < 10 ? null : Math.floor(entered / 10);
                        // block default backspace action (history back in some browsers)
                        event.preventDefault();
                    } else {
                        entered = null;
                    }
                    log("Events: jump to slide " + entered);
                    changes["enteredSlideNumber"] = entered;
                    State.update(changes);
                });
                State.addListener(function (state, modifiedProperties) {
                    var entered = modifiedProperties.enteredSlideNumber.newValue;
                    var info = document.querySelector("#ldjs-jump-target-info");
                    if (entered === null && info !== null) {
                        info.classList.add("fadeout");
                        setTimeout(function () {
                            if (info !== null && info.parentElement !== null) {
                                info.parentElement.removeChild(info);
                            }
                        }, 600);
                    } else if (entered !== null) {
                        if (info === null) {
                            info = document.createElement("div");
                            info.id = "ldjs-jump-target-info";
                            document.body.appendChild(info);
                        }
                        info.innerHTML = entered;
                    }
                }, "enteredSlideNumber");
            },
            blockAndCountInput: function () {
                log("Events: blocking user input");
                countInputAction = 0;
                document.addEventListener("click", blockAndCount, true);
                document.addEventListener("keyup", blockAndCount, true);
                document.addEventListener("keydown", blockAndCount, true);
                if ( !! ("ontouchstart" in window)) {
                    document.addEventListener("touchstart", blockAndCount, true);
                    document.addEventListener("touchmove", blockAndCount, true);
                    document.addEventListener("touchend", blockAndCount, true);
                }
                Mousetrap.pause();
            },
            permitInput: function () {
                log("Events: allowing user input");
                document.removeEventListener("click", blockAndCount, true);
                document.removeEventListener("keyup", blockAndCount, true);
                document.removeEventListener("keydown", blockAndCount, true);
                if ( !! ("ontouchstart" in window)) {
                    document.removeEventListener("touchstart", blockAndCount, true);
                    document.removeEventListener("touchmove", blockAndCount, true);
                    document.removeEventListener("touchend", blockAndCount, true);
                }
                Mousetrap.unpause();
            },
            blockKeyEvents: function () {
                log("Events: blocking keyboard events");
                Mousetrap.pause();
                document.addEventListener("keydown", blockEvent, false);
            },
            permitKeyEvents: function () {
                log("Events: allowing keyboard events");
                document.removeEventListener("keydown", blockEvent, false);
                Mousetrap.unpause();
            }
        };
    }();

    /**
     * Functions to show/hide message boxes.
     * @namespace MessageBox
     */
    var MessageBox = {
        /**
         * If MessageBox.show is called while a modal dialog is active, the
         * parameters of that call are appended to this queue. When a dialog is
         * hidden and there is at least one dialog queued, the first waiting
         * dialog is shown.
         * @memberof MessageBox
         * @inner
         */
        queue: [],
        /**
         * Displays a message box with one or two buttons (default: "Ok",
         * optionally: "Cancel").
         * If there is already another dialog, this call is being deferred until
         * the other dialog has been closed.
         * @param {String} id this will be the id attribute of the generated
         * HTML element
         * @param {String} [className=ldjs-message-box ldjs-message-box-information]
         * this will be the initial className attribute of the generated HTML
         * element. It will always contain ldjs-message-box.
         * @param {String} title the dialog's title (may contain HTML)
         * @param {String} message this is the message that will be displayed
         * (may contain HTML)
         * @param {Action} [onOk] if given, this Action will be used for the
         * "Ok" button. It will provide the button's label and its execute
         * function will be called with the click event as sole parameter before
         * the message box is hidden.
         * @param {Action} [onCancel] if given, the generated message box will
         * have a "Cancel" button in addition to the "Ok" button whose label and
         * behavior will be determined by this action. This action's execute
         * function will be called with the click event as the sole parameter
         * before the dialog is hidden.
         * @param {Boolean} [modal=false] if true, the message box will be made
         * modal, i.e. no user interaction with the remaining application will
         * be permitted until the message box has been closed using either the
         * "Ok" or (if present) the "Cancel" button.
         * @memberof MessageBox
         * @inner
         */
        show: function (id, className, title, message, onOk, onCancel, modal) {
            if (document.querySelector(".ldjs-message-box")) {
                MessageBox.queue.push(Util.toArray(arguments));
                return;
            }
            var messageBox = document.createElement("div");
            messageBox.id = id;
            messageBox.className = "ldjs-message-box " + (className ||
                "ldjs-message-box-information");
            var header = document.createElement("header");
            header.innerHTML = title;
            messageBox.appendChild(header);
            var container = document.createElement("div");
            container.className = "ldjs-message-box-content";
            container.innerHTML = message;
            messageBox.appendChild(container);
            var buttons = document.createElement("div");
            buttons.className = "ldjs-message-box-buttons";
            messageBox.appendChild(buttons);
            var okButton = document.createElement("div");
            okButton.className = "ldjs-message-box-button ldjs-message-box-ok";
            okButton.innerHTML = onOk ? onOk.getLabel || "Ok" : "Ok";
            okButton.addEventListener("click", function (event) {
                if (onOk && typeof onOk.execute === "function") {
                    onOk.execute(event);
                }
                MessageBox.hide(id);
            }, false);
            buttons.appendChild(okButton);
            if (onCancel && onCancel.getLabel && typeof onCancel.execute === "function") {
                var cancelButton = document.createElement("div");
                cancelButton.className =
                    "ldjs-message-box-button ldjs-message-box-cancel";
                cancelButton.addEventListener("click", function (event) {
                    onCancel.execute(event);
                    MessageBox.hide(id);
                }, false);
                cancelButton.innerHTML = onCancel.getLabel || "Cancel";
                buttons.appendChild(cancelButton);
            }
            if (modal) {
                if (State.get("isModalDialogActive")) return;
                State.update({
                    overlayLayerColor: "white",
                    isOverlayActive: true,
                    isModalDialogActive: true
                });
                messageBox.classList.add("ldjs-modal-dialog");
            }
            Gator(window).on("resize", MessageBox.resizeWindow);
            document.body.appendChild(messageBox);
            MessageBox.resizeWindow();
        },
        /**
         * Hides the visible message box with the given id. Does nothing if
         * there is no message box with this id.
         * @param {String} id the id of the message box that should be hidden
         * @memberof MessageBox
         * @inner
         */
        hide: function (id) {
            var messageBox = document.getElementById(id);
            if (messageBox) {
                log("MessageBox: hiding" + id);
                if (messageBox.classList.contains("ldjs-modal-dialog")) {
                    State.update({
                        isOverlayActive: false,
                        isModalDialogActive: false
                    });
                }
                messageBox.parentNode.removeChild(messageBox);
                if (MessageBox.queue.length > 0) {
                    var args = MessageBox.queue.shift();
                    MessageBox.show.apply(MessageBox, args);
                }
            }
        },
        keyListener: function (event) {
            var messageBoxes = document.querySelectorAll(".ldjs-message-box");
            if (messageBoxes.length == 0) {
                return;
            }
            var topmost = messageBoxes[messageBoxes.length - 1];
            switch (event.keyCode) {
            case 13:
                topmost.querySelector(".ldjs-message-box-ok").click();
                break;
            case 27:
                var cancel = topmost.querySelector(".ldjs-message-box-cancel");
                if (cancel) cancel.click();
                else topmost.querySelector(".ldjs-message-box-ok").click();
                break;
            }
            event.stopPropagation();
            event.preventDefault();
            return false;
        },
        resizeWindow: function (event) {
            Util.toArray(document.querySelectorAll(".ldjs-message-box")).forEach(function (
                messageBox) {
                // since css does not support max-height percent values with overflowing,
                // we need to calculate those percentages ourselves
                var height = window.innerHeight;
                // height*0.1 = top padding
                // 51/height*0.1 = lower padding (to keep the dialog above the menu bar)
                height -= height * 0.1 + (Math.max(51, height * 0.1));
                messageBox.style.maxHeight = height + "px";
                // subtract 80 from the inner container height for the button container
                // and the header
                var container = messageBox.querySelector(".ldjs-message-box-content");
                container.style.maxHeight = (height - 80) + "px";
            });
        }
    }

    var PresentationWindow = function () {
        // concat name with pathname, to link the window to this file
        var name = "ldjs-presentation-window" + window.location.pathname;

        // the reference to the presentationwindow
        var presentationwindow;

        /**
         * @see cleanup()
         */
        var unRegister = [];
        /**
         * de-registers all observer and listener if the PresentationWindow has been closed
         * note: should be called first within every listener that interacts with the presentationwindow
         * @return {boolean} true if PresentationWindow is not open
         */
        function cleanup() {
            if (PresentationWindow.isShown())
                return false;
            unRegister.forEach(function (callBack) {
                callBack();
            });
            unRegister = [];
            return true;
        }

        function isExtrawindowValid() {
            return !(presentationwindow === undefined || presentationwindow === null || presentationwindow.closed);
        }

        var specs = "" +
        "menubar=no," +     // = (yes|no|1|0) Whether or not to display the menu bar
        "scrollbars=1," +   // = (yes|no|1|0) Whether or not to display scroll bars. IE, Firefox & Opera only
        "status=no," +      // = (yes|no|1|0) Whether or not to add a status bar
        "toolbar=no," +     // = (yes|no|1|0) Whether or not to display the browser toolbar. IE and Firefox only
        "height=768," +     // = (pixels)     The height of the window. Min. value is 100
        "left=0," +         // = (pixels)     The left position of the window. Negative values not allowed
        "top=0," +          // = (pixels)     The top position of the window. Negative values not allowed
        "width=1024";       // = (pixels)     The width of the window. Min. value is 100

        return {
            /**
             * toggles the visibility of the presentationwindow
             * @memberof PresentationWindow
             * @inner
             */
            toggle: function () {
                if (PresentationWindow.isShown())
                    PresentationWindow.hide();
                else
                    PresentationWindow.show();
            },
            /**
             * processes data send by another instance of lecturedoc
             * @param {String} identification of the given data
             * @param {Object} the actual data
             * @memberof PresentationWindow
             * @inner
             */
            acceptData: function (ident, data) {
                switch (ident) {
                case "asides":
                    var slides = Util.bodyContent().querySelectorAll("section.slide");
                    Util.toArray(slides).forEach(function (slide, slideIndex) {
                        var asides = slides[slideIndex].querySelectorAll(
                            ".section-body>aside[data-title]");
                        var slide_data = data[slideIndex];
                        Util.toArray(asides).forEach(function (aside, asideIndex) {
                            if (slide_data[asideIndex])
                                asides[asideIndex].classList.add("visible");
                            else
                                asides[asideIndex].classList.remove("visible");
                        });
                    });
                    break;
                case "multiplechoice":
                    var slides = Util.bodyContent().querySelectorAll("section.slide");
                    Util.toArray(slides).forEach(function (slide, slideIndex) {
                        var answers = slide.querySelectorAll(".mc-answer");
                        var slide_data = data[slideIndex];
                        Util.toArray(answers).forEach(function (answer, answerIndex) {
                            answers[answerIndex].parentNode.appendChild(slide_data[answerIndex].cloneNode(
                                true));
                            answers[answerIndex].parentNode.removeChild(answers[answerIndex]);
                        });
                    });
                    break;
                case "simplejs":
                    var slides = Util.bodyContent().querySelectorAll("section.slide");
                    Util.toArray(slides).forEach(function (slide, slideIndex) {
                        var simplejs = slide.querySelectorAll(
                            "[id^=simplejseditor-editor-], [id^=simplejseditor-result-]");
                        var slide_data = data[slideIndex];
                        Util.toArray(simplejs).forEach(function (sjs, sjsIndex) {
                            simplejs[sjsIndex].value = slide_data[sjsIndex];
                        });
                    });
                    break;
                case "laserpointer":
                    var theLaserPointer = window.document.querySelector("#ldjs-laserpointer");

                    if (theLaserPointer.parentNode !== document.body) {
                        document.body.insertBefore(theLaserPointer,
                            document.body.firstChild);
                    }

                    theLaserPointer.style.visibility = data.visibility;
                    if (data.left)
                        theLaserPointer.style.left = data.left + "px";
                    if (data.top)
                        theLaserPointer.style.top = data.top + "px";
                    break;
                case "canvas":
                    var slide = Util.bodyContent().querySelectorAll("section.slide")[data.id];
                    var canvas;
                    //remove old canvas
                    while (canvas = slide.querySelector("div.section-body>div.ldjs-canvas")) {
                        canvas.parentNode.removeChild(canvas);
                    }
                    var slideBody = slide.querySelector("div.section-body");
                    // insert new canvas
                    slideBody.insertBefore(data.canvas, slideBody.firstChild);
                    break;
                case "hideHint":
                    MessageBox.hide("ldjs-menu-hint");
                    break;
                default:
                }
            },
            /**
             * opens and initializes a presentationwindow
             * does nothing if already open
             * @memberof PresentationWindow
             * @inner
             */
            show: function () {
                if (PresentationWindow.isShown())
                    return;
                presentationwindow = window.open(window.location.href, name, specs);

                // Note: could be too excessive to send all data every time something happens to more than one receiver
                // Note2: a presentation-streaming feature could use the following sendXXX-methods with their
                // corresponding acceptData-entries, maybe put them in their own namespace

                // when DOMContent is loaded we can access LectureDoc
                Gator(presentationwindow).on("DOMContentLoaded", function () {
                    var title = presentationwindow.document.createElement("title");
                    title.textContent = "LectureDoc Beamer-Window";
                    presentationwindow.document.head.appendChild(title);

                    State.addListener(function (s, m) {
                        if (cleanup())
                            return;
                        presentationwindow.LectureDoc.updateState(s, m);
                    });

                    // asides
                    var slides = Util.bodyContent().querySelectorAll("section.slide");
                    var sendAsides = function () {
                        if (cleanup())
                            return;
                        var data = {};
                        Util.toArray(slides).forEach(function (slide, slideIndex) {
                            var asides = slide.querySelectorAll(".section-body>aside[data-title]");
                            var slide_data = {};
                            Util.toArray(asides).forEach(function (aside, asideIndex) {
                                slide_data[asideIndex] = asides[asideIndex].classList.contains(
                                    "visible");
                            });
                            data[slideIndex] = slide_data;
                        });
                        presentationwindow.LectureDoc.sendData("asides", data);
                    };
                    Util.toArray(slides).forEach(function (slide, slideIndex) {
                        var asides = slides[slideIndex].querySelectorAll(
                            ".section-body>aside[data-title]");
                        Util.toArray(asides).forEach(function (aside, asideIndex) {
                            // create an observer instance
                            var observer = new MutationObserver(function (mutations) {
                                mutations.forEach(function (mutation) {
                                    // we send complete data every time!
                                    sendAsides();
                                });
                            });
                            // configuration of the observer:
                            var config = {
                                attributes: true,
                                childList: false,
                                characterData: false
                            };
                            // pass in the aside node, as well as the observer options
                            observer.observe(asides[asideIndex], config);
                            unRegister.push(function () {
                                observer.disconnect();
                            });
                        });
                    });
                    // MultipleChoice
                    var sendMultipleChoice = function () {
                        if (cleanup())
                            return;
                        var data = {};
                        Util.toArray(slides).forEach(function (slide, slideIndex) {
                            var answers = slide.querySelectorAll(".mc-answer");

                            var slide_data = {};
                            Util.toArray(answers).forEach(function (answer, answerIndex) {
                                slide_data[answerIndex] = answers[answerIndex];
                            });
                            data[slideIndex] = slide_data;
                        });
                        presentationwindow.LectureDoc.sendData("multiplechoice", data);
                    };
                    Util.toArray(slides).forEach(function (slide, slideIndex) {
                        var answers = slide.querySelectorAll(".mc-answer");
                        Util.toArray(answers).forEach(function (answer, answerIndex) {
                            // create an observer instance
                            var observer = new MutationObserver(function (mutations) {
                                // we send complete data every time!
                                sendMultipleChoice();
                            });
                            // configuration of the observer:
                            var config = {
                                attributes: true,
                                childList: false,
                                characterData: false
                            };
                            // pass in the aside node, as well as the observer options
                            observer.observe(answers[answerIndex], config);
                            unRegister.push(function () {
                                observer.disconnect();
                            });
                        });
                    });
                    // simplejs
                    var sendSimpleJs = function () {
                        if (cleanup())
                            return;
                        var data = {};
                        Util.toArray(slides).forEach(function (slide, slideIndex) {
                            var simplejs = slide.querySelectorAll(
                                "[id^=simplejseditor-editor-], [id^=simplejseditor-result-]");

                            var slide_data = {};
                            Util.toArray(simplejs).forEach(function (sjs, sjsIndex) {
                                slide_data[sjsIndex] = simplejs[sjsIndex].value;
                            });
                            data[slideIndex] = slide_data;
                        });
                        presentationwindow.LectureDoc.sendData("simplejs", data);
                    };
                    var addSimpleJsInputListener = function () {
                        Util.toArray(slides).forEach(function (slide, slideIndex) {
                            var simplejs = slide.querySelectorAll(
                                "[id^=simplejseditor-editor-], [id^=simplejseditor-result-]");
                            Util.toArray(simplejs).forEach(function (sjs, sjsIndex) {
                                // create an observer instance
                                simplejs[sjsIndex].addEventListener('input', sendSimpleJs, false);
                                unRegister.push(function () {
                                    simplejs[sjsIndex].removeEventListener('change',
                                        sendSimpleJs);
                                });
                            });
                        });
                    };
                    addSimpleJsInputListener();
                    State.addListener(function () {
                        addSimpleJsInputListener();
                    }, "mode");
                    Gator(window).on("click", sendSimpleJs);
                    unRegister.push(function () {
                        Gator(window).off("click", sendSimpleJs);
                    });
                    // laserpointer
                    var getSlideXY = function (x, y) {
                        var scale = parseFloat(State.get(
                            "presentationZoomFactor")) ||
                            1;
                        var windowSlideWidth = State.get("slideWidth") * scale;
                        var windowSlideHeight = State.get("slideHeight") * scale;
                        if (State.get("isEnhancedPresentationModeActive")) {
                            var transformation = Util.getTransformation(Util.bodyContent());
                            var scale = parseFloat(State.get("presentationZoomFactor")) || 1;
                            var offSetLeft = ((window.innerWidth - 1024 * parseFloat(transformation.scaleX)) /
                                2);
                            if (scale < 1)
                                offSetLeft = offSetLeft + ((1024 * ((1 - scale) * parseFloat(
                                    transformation.scaleX))) / 2)

                            var new_x = x - offSetLeft;
                            var new_y = y;

                            new_x = new_x / (scale * parseFloat(transformation.scaleX)) + (document.querySelector(
                                "section.slide.current").scrollLeft / scale);
                            new_y = new_y / (scale * parseFloat(transformation.scaleY)) + (document.querySelector(
                                "section.slide.current").scrollTop / scale);
                        } else {
                            var scrolled = Util.getScrollPosition();
                            var new_x = x + scrolled.left - Math.max(((window.innerWidth -
                                windowSlideWidth) / 2), 0);
                            var new_y = y + scrolled.top;

                            new_x = new_x / scale;
                            new_y = new_y / scale;
                        }
                        if (new_x >= 0 && new_x < State.get("slideWidth") && new_y < State.get(
                            "slideHeight")) {
                            // x/y are within the actual slide
                            return {
                                x: new_x,
                                y: new_y
                            };
                        }
                        return false;
                    };
                    var transformXY = function (x, y) {
                        var windowWidth = presentationwindow.innerWidth;
                        var extraWindowScaleFactor = presentationwindow.LectureDoc.get(
                            "presentationZoomFactor");
                        var windowSlideWidth = State.get("slideWidth") * extraWindowScaleFactor;
                        var horizontalBorder = Math.max((windowWidth - windowSlideWidth) / 2, 0); // slide is centered horizontal
                        return {
                            x: (x * extraWindowScaleFactor) + horizontalBorder,
                            y: (y * extraWindowScaleFactor)
                        };
                    };
                    var laserPointerFunction = function (event) {
                        if (cleanup())
                            return;
                        var data = {};
                        data.visibility = "hidden";
                        if (event.ctrlKey) {
                            var coordinates = getSlideXY(event.clientX, event.clientY);
                            if (!coordinates) {
                                // hide it
                                presentationwindow.LectureDoc.sendData("laserpointer", data);
                                return;
                            }
                            coordinates = transformXY(coordinates.x, coordinates.y);

                            data.left = coordinates.x;
                            data.top = coordinates.y;
                            data.visibility = "visible";
                            event.preventDefault();
                        }
                        presentationwindow.LectureDoc.sendData("laserpointer", data);
                    };
                    document.body.addEventListener('mousemove', laserPointerFunction, false);
                    unRegister.push(function () {
                        document.body.removeEventListener('mousemove', laserPointerFunction);
                    });
                    // canvas
                    var sendCanvas = function () {
                        Util.toArray(slides).forEach(function (slide, slideIndex) {
                            if (cleanup())
                                return;
                            var data = {};
                            data.id = slideIndex;
                            var canvas = slides[slideIndex].querySelector(
                                "div.section-body>div.ldjs-canvas").cloneNode(true);
                            data.canvas = canvas;
                            presentationwindow.LectureDoc.sendData("canvas", data);
                        });
                    }
                    Util.toArray(slides).forEach(function (slide, slideIndex) {
                        var observer = new MutationObserver(function (mutations) {
                            mutations.forEach(function (mutation) {
                                sendCanvas();
                            });
                        });
                        var config = {
                            attributes: true,
                            childList: true,
                            subtree: true,
                            characterData: false
                        };
                        observer.observe(slides[slideIndex].querySelector(
                            "div.section-body>div.ldjs-canvas"), config);
                    });
                    var remoteInit = function (event) {
                        // switch to normal presentation mode
                        presentationwindow.LectureDoc.updateState(undefined, {
                            isEnhancedPresentationModeActive: false,
                            presentationZoomFactor: 1,
                            isMarionette: true,
                            isModalDialogActive: false,
                            doNotShowMenuHint: true,
                        });
                        presentationwindow.LectureDoc.sendData("hideHint", undefined);
                        // send each once (and then let the listener to their job)
                        sendAsides();
                        sendMultipleChoice();
                        sendSimpleJs();
                        sendCanvas();
                        // and remove initialization listener
                        if (event.type === 'LDStateInitialized')
                            presentationwindow.removeEventListener('LDStateInitialized', remoteInit);
                        if (event.type === 'LDInitialized')
                            presentationwindow.removeEventListener('LDInitialized', remoteInit);
                    }
                    // Listen for the remote lecturedoc initialization
                    presentationwindow.addEventListener('LDStateInitialized', remoteInit, false);
                    presentationwindow.addEventListener('LDInitialized', remoteInit, false);
                });

                Gator(window).on("unload", PresentationWindow.hide);

            },
            /**
             * closes the presentationwindow and does a listener cleanup
             * does nothing if no presentationwindow is open
             * @memberof PresentationWindow
             * @inner
             */
            hide: function () {
                if (!PresentationWindow.isShown())
                    return;
                cleanup();
                presentationwindow.close();
            },
            /**
             * @return {boolean} true if a presentationwindow is open and there is still a reference to it
             * @memberof PresentationWindow
             * @inner
             */
            isShown: isExtrawindowValid,
            get: function (key) {
                if (!PresentationWindow.isShown())
                    return;
                return presentationwindow.LectureDoc.get(key);
            },
        };
    }();

    /**
     * Various functions that have no other place
     * @namespace Util
     */
    var Util = {
        /**
         * Convenience function to return the content element into which the
         * modes will place their rendered content.
         * @memberof Util
         * @inner
         */
        bodyContent: function () {

            return document.getElementById("body-content");
        },
        /**
         * Convenience function that returns true if and only if the current
         * mode is the document mode
         * @return{Boolean} as described.
         * @memberof Util
         * @inner
         */
        isInDocumentMode: function () {
            return State.get("mode") === "document";
        },
        /**
         * Convenience function that returns true if and only if the current
         * mode is the presentation mode.
         * @return {Boolean} as described.
         * @memberof Util
         * @inner
         */
        isInPresentationMode: function () {
            return State.get("mode") === "presentation";
        },
        /**
         * Convenience function that returns true if and only if the current
         * mode is the light table mode.
         * @return {Boolean} as described.
         * @memberof Util
         * @inner
         */
        isInLightTableMode: function () {
            return State.get("mode") === "lighttable";
        },
        /**
         * Convenience function that returns true if and only if the current
         * mode is the notes mode.
         * @return {Boolean} as described.
         * @memberof Util
         * @inner
         */
        isInNotesMode: function () {
            return State.get("mode") === "notes";
        },
        /**
         * A function that returns true if and only if the current mode
         * is the presentation mode and the canvas is not shown.
         * @return {Boolean} as described.
         * @memberof Util
         * @inner
         */
        isNavigationableInPresentationMode: function () {
            var enableSelected = State.get("mode") === "presentation" && !State.get(
                "isTouchDrawingEnabled");
            var canvas = Util.bodyContent().querySelector(
                ".slide.current .ldjs-canvas");
            if (!canvas) {
                return enableSelected;
            }
            var visible = canvas.classList.contains("visible");
            return !visible || enableSelected;
        },
        /**
         * Returns a function that, when executed, switches to presentation mode and
         * displays the slide with the given index.
         * @param {Number} slideIndex the index (first slide = 0) of the slide to jump to
         * @memberof Util
         * @inner
         */
        jumpToSlide: function (slideIndex) {
            return function () {
                var changes = {};
                changes["currentSlide"] = slideIndex;
                log("Util: jump to slide " + slideIndex);
                if (Util.isInPresentationMode() || Util.isInLightTableMode()) {
                    changes["mode"] = "presentation";
                } else {
                    var slide = Util.bodyContent().querySelectorAll("section.slide")[slideIndex];
                    slide.scrollIntoView(true);
                }
                State.update(changes);
            }
        },
        /**
         * Returns a function that, when executed, scrolls the viewport so that the headline
         * is visible at the very top. In presentation mode switches to the slide that
         * succeeds the headline
         * @param {Number} headlineIndex the index (first headline = 0) of the headline to jump to
         * @memberof Util
         * @inner
         */
        jumpToHeadline: function (headlineIndex) {
            return function () {
                log("Util: jump to headline " + headlineIndex);
                if (Util.isInPresentationMode()) {
                    var slideIndex = -1;
                    var headlineCounter = -1;
                    var headlines = Util.bodyContent().querySelectorAll(
                        "#body-content>h1,#body-content>h2,#body-content>h3," +
                        "#body-content>h4,#body-content>h5,#body-content>h6," +
                        "#body-content > section.slide");
                    for (var i = 0; i < headlines.length; i++) {
                        if (!headlines[i].classList.contains("slide")) {
                            headlineCounter++;
                            if (headlineCounter === headlineIndex) {
                                Util.jumpToSlide(slideIndex + 1);
                                return;
                            }
                        } else {
                            slideIndex++;
                        }
                    }
                } else if (Util.isInNotesMode()) {
                    var headline = Util.bodyContent().querySelectorAll(
                        "#ldjs-lecture-notes")[0]
                        .querySelectorAll(
                            "#ldjs-lecture-notes div.ldjs-lecture-notes-subsection div.ldjs-lecture-notes-content>h1," +
                            "#ldjs-lecture-notes div.ldjs-lecture-notes-subsection div.ldjs-lecture-notes-content>h2," +
                            "#ldjs-lecture-notes div.ldjs-lecture-notes-subsection div.ldjs-lecture-notes-content>h3," +
                            "#ldjs-lecture-notes div.ldjs-lecture-notes-subsection div.ldjs-lecture-notes-content>h4," +
                            "#ldjs-lecture-notes div.ldjs-lecture-notes-subsection div.ldjs-lecture-notes-content>h5," +
                            "#ldjs-lecture-notes div.ldjs-lecture-notes-subsection div.ldjs-lecture-notes-content>h6"
                    )[headlineIndex];
                    headline.scrollIntoView(true);
                } else {
                    var headline = Util.bodyContent().querySelectorAll(
                        "#body-content>h1,#body-content>h2,#body-content>h3," +
                        "#body-content>h4,#body-content>h5,#body-content>h6")[
                        headlineIndex];
                    headline.scrollIntoView(true);
                }
                Menubar.highlightCurrentTableOfContent();
            }
        },
        /**
         * Adds an element with the slide's number to it.
         * @param {HTMLElement} slide the slide to add the number to
         * @param {Number} index the index of the slide (slide number = index + 1)
         * @memberof Util
         * @inner
         */
        addSlideNumber: function (slide, index) {
            var slideNumber = document.createElement("div");
            slideNumber.innerHTML = index + 1;
            slideNumber.className = "ldjs-slide-number";
            slide.querySelector(".section-body").appendChild(slideNumber);
        },
        /**
         * returns the last AnimationStep element shown on the current slide
         * return {Number} the last AnimationStep element currently shown on State.get("currentSlide")
         * @memberof Util
         * @inner
         */
        getCurrentAnimationStep: function () {
            if (State.get("currentAnimationStep") === undefined)
                return;
            var AnimationStep = Util.StateStringToArray(State.get("currentAnimationStep"));
            var currentAnimationStep = AnimationStep[State.get("currentSlide")];
            return currentAnimationStep;
        },
        /**
         * sets the last AnimationStep element shown on the current slide
         * @param {Number} new value for the AnimationStep elements on the current slide
         * @memberof Util
         * @inner
         */
        setCurrentAnimationStep: function (newValue) {
            if (State.get("currentAnimationStep") === undefined)
                return;
            var AnimationStep = Util.StateStringToArray(State.get("currentAnimationStep"));
            AnimationStep[State.get("currentSlide")] = newValue;
            State.update({
                currentAnimationStep: Util.arrayToStateString(AnimationStep),
            });
        },
        /**
         * shows the next AnimationStep element(s) shown on the current slide
         * @memberof Util
         * @inner
         */
        nextCurrentAnimationStep: function () {
            var next = Util.getCurrentAnimationStep() + 1;
            var slide = State.get("theSlides")[State.get("currentSlide")];
            while (slide.querySelectorAll("[data-animation-step=\"" + next + "\"]").length ===
                0 &&
                State.get("theAnimationStepElements")[State.get("currentSlide")] > next) {

                next++;
            }
            Util.setCurrentAnimationStep(next);
        },
        /**
         * hides the last AnimationStep element(s) shown on the current slide
         * @memberof Util
         * @inner
         */
        previousCurrentAnimationStep: function () {
            var previous = Util.getCurrentAnimationStep() - 1;
            var slide = State.get("theSlides")[State.get("currentSlide")];
            while (slide.querySelectorAll("[data-animation-step=\"" + previous + "\"]").length ===
                0 && previous >= 0) {
                previous--;
            }
            Util.setCurrentAnimationStep(previous);
        },
        remainingAnimationSteps: function () {
            var remaining = State.get("theAnimationStepElements")[State.get("currentSlide")] - Util.getCurrentAnimationStep();
            var empty = 0;
            var slide = State.get("theSlides")[State.get("currentSlide")];
            for (var i = Util.getCurrentAnimationStep() + 1; i < State.get("theAnimationStepElements")[State.get(
                "currentSlide")]; i++) {
                if (slide.querySelectorAll("[data-animation-step=\"" + i + "\"]").length === 0)
                    empty++;
            }
            return remaining - empty;
        },
        /**
         * Converts an number-array to a String. that can be saved and read in the State
         * @param {Array<Number>} array of number to be converted
         * @return {String} a string that contains the information given by the array
         * @see Util.StateStringToArray to convert it back
         * @memberof Util
         * @inner
         */
        arrayToStateString: function (array) {
            var limiter = ",";
            if (array.length === 0)
                return;
            var string = array[0];
            for (var i = 1; i < array.length; i++) {
                string += limiter;
                string += array[i];
            }
            return string;
        },
        /**
         * Converts a string back to an array of numbers
         * @param {String} a string that was previously converted by arrayToStateString
         * @return {Array<Number>}
         * @see Util.arrayToStateString to convert
         * @memberof Util
         * @inner
         */
        StateStringToArray: function (string) {
            var limiter = ",";
            var array = string.split(limiter).map(function (element, i) {
                return parseInt(element);
            });
            return array;
        },
        /**
         * Calculates the zoom factor necessary to display the current slide as
         * big as possible without cutting off anything and updates the State.
         * @memberof Util
         * @inner
         */
        fitPresentationSlideToPage: function () {
            log("Util: fit presentation slide to page");
            var windowWidth = window.innerWidth;
            var windowHeight = window.innerHeight;
            var factor = 1;
            if (!State.get("isEnhancedPresentationModeActive")) {
                var windowResolution = windowWidth / windowHeight;
                if (windowResolution >= State.get("slideAspectRatio")) {
                    factor = windowHeight / State.get("slideHeight");
                } else {
                    factor = windowWidth / State.get("slideWidth");
                }
            } else {
                State.update({
                    presentationZoomFactor: 0.9
                });
            }
            State.update({
                presentationZoomFactor: factor
            });
        },
        /**
         * Returns the page's scroll position. This is necessary because Firefox
         * scrolls the html node while Chrome scrolls the body node.
         * @return {Offset} the page's scroll position
         * @memberof Util
         * @inner
         */
        getScrollPosition: function () {
            return {
                left: document.body.scrollLeft || document.body.parentElement.scrollLeft,
                top: document.body.scrollTop || document.body.parentElement.scrollTop
            };
        },
        /**
         * Sets the page's scroll position.
         * @param {Number} top how many pixels to scroll to from the top
         * @param {Number} left how many pixels to scroll to from the left
         * @see Util.getScrollPosition regarding browser quirks
         * @memberof Util
         * @inner
         */
        setScrollPosition: function (top, left) {
            if (document.body.scrollLeft) {
                document.body.scrollLeft = left;
            } else if (document.body.parentElement.scrollLeft) {
                document.body.parentElement.scrollLeft = left;
            }
            if (document.body.scrollTop) {
                document.body.scrollTop = top;
            } else if (document.body.parentElement.scrollTop) {
                document.body.parentElement.scrollTop = top;
            }
        },
        /**
         * @typedef Offset
         * @type {Object}
         * @property {Number} top
         * @property {Number} left
         */
        /**
         * Computes the absolute pixel coordinates on the page of the given
         * element.<br> "Absolute" here refers to the css position attribute
         * "absolute", as in absolute with respect to the current page.
         * @param {HTMLElement} element the element
         * @return {Offset} the element's offset
         * @memberof Util
         * @inner
         */
        getOffset: function (element) {
            var left = 0;
            var top = 0;
            while (element && !isNaN(element.offsetLeft) && !isNaN(element.offsetTop)) {
                left += element.offsetLeft;
                top += element.offsetTop;
                element = element.offsetParent;
            }
            return {
                top: top,
                left: left
            };
        },
        /**
         * @typedef Box
         * @type {Object}
         * @property top
         * @property left
         * @property width
         * @property height
         */
        /**
         * Computes the given element's bounding box, consisting of its top left
         * corner as returned by getOffset as well as its width and height.
         * @param {HTMLElement} element the element for which to compute the
         * bounding box.
         * @return {Box}
         * @memberof Util
         * @inner
         */
        getBox: function (element) {
            var offset = Util.getOffset(element);
            return {
                top: offset.top,
                left: offset.left,
                height: element.offsetHeight,
                width: element.offsetWidth
            };
        },
        /**
         * Determines if a DOM element is (partially) visible in the current
         * viewport.
         * @param {HTMLElement} element the element to check
         * @return {Boolean} true if the element is (partially) visible in the
         * current viewport, false if it resides entirely outside of the current
         * viewport.
         * @see getBox
         * @see isBoxInViewport
         * @memberof Util
         * @inner
         */
        isElementInViewport: function (element) {
            return Util.isBoxInViewport(getBox(element));
        },
        /**
         * Determines if the given bounding box is (partially) visible in the
         * current viewport.
         * @param {Box} box the box to check
         * @return {Boolean} true if the element is (partially) visible in the
         * current viewport, false if it resides entirely outside of the current
         * viewport.
         * @see Util.getBox
         * @memberof Util
         * @inner
         */
        isBoxInViewport: function (box) {
            return (box.top < (window.pageYOffset + window.innerHeight) &&
                box.left < (window.pageXOffset + window.innerWidth) &&
                (box.top + box.height) > window.pageYOffset &&
                (box.left + box.width) > window.pageXOffset);
        },
        /**
         * Determines if the given child element is a child of the given parent
         * element. Designed to work on HTML elements, but can work with any
         * structure whose elements provide a <code>parentNode</code> field.<br>
         * @param {HTMLElement} parent the parent node
         * @param {HTMLElement} child the child node
         * @return {Boolean} true if child is below parent or parent == child
         * @memberof Util
         * @inner
         */
        isDescendant: function (parent, child) {
            var node = child;
            while (node != null) {
                if (node == parent) {
                    return true;
                }
                node = node.parentNode;
            }
            return false;
        },
        /**
         * @typedef Box
         * @type {Object}
         * @property scaleX
         * @property scaleY
         * @property translateX
         * @property
         */
        /**
         * Returns the scaling and translation parameter of an element.
         * @param {HTMLElement} element the element you are referring to.
         * @return {Transformation} the tranformation of the element.
         * @memberof Util
         * @inner
         */
        getTransformation: function (element) {
            var transformationObject = {
                scaleX: 1,
                scaleY: 1,
                translateX: 0,
                translateY: 0,
            };
            var style = window.getComputedStyle(element, null);
            var transform = style.getPropertyValue("transform") || style.getPropertyValue(
                "-webkit-transform");
            // matrix(X-SCALE, \d, \d, Y-SCALE, X-TRANSLATE, Y-TRANSLATE)
            var matrix = transform.match(
                /matrix\((\-?\d+\.?\d*), (\-?\d+\.?\d*), (\-?\d+\.?\d*), (\-?\d+\.?\d*), (\-?\d+\.?\d*), (\-?\d+\.?\d*)\)/
            );
            if (matrix !== null) {
                transformationObject.scaleX = matrix[1];
                transformationObject.scaleY = matrix[4];
                transformationObject.translateX = matrix[5];
                transformationObject.translateY = matrix[6];
            } else
                log("Couldn't match transformation:" + transform, debug_WARNING);
            return transformationObject;
        },
        /**
         * Returns the scaling of an element and all its parents up to the document node.
         * @param {HTMLElement} element the element you are referring to.
         * @return {Scaling} the scaling of the element.
         * @memberof Util
         * @inner
         */
        getScaling: function (element) {
            var scaling = {};
            scaling.scaleX = 1;
            scaling.scaleY = 1;
            while (element !== window.document) {
                var transform = Util.getTransformation(element);
                scaling.scaleX = scaling.scaleX * transform.scaleX;
                scaling.scaleY = scaling.scaleY * transform.scaleY;
                element = element.parentNode;
            }
            return scaling;
        },
        /**
         * Finds the index of the first (out of slide-)headline that is (partially) displayed in
         * the current viewport.
         * @return {Number} in Presentation Mode, this method simply returns
         * State.get("currentSlide").
         * In any other mode, it returns the index of the first headline from the
         * top left corner that is at least partially in the current viewport.
         * If, e.g. because of notes or slides between headlines, there are no headlines in the
         * current viewport, this method returns the index of the headline that
         * precedes the current viewport.
         * @memberof Util
         * @inner
         */
        indexOfCurrentHeadline: function () {
            if (Util.isInPresentationMode()) {
                return State.get("currentSlide");
            } else {
                var headlines = undefined;
                if (Util.isInNotesMode()) {
                    headlines = Util.bodyContent().querySelectorAll("#ldjs-lecture-notes")[
                        0]
                        .querySelectorAll(
                            "#ldjs-lecture-notes div.ldjs-lecture-notes-subsection div.ldjs-lecture-notes-content > h1," +
                            "#ldjs-lecture-notes div.ldjs-lecture-notes-subsection div.ldjs-lecture-notes-content > h2," +
                            "#ldjs-lecture-notes div.ldjs-lecture-notes-subsection div.ldjs-lecture-notes-content > h3," +
                            "#ldjs-lecture-notes div.ldjs-lecture-notes-subsection div.ldjs-lecture-notes-content > h4," +
                            "#ldjs-lecture-notes div.ldjs-lecture-notes-subsection div.ldjs-lecture-notes-content > h5," +
                            "#ldjs-lecture-notes div.ldjs-lecture-notes-subsection div.ldjs-lecture-notes-content > h6"
                    );
                } else {
                    headlines = Util.bodyContent().querySelectorAll(
                        "#body-content>h1,#body-content>h2,#body-content>h3," +
                        "#body-content>h4,#body-content>h5,#body-content>h6");
                }
                for (var i = headlines.length - 1; i >= 0; i--) {
                    var box = Util.getBox(headlines[i]);
                    if (Util.isHeadlineAboveTreshold(box)) {
                        return i;
                    }
                }
                log("indexOfCurrentHeadline: Could not be determined, returning -1",
                    debug_WARNING);
                return -1;
            }
        },
        /**
         * Returns true if the headline has bypassed a certain treshold and the viewport is
         * considered to be after the headline.
         * @param {Box} box the box to check
         * @return {boolean}
         * @see Util.getBox
         * @memberof Util
         * @inner
         */
        isHeadlineAboveTreshold: function (box) {
            var delta = window.innerHeight * (1 / 4);
            return (box.top < window.pageYOffset + delta);
        },
        /**
         * Finds the index of the first slide that is (partially) displayed in
         * the current viewport.
         * @return {Number} in Presentation Mode, this method simply returns
         * State.get("currentSlide").
         * In any other mode, it returns the index of the first slide from the
         * top left corner that is at least partially in the current viewport.
         * If, e.g. because of notes between slides, there are no slides in the
         * current viewport, this method returns the index of the slide that
         * precedes the current viewport.
         * @memberof Util
         * @inner
         */
        indexOfCurrentSlide: function () {
            if (State.get("mode") === "presentation") {
                return State.get("currentSlide");
            } else {
                var theSlides = Util.bodyContent().querySelectorAll("section.slide");
                for (var i = 0; i < theSlides.length; i++) {
                    var box = Util.getBox(theSlides[i]);
                    if (Util.isBoxInViewport(box)) {
                        return i;
                    } else if (box.top >= (window.pageYOffset + window.innerHeight)) {
                        return i - 1;
                    }
                }
                return -1;
            }
        },
        /**
         * Returns true if a state change should be allowed given the parameters.
         * @return {boolean} as described
         * @memberof Util
         * @inner
         */
        isAllowedStateChange: function (state, modifiedProperties) {
            // just allow if our title is set to "LectureDoc Beamer-Window"
            return (document.querySelector("title").textContent === "LectureDoc Beamer-Window");
        },
        /**
         * Gets the max id of client notes of a slide
         * @param {Number} slideIndex the index of the slide
         * @return {Number} If there are no client notes for the slide return 0.
         * If this is not the case return the maximum id of client notes.
         * @memberof Util
         * @inner
         */
        getMaxClientNoteIdOfSlide: function (slideIndex) {
            var ids = Util.getClientNotesOfSlide(slideIndex).map(function (note) {
                return note.id;
            });
            if (ids.length === 0) {
                return 0;
            }
            return Math.max.apply(Math, ids);
        },
        /**
         * Returns all client note objects which are associated with the slide at index slideIndex.
         * @param{Number} slideIndex which client notes we want to get.
         * @return{Array[Object]} all the client notes of the slide.
         * @memberof Util
         * @inner
         */
        getClientNotesOfSlide: function (slideIndex) {
            var slide = State.get("theSlides")[slideIndex];
            var notes = State.get("clientNotes");

            if (slide === undefined || notes === undefined) {
                return;
            }

            var hash = slide.getAttribute("data-hash");
            //structure of clientNotes {hash,slideNrl,title,id,text,x,y}
            //if hash was not found search after slideNr, if slideNr was not found either use title.
            //When this fails return nothing. The Notes that will not be found by this process are shown on a special slide.

            var slideNr = slide.getAttribute("data-nr");
            var title = slide.getAttribute("data-title");
            var results = notes.filter(function (item) {
                return item.hash === hash;
            });
            if (results.length !== 0) {
                return results;
            }
            var results = notes.filter(function (item) {
                return item.slideNr !== null;
            }).filter(function (item) {
                return item.slideNr === slideNr;
            });
            if (results.length !== 0) {
                return results;
            }
            var results = notes.filter(function (item) {
                return item.title !== null;
            }).filter(function (item) {
                return item.title === title;
            });
            if (results.length !== 0) {
                return results;
            }
            return [];
        },
        /**
         * Return the index of the slide where the client note is added to.
         * @param {Object} note you are referring to.
         * @return {Number} If there is no slide in the current State that the note is associated with return -1.
         * Otherwise return the index of the slide.
         * @memberof Util
         * @inner
         */
        getSlideIndexOfClientNote: function (note) {
            var slide = State.get("theSlides").map(function (item, index) {
                return [item, index];
            }).filter(function (item) {
                return item[0].getAttribute("data-hash") === note.hash;
            });
            if (slide.length !== 0) {
                return slide[0][1];
            }
            slide = State.get("theSlides").map(function (item, index) {
                return [item, index];
            }).filter(function (item) {
                return item[0].getAttribute("data-nr") === note.slideNr;
            });
            if (slide.length !== 0) {
                return slide[0][1];
            }
            slide = State.get("theSlides").map(function (item, index) {
                return [item, index];
            }).filter(function (item) {
                return item[0].getAttribute("data-title") === note.title;
            });
            if (slide.length !== 0) {
                return slide[0][1];
            }
            return -1;
        },
        /**
         * Gets the client note first searching with the attributes hash,slideNr,title and id.
         * @param {String} hash the hash value of the slide which the note was added to.
         * @param {String} slideNr the number which the slide was given via the md document.
         * @param {String} title the title which the slide was given via the md document.
         * @param {String} id the number which the note was given to after it was added to a slide.
         * @return {Object} If there is no such client note return undefined.
         * Otherwise search for hash then slideNr and at last for title. Return the first hit of the found id.
         * @memberof Util
         * @inner
         */
        getClientNote: function (hash, slideNr, title, id) {
            var notes = State.get("clientNotes").filter(function (item) {
                return item.hash === hash;
            });
            if (notes.length !== 0) {
                return notes.filter(function (item) {
                    return item.id == id;
                })[0];
            }
            notes = State.get("clientNotes").filter(function (item) {
                return item.slideNr == slideNr;
            });
            if (notes.length !== 0) {
                return notes.filter(function (item) {
                    return item.id == id;
                })[0];
            }
            notes = State.get("clientNotes").filter(function (item) {
                return item.title === title;
            });
            if (notes.length !== 0) {
                return notes.filter(function (item) {
                    return item.id == id;
                })[0];
            }
            return undefined;
        },
        /**
         * Returns all client note objects which cant be associated with a slide.
         * @return{Array[Object]} an array of notes.
         * @memberof Util
         * @inner
         */
        getLostClientNotes: function () {
            var hashOfSlides = State.get("theSlides").map(function (item) {
                return item.getAttribute("data-hash");
            });
            var numberOfSlide = State.get("theSlides").filter(function (item) {
                return item.getAttribute("data-nr") !== null;
            }).map(function (item) {
                return item.getAttribute("data-nr");
            });
            var titleOfSlides = State.get("theSlides").filter(function (item) {
                return item.getAttribute("data-title") !== null;
            }).map(function (item) {
                return item.getAttribute("data-title");
            });
            var results = State.get("clientNotes").filter(function (item) {
                return hashOfSlides.indexOf(item.hash) == -1 && numberOfSlide.indexOf(
                    item.slideNr) == -1 && titleOfSlides.indexOf(item.title) == -1;
            });
            return results;
        },
        /**
         * Returns the title of the slide.
         * @param {Number} slideIndex is the index of the slide.
         * @return {String}
         * @memberof Util
         * @inner
         */
        getTitleOfSlide: function (slideIndex) {
            var slide = State.get("theSlides")[slideIndex];
            if (slide === undefined) {
                return null;
            }
            return slide.getAttribute("data-title");
        },
        /**
         * Creates a new element with the given tag name and appends the passed
         * element as the new element's only child.
         * @param {HTMLElement} element the element to wrap
         * @param {String} wrapperTag the tag name of the element that will be
         * created
         * @memberof Util
         * @inner
         */
        wrapIn: function (element, wrapperTag) {
            var wrapperElement = document.createElement(wrapperTag);
            wrapperElement.appendChild(element);
            return wrapperElement;
        },
        /**
         * Shorthand to wrap the passed element in an "li" element.
         * @see Util.wrapIn
         * @memberof Util
         * @inner
         */
        wrapInLi: function (element) {
            return Util.wrapIn(element, "li");
        },
        /**
         * Shifts an element's scroll position by a certain amount of pixels.
         * @param {HTMLElement} element the element to scroll
         * @param {Number} amount the amount to scroll by (positive scrolls
         * toward right edge, negative toward left)
         * @memberof Util
         * @inner
         */
        scrollBy: function (element, amount) {
            element.scrollLeft += amount;
        },
        /**
         * Sets an element's scroll position to a certain position.
         * @param {HTMLElement} element the element whose scroll position will
         * be changed
         * @param {Number} where position to jump to
         * @memberof Util
         * @inner
         */
        scrollTo: function (element, where) {
            element.scrollLeft = where;
        },
        /**
         * Compares the size of the current slide and the window and, if the
         * slide is bigger than the window, activates scrollbars.
         * @memberof Util
         * @inner
         */
        manageScrollbars: function () {
            if (State.get("mode") !== "presentation") return;
            var slide = Util.bodyContent().querySelector("section.slide");
            if (slide === null) return;
            var isWider = slide.clientWidth > window.innerWidth + 1;
            var isHigher = slide.clientHeight > window.innerHeight + 1;
            if (isWider || isHigher) {
                document.body.classList.add("scroll");
            } else {
                document.body.classList.remove("scroll");
            }
        },
        /**
         * Adds a pair consisting of a dt and a dd element to the given dl.
         * @param {HTMLElement} definitionList the dl to add to
         * @param {String} term the definition term
         * @param {String} definition the definition text
         * @memberof Util
         * @inner
         */
        addDefinition: function (definitionList, term, definition) {
            var definitionTerm = document.createElement("dt");
            definitionTerm.innerHTML = term;
            definitionList.appendChild(definitionTerm);
            var definitionDescription = document.createElement("dd");
            definitionDescription.innerHTML = definition;
            definitionList.appendChild(definitionDescription);
        },
        /**
         * Constructs the LectureDoc help from the given helpData object and stores
         * it in the helpNode field.
         * @see LectureDoc.setHelp for the required fields of the helpData object
         * @memberof Util
         * @inner
         */
        buildHelpNode: function (helpData) {
            var helpNode = document.createElement("div");
            helpNode.id = "ldjs-help";

            var help = helpData.content;
            if (typeof help === "string") {
                var p = document.createElement("p");
                p.appendChild(document.createTextNode(help));
                helpNode.appendChild(p);
            } else {
                var definitionList = document.createElement("dl");
                for (var group in help) {
                    for (var key in help[group]) {
                        var definition = help[group][key].join(", ");
                        Util.addDefinition(definitionList, key, definition);
                    }
                    Util.addDefinition(definitionList, "", "&nbsp;");
                }
                helpNode.appendChild(definitionList);
            }
            var lastModifiedSpan = document.createElement("span");
            lastModifiedSpan.style.fontWeight = "bold";
            var helpFooter = document.createElement("footer");
            var p = document.createElement("p");
            p.id = "ldjs-help-last-modified-info";
            var lastModifiedDate = State.get("lastModified");
            if (lastModifiedDate instanceof Date) {
                p.appendChild(document.createTextNode(
                    "This document was last modified on "));
                p.appendChild(lastModifiedSpan);
                lastModifiedSpan.appendChild(document.createTextNode(lastModifiedDate.toLocaleString()));
            } else {
                p.appendChild(document.createTextNode("Modification date not available."));
            }
            helpFooter.appendChild(p);
            var footer = helpData.footer ||
                "LectureDoc © 2013, 2014 Michael Eichberg, Marco Jacobasch, Arne Lottmann, " +
                "Daniel Killer, Simone Wälde, Kerstin Reifschläger, " +
                "David Becker, Tobias Becker, Andre Pacak, Volkan Hacimüftüoglu";
            p = document.createElement('p');
            p.innerHTML = footer;
            helpFooter.appendChild(p);
            helpNode.appendChild(helpFooter);
            return helpNode;
        },
        /**
         * Converts an array-like structure (e.g. a NodeList) to an Array.
         * @param {Object} nodeList the structure to convert
         * @return {Array}
         * @memberof Util
         * @inner
         */
        toArray: function (nodeList) {
            return Array.prototype.slice.call(nodeList);
        },
        /**
         * Merges the *own* properties of two objects into a new object. If a
         * property is defined in both objects, the definition in the second
         * parameter overrides the one in the first.
         * @param {Object} anObject
         * @param {Object} anotherObject
         * @memberof Util
         * @inner
         */
        merge: function (anObject, anotherObject) {
            if (!anObject) return anotherObject;
            if (!anotherObject) return anObject;
            var merged = {};
            Object.keys(anObject).forEach(function (k) {
                merged[k] = anObject[k];
            });
            Object.keys(anotherObject).forEach(function (k) {
                if (k in merged) {
                    if (merged[k] === anotherObject[k]) {
                        log("Merge: overwriting (unchanged value):" +
                            debugString(k, merged[k], anotherObject[k]),
                            debug_TRACE);
                    } else {
                        log("Merge: overwriting (value changed): " +
                            debugString(k, merged[k], anotherObject[k]),
                            debug_WARNING);
                    }
                }
                merged[k] = anotherObject[k];
            });
            return merged;
        },
        /**
         * Creates the difference from two object, leaving every property from the first that
         * is not defined in the second object.
         * @param {Object} anObject
         * @param {Object} anotherObject
         * @memberof Util
         * @inner
         */
        difference: function (anObject, anotherObject) {
            if (!anotherObject) return anObject;
            if (!anObject) return undefined;
            var difference = {};
            Object.keys(anObject).forEach(function (k) {
                if (!(k in anotherObject))
                    difference[k] = anObject[k];
            });
            return difference;
        },
        /**
         * Shows a feedbackwindow in the middle of the browserwindow.
         * It will be shown for 600ms and then fades aways.
         * @param {String} text which text will be shown inside the feedbackwindow
         * @param {String} style which color the shown text has
         * @memberof Util
         * @inner
         */
        showFeedback: function (text, style) {
            var outer = Util.bodyContent().querySelector("#ldjs-keypress-feedback-outer");
            if (outer !== null) {
                outer.parentElement.removeChild(outer);
            }

            outer = document.createElement("div");
            outer.id = "ldjs-keypress-feedback-outer";
            Util.bodyContent().appendChild(outer);

            var feedback = document.createElement("div");
            feedback.id = "ldjs-keypress-feedback";
            outer.appendChild(feedback);

            var status = document.createElement("div");
            status.id = style;
            status.innerHTML = text;

            feedback.appendChild(status);
            setTimeout(function () {
                feedback.classList.add("fadeout");
                setTimeout(function () {
                    if (outer.parentElement !== null) {
                        outer.parentElement.removeChild(outer);
                    }
                }, 600);
            }, 600);
        }
    };

    /**
     * Functions that create the menu bar.
     * @namespace Menubar
     */
    var Menubar = {
        /**
         * The HTML element representing the menu bar.
         * @type {HTMLElement}
         * @memberof Menubar
         * @inner
         */
        element: undefined,
        iconsFolder: "Library/LectureDoc/MenuIcons/",
        /**
         * Determines the visibility of an HTML element based on the presence of
         * the class "visible".
         * @param {{String|HTMLElement}} [what] If left out, checks the menu
         * bar's visibility.
         * If String, checks for the element with that id.
         * @memberof Menubar
         * @inner
         */
        isVisible: function (what) {
            if (what === undefined) return State.get("isMenubarVisible");
            if (typeof what === "string") what = document.getElementById(what);
            if (what === null || typeof what !== "object" || !what.className) return false;
            return what.className.search(/\bvisible\b/) !== -1;
        },
        createModesSubmenu: function () {
            var builder = new Menubar.SubmenuBuilder("modes", "ul", "Modes");
            [
                Actions.PresentationModeAction,
                Actions.DocumentModeAction,
                Actions.NotesModeAction,
                Actions.LightTableModeAction,
                Actions.ContinuousModeAction
            ].forEach(function (action) {
                builder.addButton(Menubar.createTextButton(action));
            });
            return builder.element;
        },
        createTableOfContentsSubmenu: function () {
            var builder = new Menubar.SubmenuBuilder("table-of-contents", "ol",
                "Table of Contents");
            // get all headers that are not within a slide:
            var j = 0;
            // variable to determine the lowest depth found in the document
            // initializing with highest possible depth (h6)
            var lowestDepth = 6;
            var tableOfContentsList = State.get("theTableOfContents")
                .map(function (element, i) {
                    if (!element.classList.contains("slide")) {
                        var action = new ActionBuilder().setLabel(element.textContent)
                            .setExecute(Util.jumpToHeadline(j)).buildAction();
                        var button = Menubar.createTextButton(action);
                        var depth = parseInt(element.nodeName.charAt(1));
                        j++;
                        lowestDepth = (depth < lowestDepth) ? depth : lowestDepth;
                        return [button, depth];
                    } else {
                        return [-1, 0];
                    }
                }).filter(function (element) {
                    return element[0] !== -1;
                });
            builder.addNestedButtons(tableOfContentsList, lowestDepth);

            State.addListener(function (state, modifiedProperties) {
                Menubar.highlightCurrentTableOfContent();
            }, "currentSlide");
            return builder.element;
        },
        /**
         * Highlights the table of content entry that corresponds to the slide closest to the one
         * currently displayed.
         */
        highlightCurrentTableOfContent: function () {
            if (!Menubar.element) return;
            var highlightedNow = Menubar.element.querySelector(
                "#ldjs-menubar-submenu-table-of-contents .current");
            var i = Util.indexOfCurrentHeadline();
            log("Table of Contents: highlighting " + i);
            if (i == -1) return;
            var buttons = Util.toArray(Menubar.element.querySelectorAll(
                "#ldjs-menubar-submenu-table-of-contents a"));
            var highlightedNext = buttons[i];
            if (highlightedNext) {
                if (highlightedNext === highlightedNow)
                    return;
                if (highlightedNow)
                    highlightedNow.classList.remove("current");
                highlightedNext.classList.add("current");
            }
        },
        createKeySlidesSubmenu: function () {
            var builder = new Menubar.SubmenuBuilder("key-slides", "ol", "Key Slides");
            State.get("theSlides").map(function (slide, i) {
                return [slide, i];
            }).filter(function (slideWithIndex) {
                return !!slideWithIndex[0].dataset.title;
            }).forEach(function (slideWithIndex) {
                var slide = slideWithIndex[0];
                var i = slideWithIndex[1];
                var action = new ActionBuilder().setLabel(slide.dataset.title).
                setExecute(Util.jumpToSlide(i)).buildAction();
                var button = Menubar.createTextButton(action);
                button._index = i;
                builder.addButton(button);
            });
            State.addListener(function (state, modifiedProperties) {
                Menubar.highlightCurrentKeySlide(modifiedProperties.currentSlide.newValue);
            }, "currentSlide");
            return builder.element;
        },
        /**
         * Highlights the key slide entry that corresponds to the slide closest to the one
         * currently displayed.
         * @param {Number} currentSlideIndex the index of the currently displayed slide
         */
        highlightCurrentKeySlide: function (currentSlideIndex) {
            if (!Menubar.element) return;
            var highlightedNow = Menubar.element.querySelector(
                "#ldjs-menubar-submenu-key-slides .current");
            if (highlightedNow) highlightedNow.classList.remove("current");
            var theSlides = State.get("theSlides");
            if (currentSlideIndex < 0 || currentSlideIndex >= theSlides.length) return;
            var i = currentSlideIndex + 1;
            do {
                i--;
            } while (i >= 0 && !theSlides[i].dataset.title);
            if (i == -1) return;
            var buttons = Util.toArray(Menubar.element.querySelectorAll(
                "#ldjs-menubar-submenu-key-slides a"));
            var theButton = buttons.filter(function (button) {
                return button._index == i;
            })[0];
            if (theButton) theButton.classList.add("current");
        },
        createPresentationZoomSubmenu: function () {
            var builder = new Menubar.SubmenuBuilder("presentation-zoom", "ul", "Zoom");

            function zoom(factor) {
                return function () {
                    State.update({
                        presentationZoomFactor: factor
                    });
                };
            }
            var action = new ActionBuilder();
            [3, 2.5, 2, 1.5, 1, 0.75, 0.5].forEach(function (factor) {
                builder.addButton(Menubar.createTextButton(action.setExecute(zoom(
                        factor))
                    .setLabel(factor * 100 + "%").setActive(function () {
                        return State.get("presentationZoomFactor") === factor;
                    }).buildAction()));
            });
            builder.addButton(Menubar.createTextButton(action.setExecute(Util.fitPresentationSlideToPage)
                .setLabel("Fit to page").setActive(false).buildAction()));
            return builder.element;
        },
        createLightTableZoomSubmenu: function () {
            var builder = new Menubar.SubmenuBuilder("light-table-zoom", "ul", "Zoom");

            function zoom(factor) {
                return function () {
                    State.update({
                        lightTableZoomFactor: factor
                    });
                };
            }
            var action = new ActionBuilder();
            [100, 75, 50, 25, 10, 5, 2].forEach(function (factor) {
                builder.addButton(Menubar.createTextButton(action.setExecute(zoom(
                        factor / 100))
                    .setLabel(factor + "%").setActive(function () {
                        return State.get("lightTableZoomFactor") === factor /
                            100;
                    }).buildAction()));
            });
            return builder.element;
        },
        /**
         * @memberof Menubar
         * @inner
         * @namespace Menubar.createScrollingOverview
         */
        createScrollingOverview: function () {
            var overview = document.createElement("div");
            overview.id = "ldjs-menubar-submenu-scrolling-overview";
            overview.className = "ldjs-menubar-submenu";

            var theSlides = State.get("theSlides");
            theSlides.forEach(function (slide, i) {
                var clone = slide.cloneNode(true);
                clone._index = i;
                overview.appendChild(clone);
            });
            Events.onClick("#ldjs-menubar-submenu-scrolling-overview", function (event) {
                var x = overview.scrollLeft + event.clientX;
                var slide = Util.toArray(overview.childNodes).filter(function (
                    element) {
                    var left = element.offsetLeft;
                    var right = left + element.offsetWidth;
                    return left <= x && right >= x;
                })[0];
                if (isFinite(slide._index)) {
                    Util.jumpToSlide(slide._index)();
                }

            });
            var highlight = document.createElement("div");
            highlight.innerHTML = " ";
            highlight.id = "current-slide-highlight";
            overview.appendChild(highlight);

            var scrollIntervalHandler = undefined;
            var scrollInterval = 50; // milliseconds
            // speeds in pixels
            var minimumSpeed = 2;
            var maximumSpeed = 50;

            var leftRegion = 1 / 3;
            var rightRegion = 1 - leftRegion;

            /*
             * Scrolling speed is calculated with the inner edge of the scrolling
             * area mapped to minimumSpeed and the outer edge mapped to
             * maximumSpeed. The variables base and stretch are used in the
             * conversion; they are obtained as:
             * 1 - leftRegion * stretch + base = minimumSpeed
             * 1 * stretch + base = maximumSpeed
             */
            var base = (minimumSpeed - rightRegion * maximumSpeed) / leftRegion;
            var stretch = maximumSpeed - base;

            /**
             * Converts the given position ratio to a scroll amount.
             * @param {Number} ratio is expected to be between 0 (left edge) and
             * 1 (right edge)
             * @return the amount of pixels to scroll, regardless of direction
             * @memberof Menubar.createScrollingOverview
             * @inner
             */

            function ratioToAmount(ratio) {
                if (ratio < 0.5) ratio = 1 - ratio; // normalize to (1-leftRegion) .. 1
                return ratio * stretch + base;
            }

            /**
             * Internal state that stores the amount of pixels to scroll per step.
             * @memberof Menubar.createScrollingOverview
             * @inner
             */
            var scrollAmount = 0;

            /**
             * Terminates the scrolling interval registered under the
             * scrollIntervalHandler variable.
             * @memberof Menubar.createScrollingOverview
             * @inner
             */

            function stopScrolling() {
                if (scrollIntervalHandler !== undefined) {
                    clearInterval(scrollIntervalHandler);
                    scrollIntervalHandler = undefined;
                    scrollAmount = 0;
                }
            }

            /**
             * Repeatedly scrolls the given element by the specified amount; the
             * repetition interval is given in the variable scrollInterval.
             * Halts previous scrolling prior to registering its own scrolling
             * interval.
             * @param element the element (DOM element)
             * @param amount the amount to scroll ( < 0 to the left, > 0 to the
             * right )
             * @memberof Menubar.createScrollingOverview
             * @inner
             */

            function scrollRepeat(element, amount) {
                scrollAmount = amount;
                if (scrollIntervalHandler === undefined) {
                    scrollIntervalHandler = setInterval(function () {
                        Util.scrollBy(element, scrollAmount);
                    }, scrollInterval);
                }
            }

            /**
             * Internal state that stores the information if the Scrolling Overview is touched on the touchscreen.
             * @memberof Menubar.createScrollingOverview
             * @inner
             */
            var isPressed;

            /**
             * Represents the x-coordinate of the start of a touchgesture.
             * @memberof Menubar.createScrollingOverview
             * @inner
             */
            var startX;

            /**
             * Stores if the touchgesture is only a tap with the finger or a swipemotion.
             * @memberof Menubar.createScrollingOverview
             * @inner
             */
            var isScrolling;

            /**
             * The velocity of the finger moving over the scrolling overview.
             * @memberof Menubar.createScrollingOverview
             * @inner
             */
            var velocity;

            /**
             * The amplitude of the swipe.
             * @memberof Menubar.createScrollingOverview
             */
            var amplitude;

            /**
             * Last time the trackVelocity or the startTouch function was called.
             * @memberof Menubar.createScrollingOverview
             */
            var timestamp;

            /**
             * Handler for calculating the velocity.
             * @member of Menubar.createScrollingOverview
             */
            var velocityIntervalHandler;

            /**
             * Keeps track how many pixel it moved to calculate the velocity.
             * @member of Menubar.createScrollingOverview
             */
            var offset;
            /**
             * Is the evaluated function which we are trying to find with the differantial equation.
             * Determines how many pixel we have to move if it is still autoscrolling.
             * @member of Menubar.createScrollingOverview
             */
            var targetLeft;

            // The whole process of scrolling is modeled with the differential equation y' = y-a*e^(-t/tau)
            // You can look it up at overdamped springmass system.
            // tau is chosen as 325 to get a feeling like on iOS.
            // y is targetLeft
            // a is our amplitude which is calculated in respect to the velocity
            // t is deltaTime
            function startTouch(event) {
                if (event.touches.length != 1) {
                    return;
                }
                //set all variables up for tracking the velocity
                velocity = amplitude = 0;
                offset = overview.scrollLeft;
                timestamp = Date.now();
                clearInterval(velocityIntervalHandler);
                //check every 100ms
                velocityIntervalHandler = setInterval(trackVelocity, 100);

                isPressed = true;
                isScrolling = false;
                startX = event.touches[0].pageX;
                event.preventDefault();
                event.stopPropagation();
            }

            function moveTouch(event) {
                if (isPressed && event.touches.length == 1) {
                    isScrolling = true;
                    var delta = startX - event.touches[0].pageX;
                    //average finger 57px wide, 57/10 = 5.7 rounded up
                    if (delta > 6 || delta < -6) {
                        startX = event.touches[0].pageX;
                        Util.scrollBy(overview, delta);
                    }
                }
            }

            function trackVelocity() {
                var now, deltaTime, deltaX, v;
                now = Date.now();
                deltaTime = now - timestamp;
                timestamp = now;
                deltaX = overview.scrollLeft - offset;
                offset = overview.scrollLeft;
                v = 1000 * deltaX / (1 + deltaTime);
                velocity = 0.8 * v + 0.2 * velocity;
            }

            function changeOrientation(event) {
                startX = undefined;
                isPressed = false;
                isScrolling = false;
                clearInterval(velocityIntervalHandler);
                amplitude = velocity = offset = 0;
                timestamp = 0;
                targetLeft = 0;
            }

            function decreaseScrollSpeed() {
                var deltaTime, deltaX;
                if (amplitude) {
                    deltaTime = Date.now() - timestamp;
                    deltaX = -amplitude * Math.exp(-deltaTime / 325);
                    if (deltaX > 0.5 || deltaX < -0.5) { // more than a half pixel
                        Util.scrollTo(overview, targetLeft + deltaX);
                        requestAnimationFrame(decreaseScrollSpeed);
                    } else {
                        Util.scrollTo(overview, targetLeft);
                    }
                }
            }

            function endTouch(event) {
                isPressed = false;
                clearInterval(velocityIntervalHandler);
                //prevent scrolling if velocity is too low.
                if (velocity > 10 || velocity < -10) {
                    amplitude = 0.8 * velocity;
                    targetLeft = Math.round(overview.scrollLeft + amplitude);
                    timestamp = Date.now();
                    requestAnimationFrame(decreaseScrollSpeed);
                }
                //if it was only a tap
                if (!isScrolling) {
                    var x = overview.scrollLeft + startX;
                    var slide = Util.toArray(overview.childNodes).filter(function (
                        element) {
                        var left = element.offsetLeft;
                        var right = left + element.offsetWidth;
                        return left <= x && right >= x;
                    })[0];
                    if (isFinite(slide._index)) {
                        Util.jumpToSlide(slide._index)();
                    }
                    Menubar.hideAllSubmenus();
                }
                event.preventDefault();
                event.stopPropagation();
            }

            //checks if the device has a touchscreen.
            if ( !! ('ontouchstart' in window)) {
                overview.addEventListener('touchstart', startTouch);
                overview.addEventListener('touchmove', moveTouch);
                overview.addEventListener('touchend', endTouch);
                overview.addEventListener('orientationchange', changeOrientation);
            }

            function startScrolling(event) {
                log("Menubar: scrolling overview starts scrolling");
                var width = overview.clientWidth;
                var x = event.clientX;
                var ratio = x / width;
                var amount = ratioToAmount(ratio);
                if (ratio <= leftRegion) {
                    scrollRepeat(overview, -amount);
                } else if (ratio >= rightRegion) {
                    scrollRepeat(overview, amount);
                } else {
                    stopScrolling();
                }
            }
            overview.addEventListener("mousemove", startScrolling, false);
            overview.addEventListener("mouseout", stopScrolling, false);
            State.addListener(function (state, modifiedProperties) {
                var currentSlideIndex = modifiedProperties.currentSlide.newValue;
                Menubar.scrollOverviewToSlide(currentSlideIndex);
                var currentSlide = overview.querySelectorAll("section.slide")[
                    currentSlideIndex];
                var highlight = overview.querySelector("#current-slide-highlight");
                if (currentSlide) {
                    highlight.style.left = currentSlide.style.left;
                    highlight.style.display = "block";
                } else {
                    highlight.style.display = "none";
                }
            }, "currentSlide");

            return overview;
        },
        /**
         * Scrolls the overview so that the given slide is as close to the center
         * of the overview as possible.
         * @param {Number} slide the slide to put in the center
         * @memberof Menubar
         */
        scrollOverviewToSlide: function (slide) {
            var overview = document.getElementById(
                "ldjs-menubar-submenu-scrolling-overview");
            var overviewSlideWidth = overview.querySelector("section.slide").offsetWidth;
            var leftForCenter = -(overview.offsetWidth - overviewSlideWidth) / 2;
            var slideLeft = overviewSlideWidth * slide;
            var targetLeft = slideLeft + leftForCenter;
            Util.scrollTo(overview, targetLeft);
        },
        createCanvasSubmenu: function () {
            var builder = new Menubar.SubmenuBuilder("canvas", "ul", "Canvas");
            if ( !! ('ontouchstart' in window)) {
                builder.addButton(Menubar.createTextButton(new ActionBuilder(
                    "canvas-enable-mobile").setLabel(
                    function () {
                        if (!State.get("isTouchDrawingEnabled")) {
                            return "enable";
                        } else {
                            return "disable";
                        }
                    }).setEnabled(function () {
                    var canvas = Util.bodyContent().querySelector(
                        ".slide.current .ldjs-canvas");
                    if (!canvas) return;
                    var visible = canvas.classList.contains("visible");
                    var empty = canvas.querySelector("svg").childNodes.length ==
                        0;
                    return visible && !empty;
                }).setExecute(function () {
                    if (State.get("isTouchDrawingEnabled")) {
                        State.update({
                            isTouchDrawingEnabled: false
                        });
                    } else {
                        State.update({
                            isTouchDrawingEnabled: true
                        });
                    }
                }).buildAction()));
            }
            builder.addButton(Menubar.createTextButton(new ActionBuilder("canvas-hide").setLabel(
                function () {
                    var canvas = Util.bodyContent().querySelector(
                        ".slide.current .ldjs-canvas");
                    if (!canvas) return;
                    var visible = canvas.classList.contains("visible");
                    if (visible) {
                        return "hide";
                    } else {
                        return "show";
                    }
                }).setEnabled(function () {
                var canvas = Util.bodyContent().querySelector(
                    ".slide.current .ldjs-canvas");
                if (!canvas) return;
                var visible = canvas.classList.contains("visible");
                var empty = canvas.querySelector("svg").childNodes.length ==
                    0;
                return visible || !empty;
            }).setExecute(function () {
                var currentIndex = Util.indexOfCurrentSlide();
                var slides = Util.bodyContent().querySelectorAll(
                    "section.slide");
                var slide = slides[currentIndex];
                if (!slide) return;
                var canvas = slide.querySelector(".ldjs-canvas");
                var visible = canvas.classList.contains("visible");
                if (visible) {
                    State.update({
                        isTouchDrawingEnabled: false
                    });
                    canvas.classList.remove("visible");
                } else {
                    canvas.classList.add("visible");
                }
            }).buildAction()));
            builder.addButton(Menubar.createTextButton(new ActionBuilder("canvas-clear").setLabel(
                "clear").setEnabled(function () {
                return !!Util.bodyContent().querySelector(
                    ".slide.current .ldjs-canvas.visible");
            }).setExecute(function () {
                var svg = Util.bodyContent().querySelector(
                    ".slide.current .ldjs-canvas.visible svg");
                while (svg.hasChildNodes()) {
                    svg.removeChild(svg.firstChild);
                }
            }).buildAction()));
            ["black", "red", "yellow"].forEach(function (color) {
                builder.addButton(Menubar.createTextButton(new ActionBuilder(
                    "canvas-set-stroke-" + color).setLabel(
                    "<span style=\"color: " + color +
                    "; pointer-events: none;\">" + color + "</span>").setExecute(
                    function () {
                        var currentIndex = Util.indexOfCurrentSlide();
                        var slides = Util.bodyContent().querySelectorAll(
                            "section.slide");
                        var slide = slides[currentIndex];
                        if (!slide) return;
                        var canvas = slide.querySelector(".ldjs-canvas");
                        canvas.classList.add("visible");
                        canvas.dataset.strokeColor = color;
                        if ( !! ('ontouchstart' in window)) {
                            State.update({
                                isTouchDrawingEnabled: true
                            });
                        }
                        this.update();
                    }).setActive(function () {
                    var currentIndex = Util.indexOfCurrentSlide();
                    var slides = Util.bodyContent().querySelectorAll(
                        "section.slide");
                    var slide = slides[currentIndex];
                    if (!slide) return;
                    var canvas = slide.querySelector(
                        ".ldjs-canvas.visible");
                    return canvas && canvas.dataset.strokeColor === color;
                }).buildAction()));
            });
            return builder.element;
        },
        createClientNoteSubmenu: function () {
            var builder = new Menubar.SubmenuBuilder("client-notes", "ul", "Client Notes");
            builder.addButton(Menubar.createTextButton(Actions.CreateClientNoteAction));
            builder.addButton(Menubar.createTextButton(Actions.ToggleClientNotesVisibilityAction));
            builder.addButton(Menubar.createTextButton(Actions.ToggleLostClientNotesAction));
            return builder.element;
        },
        createOverlaySubmenu: function () {
            var builder = new Menubar.SubmenuBuilder("overlay", "ul", "Overlay");
            builder.addButton(Menubar.createTextButton(new ActionBuilder("white").setLabel(
                function () {
                    if (State.get("isOverlayActive") && State.get(
                        "overlayLayerColor") === "white") {
                        return "Return";
                    } else {
                        return "White";
                    }
                }).setExecute(Actions.WhiteoutAction.execute).buildAction()));
            builder.addButton(Menubar.createTextButton(new ActionBuilder("blackout").setLabel(
                function () {
                    if (State.get("isOverlayActive") && State.get(
                        "overlayLayerColor") === "black") {
                        return "Return";
                    } else {
                        return "Black";
                    }
                }).setExecute(Actions.BlackoutAction.execute).buildAction()));
            return builder.element;
        },
        createAnimationSubmenu: function () {
            var builder = new Menubar.SubmenuBuilder("animation", "ul", "Animation");
            builder.addButton(Menubar.createTextButton(new ActionBuilder(
                "toggle-slide-transitions").setLabel(function () {
                if (State.get("isTransitionEnabled")) {
                    return "Transitions" + " ✦";
                } else {
                    return "Transitions";
                }
            }).setExecute(
                Actions.ToggleTransitionsAction.execute).buildAction()));
            builder.addButton(Menubar.createTextButton(new ActionBuilder(
                "toggle-element-animation").setLabel(function () {
                if (!State.get("isAllVisible")) {
                    return "Element Animation" + " ✦";
                } else {
                    return "Element Animation";
                }
            }).setExecute(
                Actions.AnimationStepAction.execute).buildAction()));
            return builder.element;
        },
        createPresenterSubmenu: function () {
            var builder = new Menubar.SubmenuBuilder("presenter", "ul", "Presenter");
            [
                Actions.PresentationWindowAction,
                Actions.PresenterAction,
                Actions.ResetPresenterModeLayoutAction
            ].forEach(function (action) {
                builder.addDynamicButton(action);
            });
            return builder.element;
        },
        /**
         * Initializes the menu bar. After this method call, Menubar.element will
         * be an HTML nav element with id "ldjs-menubar".
         * @memberof Menubar
         */
        initialize: function () {
            Menubar.element = document.createElement("nav");
            Menubar.element.id = "ldjs-menubar";
            Menubar.element.className = "visible";

            var submenus = {
                asides: new Menubar.SubmenuBuilder("asides", "ul", "Asides").element,
                modes: Menubar.createModesSubmenu(),
                presentationZoom: Menubar.createPresentationZoomSubmenu(),
                lightTableZoom: Menubar.createLightTableZoomSubmenu(),
                scrollingOverview: Menubar.createScrollingOverview(),
                keySlides: Menubar.createKeySlidesSubmenu(),
                tableOfContents: Menubar.createTableOfContentsSubmenu(),
                canvas: Menubar.createCanvasSubmenu(),
                overlay: Menubar.createOverlaySubmenu(),
                animation: Menubar.createAnimationSubmenu(),
            };
            //add client note & presenter submenu action for devices with no touchscreen
            if (!('ontouchstart' in window)) {
                submenus["notes"] = Menubar.createClientNoteSubmenu();
                submenus["presenter"] = Menubar.createPresenterSubmenu();
            }

            //remove effects of the menubar on touchscreen devices
            //for better usability

            if ( !! ('ontouchstart' in window)) {

                Menubar.element.style.transition = "none";
                Menubar.element.style.webkitTransition = "none";
                Object.keys(submenus).forEach(function (name) {
                    submenus[name].style.transition = "none";
                    submenus[name].style.webkitTransition = "none";
                });
            }
            Actions.ToggleMenubarAction = new ActionBuilder("toggle-menubar").
            setEnabled(Util.isInPresentationMode).setIcon("").setExecute(function () {
                if (Menubar.element !== undefined) {
                    if (Menubar.isVisible()) {
                        State.update({
                            isMenubarVisible: false
                        });
                    } else {
                        State.update({
                            isMenubarVisible: true
                        });
                    }
                }
            }).setLabel(function () {
                if (Util.isInPresentationMode()) {
                    var slideNumber = String(State.get("currentSlide") + 1);
                    var isLastSlide = State.get("currentSlide") == State.get(
                        "theSlides").length;
                    var className = "ldjs-slide-number";
                    var isBlackout = State.get("isOverlayActive") && State.get(
                        "overlayLayerColor") === "black";
                    var isWhiteout = State.get("isOverlayActive") && State.get(
                        "overlayLayerColor") === "white";
                    if ((isBlackout || (isLastSlide && !isWhiteout)) && !Menubar.isVisible()) {
                        className += " dimmed";
                    }
                    return "<div class=\"" + className + "\"><p>" + slideNumber +
                        "</p></div>";
                }
                return "";
            }).buildAction();

            function getAsidesForCurrentSlide() {
                return State.get("slideAsides")[State.get("currentSlide")];
            }

            function isNotOnPresentationFinishedSlide() {
                return !Util.isInPresentationMode() || (State.get("currentSlide") < State
                    .get("theSlides").length);
            }

            /** @memberof Actions */
            Actions.ToggleAsidesAction = new ActionBuilder("toggle-submenu-asides").setIcon(
                "nav").
            setEnabled(function () {
                var asides = getAsidesForCurrentSlide();
                return Util.isInPresentationMode() && !(State.get("isMarionette")) && asides && asides.length >
                    0;
            }).setVisible(function () {
                if (!Util.isInPresentationMode()) return false;
                if (State.get("isMarionette")) return false;
                if (State.get("isOverlayActive") && !Menubar.isVisible()) return false;
                var asides = getAsidesForCurrentSlide();
                return Menubar.isVisible() || asides && asides.length > 0;
            }).setExecute(function (event) {
                if (!Menubar.isVisible(submenus.asides)) {
                    var buttons = submenus.asides.querySelector(
                        ".ldjs-menubar-submenu-buttons");
                    buttons.innerHTML = ""; // remove all children
                    getAsidesForCurrentSlide().forEach(function (link) {
                        buttons.appendChild(Util.wrapInLi(link));
                    });
                }
                Menubar.toggleSubmenu("asides")(event);
            }).setKeyStrokes(["a"]).buildAction();
            /** @memberof Actions */
            Actions.ToggleModesAction = new ActionBuilder("toggle-submenu-modes").setIcon(
                "modes").
            setKeyStrokes(["m"]).setExecute(Menubar.toggleSubmenu("modes")).setEnabled(
                function () {
                    return !(State.get("isMarionette")) && State.get("theSlides").length > 0;
                }).
            setVisible(function () {
                return !(State.get("isMarionette"))
            }).buildAction();
            /** @memberof Actions */
            Actions.TogglePresentationZoomAction = new ActionBuilder(
                "toggle-submenu-presentation-zoom").
            setIcon("zoom").setEnabled(isNotOnPresentationFinishedSlide).
            setVisible(Util.isInPresentationMode).
            setExecute(Menubar.toggleSubmenu("presentation-zoom")).buildAction();
            /** @memberof Actions */
            Actions.ToggleLightTableZoomAction = new ActionBuilder(
                "toggle-submenu-light-table-zoom").
            setIcon("zoom").setEnabled(Util.isInLightTableMode).setVisible(Util.isInLightTableMode)
                .setExecute(Menubar.toggleSubmenu("light-table-zoom")).buildAction();

            /**
             * @return true if key-slides and overview buttons should be visible.
             * @memberof Actions
             */

            function showSlideNavigationButtons() {
                var mode = State.get("mode");
                return !(mode === "lighttable" || mode === "continuous") && !(State.get("isMarionette"));
            }

            /** @memberof Actions */
            Actions.ToggleScrollingOverviewAction = new ActionBuilder(
                "toggle-submenu-scrolling-overview").
            setIcon("overview").setEnabled(function () {
                return showSlideNavigationButtons() && State.get("theSlides").length >
                    0;
            }).
            setVisible(showSlideNavigationButtons).setKeyStrokes(["o"]).
            setExecute(function (event) {
                var overview = submenus.scrollingOverview;
                var button = Menubar.element.querySelector(
                    "#ldjs-menubar-button-toggle-submenu-scrolling-overview");
                if (!Menubar.isVisible(overview)) {
                    var left = 0;
                    Util.toArray(overview.querySelectorAll("section.slide")).forEach(
                        function (slide) {
                            slide.style.left = left + "px";
                            left += slide.offsetWidth;
                        });
                    var currentSlideIndex = Util.indexOfCurrentSlide();
                    var currentSlide = overview.querySelectorAll("section.slide")[
                        currentSlideIndex];
                    var highlight = overview.querySelector("#current-slide-highlight");
                    if (currentSlide) {
                        highlight.style.left = currentSlide.style.left;
                        highlight.style.display = "block";
                    } else {
                        highlight.style.display = "none";
                    }
                    Menubar.scrollOverviewToSlide(currentSlideIndex);
                    Menubar.hideAllSubmenus();
                    State.update({
                        isMenubarVisible: true
                    });
                    button.classList.add("active");
                    Menubar.show("ldjs-menubar-submenu-scrolling-overview");
                } else {
                    button.classList.remove("active");
                    Menubar.hide("ldjs-menubar-submenu-scrolling-overview");
                    if (event.type.search("^key") === 0 && Util.isInPresentationMode()) {
                        State.update({
                            isMenubarVisible: false
                        });
                    }
                }
            }).buildAction();
            /** @memberof Actions */
            Actions.ToggleKeySlidesAction = new ActionBuilder(
                "toggle-submenu-key-slides").
            setIcon("key-slides").setEnabled(function () {
                return showSlideNavigationButtons() &&
                    State.get("theSlides").filter(function (slide) {
                        return !!slide.dataset.title;
                    }).length > 0;
            }).
            setVisible(showSlideNavigationButtons).setKeyStrokes(["k"]).
            setExecute(function (event) {
                var currentSlideIndex = Util.indexOfCurrentSlide();
                Menubar.highlightCurrentKeySlide(currentSlideIndex);
                Menubar.toggleSubmenu("key-slides")(event);
            }).buildAction();

            Actions.ToggleTableOfContentsAction = new ActionBuilder(
                "toggle-submenu-table-of-contents").
            setIcon("table-of-contents").setEnabled(function () {
                return showSlideNavigationButtons() &&
                    (State.get("mode") !== "presentation") &&
                    State.get("theTableOfContents").filter(function (element) {
                        return !element.classList.contains("slide");
                    }).length > 0;
            }).
            setVisible(function () {
                return showSlideNavigationButtons() && (State.get("mode") !==
                    "presentation")
            }).
            setKeyStrokes(["t"]).
            setExecute(function (event) {
                Menubar.highlightCurrentTableOfContent();
                Menubar.toggleSubmenu("table-of-contents")(event);
            }).buildAction();


            Actions.ToggleLecturerNotesAction = new ActionBuilder("toggle-lecturer-notes").
            setEnabled(Util.isInDocumentMode).
            setKeyStrokes(["x"]).
            setExecute(function () {
                Renderer.modes.document.toggleLecturerNotes();
            }).buildAction();

            Actions.ToggleClientNotesAction = new ActionBuilder(
                "toggle-submenu-client-notes").
            setEnabled(function () {
                //TODO: we need a better way to detect mobile devices, sadly the user agent can lie
                return !('ontouchstart' in window) && !(State.get("isMarionette")) && !Util.isInLightTableMode() &&
                    State.get("theSlides").length > 0 && State.get("currentSlide") <
                    State.get("theSlides").length;
            }).setVisible(function (event) {
                return !('ontouchstart' in window) && !(State.get("isMarionette")) && State.get("mode") !==
                    "lighttable";
            }).setIcon("clientnote").
            setExecute(function (event) {
                if (Menubar.isVisible("ldjs-menubar-submenu-modes") &&
                    event.type.search("^key") === 0) {
                    return;
                }
                Menubar.toggleSubmenu("client-notes")(event);
            }).setKeyStrokes(["n"]).buildAction();

            /**
             * Action to toggle the current slide's canvas.
             * @memberof Actions
             */
            Actions.CanvasAction = new ActionBuilder("toggle-submenu-canvas").setIcon(
                "canvas").
            setVisible(function () {
                return !(State.get("isMarionette")) && Util.isInPresentationMode();
            }).
            setKeyStrokes(["c"]).setEnabled(
                function () {
                    return Util.isInPresentationMode() && !(State.get("isMarionette")) &&
                        isNotOnPresentationFinishedSlide();
                }).setExecute(function (event) {
                if (Menubar.isVisible("ldjs-menubar-submenu-modes") &&
                    event.type.search("^key") === 0) {
                    return;
                }
                Menubar.toggleSubmenu("canvas")(event);
            }).setActive(function () {
                var currentIndex = Util.indexOfCurrentSlide();
                var slides = Util.bodyContent().querySelectorAll("section.slide");
                var slide = slides[currentIndex];
                return !!(slide && slide.querySelector(".ldjs-canvas.visible"));
            }).buildAction();
            /**
             * Action to toggle the submenu for the black and whiteout actions.
             * @memberof Actions
             */
            Actions.ToggleOverlayAction = new ActionBuilder("toggle-submenu-overlay").setIcon(
                "overlay").
            setVisible(function () {
                return !(State.get("isMarionette")) && Util.isInPresentationMode();
            }).
            setExecute(function (event) {
                Menubar.toggleSubmenu("overlay")(event);
            }).buildAction();

            /**
             * Action to toggle the submenu for the animations
             * like slide transitions and AnimationStep elements
             * @memberof Actions
             */
            Actions.ToggleAnimationAction = new ActionBuilder("toggle-submenu-animation")
                .setIcon("animation").
            setVisible(function () {
                return !('ontouchstart' in window) && !(State.get("isMarionette")) && Util.isInPresentationMode();
            }).
            setExecute(function (event) {
                Menubar.toggleSubmenu("animation")(event);
            }).buildAction();
            /**
             * Action to toggle the submenu for the presenter functions
             * like beamer window and enhanced presentation mode
             * @memberof Actions
             */
            Actions.TogglePresenterAction = new ActionBuilder("toggle-submenu-presenter")
                .setIcon("presentationwindow").
            setVisible(function () {
                return !('ontouchstart' in window) && !(State.get("isMarionette")) && Util.isInPresentationMode();
            }).
            setExecute(function (event) {
                Menubar.toggleSubmenu("presenter")(event);
            }).buildAction();

            var buttons = [
                Menubar.createIconButton(Actions.HelpAction, "left"),
                Menubar.createIconButton(Actions.PrintAction, "left"),
                Menubar.createSeparator("left"),
                Menubar.createTextButton(Actions.ToggleMenubarAction, "right"),
                Menubar.createIconButton(Actions.ToggleAsidesAction),
                Menubar.createSeparator("right"),
                Menubar.createIconButton(Actions.ToggleModesAction),
                Menubar.createIconButton(Actions.TogglePresentationZoomAction),
                Menubar.createIconButton(Actions.ToggleLightTableZoomAction),
                Menubar.createIconButton(Actions.ToggleScrollingOverviewAction),
                Menubar.createIconButton(Actions.ToggleKeySlidesAction),
                Menubar.createIconButton(Actions.ToggleTableOfContentsAction),
                Menubar.createIconButton(Actions.CanvasAction),
                Menubar.createIconButton(Actions.ToggleOverlayAction),
                Menubar.createSeparator("right"),
                Menubar.createIconButton(Actions.ToggleAnimationAction),
                Menubar.createIconButton(Actions.ToggleClientNotesAction),
                Menubar.createIconButton(Actions.TogglePresenterAction),
            ];
            buttons.concat(Object.keys(submenus).map(function (s) {
                return submenus[s];
            })).forEach(function (element) {
                Menubar.element.appendChild(element);
            });

            State.addListener(function (state, modifiedProperties) {
                if (modifiedProperties.isMenubarVisible.newValue) {
                    Menubar.show();
                } else {
                    Menubar.hideAllSubmenus();
                    Menubar.hide();
                }
            }, "isMenubarVisible");
            var menubarPresenter = document.createElement("div");
            menubarPresenter.id = "ldjs-menubar-presenter-text";
            menubarPresenter.style.position = "absolute";
            menubarPresenter.style.bottom = "5px";
            menubarPresenter.style.left = "35%";
            menubarPresenter.style.lineHeight = "40px";
            menubarPresenter.style.fontSize = "40px";
            menubarPresenter.textContent = "Presentation-Window";
            var updateMenubarPresenter = function () {
                if (State.get("isMarionette")) {
                    Menubar.element.classList.add("presentation-window");
                    Menubar.element.insertBefore(menubarPresenter, Menubar.element.firstChild);
                } else {
                    if (menubarPresenter.parentNode === Menubar.element)
                        Menubar.element.removeChild(menubarPresenter);
                    Menubar.element.classList.remove("presentation-window");
                }
            };
            updateMenubarPresenter();
            State.addListener(updateMenubarPresenter, "isMarionette");
            State.addListener(function (state, modifiedProperties) {
                if (State.get("mode") !== "presentation") return;
                var submenusToClose = ["asides", "presentation-zoom", "modes",
                    "canvas", "animation", "overlay", "client-notes", "presenter"];
                submenusToClose.forEach(function (id) {
                    var menu = Menubar.element.querySelector(
                        "#ldjs-menubar-submenu-" + id);
                    var button = Menubar.element.querySelector(
                        "#ldjs-menubar-button-toggle-submenu-" + id);
                    Menubar.hideSubmenu(menu, button);
                });
            }, "currentSlide");
            Events.onClick(".ldjs-menubar-button", function (event) {
                var button = event.target;
                if (button.action) {
                    button.action.execute(event);
                }
            });
            Events.onClick("#" + Menubar.element.id, function (event) {
                if (event.target.parentNode !== Menubar.element ||
                    event.target.id === "ldjs-menubar-submenu-scrolling-overview") {
                    Menubar.hideAllSubmenus();
                }
                if (event.target === Menubar.element) return;
                if (!Util.isInPresentationMode() || !Menubar.isVisible()) return;
                if (event.target.id === "ldjs-menubar-button-toggle-menubar") return;
                if (event.target.id.search(/-toggle-submenu-/) === -1) {
                    State.update({
                        isMenubarVisible: false
                    });
                }
            });
            Gator(window).on("resize", function () {
                var submenu = Menubar.element.querySelector(
                    ".ldjs-menubar-submenu.visible");
                if (!submenu) return;
                var buttonId = submenu.id.replace(/^(ldjs-menubar-)(submenu-.*)/,
                    "$1button-toggle-$2");
                var button = document.getElementById(buttonId);
                if (button) {
                    Menubar.centerHorizontallyOverElement(submenu, button);
                } else {
                    submenu.style.left = Math.max(0, parseInt(submenu.style.left)) +
                        "px";
                }
            });
            if (State.get("mode") === "presentation") {
                State.update({
                    isMenubarVisible: false
                });
            }
        },
        /**
         * Hides the given element by removing its "visible" class.
         * @param {{String|HTMLElement}} what If left undefined, hides the menu bar.
         * If String, hides the element with that id. Otherwise hides the element.
         * @memberof Menubar
         */
        hide: function (what) {
            if (what === undefined) {
                Menubar.element.classList.remove("visible");
            }
            if (typeof what === "string") what = document.getElementById(what);
            if (what === null || typeof what !== "object" || !what.classList) return;
            what.classList.remove("visible");
        },
        /**
         * Shows the given element by adding its "visible" class.
         * @param {{String|HTMLElement}} what If left undefined, shows the menu bar.
         * If String, shows the element with that id. Otherwise shows the element.
         * @memberof Menubar
         */
        show: function (what) {
            if (what === undefined) {
                Menubar.element.classList.add("visible");
            }
            if (typeof what === "string") what = document.getElementById(what);
            if (what === null || typeof what !== "object" || !what.classList) return;
            what.classList.add("visible");
        },
        /**
         * Creates a self-updating button from the given action with label
         * and/or icon. The button floats to the side specified with the float
         * parameter.
         * @param {Action} action the Action that controls this button
         * @param {String} [float=right] must be "left", "right", or undefined
         * @param {Boolean} [useIcon=false]
         * @param {Boolean} [useLabel=false]
         * @return {HTMLElement} an HTML a element representing the given Action
         * @memberof Menubar
         */
        createButton: function (action, float, useIcon, useLabel) {
            var button = document.createElement("a");
            button.className = "ldjs-menubar-button " + (float || "right");
            button.id = "ldjs-menubar-button-" + action.getId;
            if (!action.isEnabled) {
                button.classList.add("disabled");
            }
            if (!action.isVisible) {
                Menubar.hide(button);
            } else {
                Menubar.show(button);
            }
            if (action.isActive) {
                button.classList.add("active");
            }
            button.action = action;
            var properties = ["isVisible", "isEnabled", "getId", "isActive"];
            if (useLabel) {
                properties = properties.concat("getLabel");
                button.innerHTML = action.getLabel;
            }
            if (useIcon && action.getIcon) {
                properties = properties.concat("getIcon");
                button.classList.add("ldjs-menubar-icon-button");
                button.style.backgroundImage = 'url("' + Menubar.iconsFolder + action.getIcon +
                    '.svg")';
            }
            action.addListener(function (action, modifiedProperties) {
                if (modifiedProperties.isVisible) {
                    if (modifiedProperties.isVisible.newValue) {
                        Menubar.show(button);
                    } else {
                        Menubar.hide(button);
                    }
                }
                if (modifiedProperties.isEnabled) {
                    if (modifiedProperties.isEnabled.newValue) {
                        button.classList.remove("disabled");
                    } else {
                        button.classList.add("disabled");
                    }
                }
                if (modifiedProperties.isActive) {
                    if (modifiedProperties.isActive.newValue) {
                        button.classList.add("active");
                    } else {
                        button.classList.remove("active");
                    }
                }
                if (modifiedProperties.getId) {
                    button.id = modifiedProperties.getId.newValue;
                }
                if (useLabel && modifiedProperties.getLabel) {
                    button.innerHTML = modifiedProperties.getLabel.newValue;
                }
                if (useIcon && modifiedProperties.getIcon) {
                    if (modifiedProperties.getIcon.newValue) {
                        var path = Menubar.iconsFolder + modifiedProperties.getIcon.newValue;
                        button.style.backgroundImage = 'url("' + path + '.svg")';
                    } else {
                        button.style.backgroundImage = "";
                    }
                }
            }, properties);
            return button;
        },
        /**
         * Creates a self-updating button with an icon from the given action.
         * @see Menubar.createButton
         * @memberof Menubar
         */
        createIconButton: function (action, float) {
            return Menubar.createButton(action, float, true, false);
        },
        /**
         * Creates a self-updating button with a label from the given action.
         * @see Menubar.createButton
         * @memberof Menubar
         */
        createTextButton: function (action, float) {
            return Menubar.createButton(action, float, false, true);
        },
        /**
         * Creates a visual separator element that floats to the side specified
         * by the float parameter.
         * @param {String} float must be "left", "right", or undefined
         * @return {HTMLElement} a span with classes "separator" and float
         * @memberof Menubar
         */
        createSeparator: function (float) {
            var separator = document.createElement("span");
            separator.className = "separator " + (float || "right");
            return separator;
        },
        /**
         * Hides all children of the menubar with the class name "ldjs-menubar-submenu".
         * @see Menubar.hide
         * @memberof Menubar
         */
        hideAllSubmenus: function () {
            if (!Menubar.element) return;
            Util.toArray(Menubar.element.querySelectorAll(".ldjs-menubar-submenu")).forEach(
                function (submenu) {
                    // remove "ldjs-menubar-submenu" => length 20
                    var idPart = submenu.id.substring(20);
                    var button = Menubar.element.querySelector(
                        "#ldjs-menubar-button-toggle-submenu" + idPart);
                    Menubar.hideSubmenu(submenu, button);
                });
        },
        /**
         * Builder to create a list-based submenu. Elements are added via calls to
         * <pre>builder.addButton()</pre>
         * and the resulting submenu can be retrieved via
         * <pre>builder.element</pre>.
         * <br>
         * The element is an HTML ol element with class "ldjs-menubar-submenu" and id
         * "ldjs-menubar-submenu-" + the provided id parameter.
         * @param {String} id the submenus id fragment
         * @param {String} [type=ol] the type of list to create for this submenu (ul or ol)
         * @param {String} [header=""] the text to put at the top of the dialog (may contain html)
         * @class SubmenuBuilder
         * @memberof Menubar
         * @namespace SubmenuBuilder
         */
        SubmenuBuilder: function (id, type, header) {
            /**
             * The HTML list representing the submenu.
             * @type {HTMLElement}
             * @memberof Menubar.SubmenuBuilder
             */
            this.element = document.createElement("div");
            this.element.className = "ldjs-menubar-submenu";
            this.element.id = "ldjs-menubar-submenu-" + id;
            var headerElement = document.createElement("header");
            headerElement.innerHTML = header || "";
            this.element.appendChild(headerElement);
            var buttonList = document.createElement(type === "ul" ? type : "ol");
            buttonList.className = "ldjs-menubar-submenu-buttons";
            this.element.appendChild(buttonList);

            /**
             * @param {HTMLElement} button the button to add
             * @see Menubar.createButton
             * @see Menubar.createIconButton
             * @see Menubar.createTextButton
             * @memberof Menubar.SubmenuBuilder
             */
            this.addButton = function (button) {
                buttonList.appendChild(Util.wrapInLi(button));
                return this;
            };
            /**
             * creates a text button from the action, thats not rendered when the action is not visible
             * @param {Action} an action to create the button from
             * @see Menubar.createButton
             * @see Menubar.createIconButton
             * @see Menubar.createTextButton
             * @memberof Menubar.SubmenuBuilder
             */
            this.addDynamicButton = function (action) {
                var button = Menubar.createTextButton(action);
                var node = buttonList.appendChild(Util.wrapInLi(button));
                var update = function () {
                    if (action.isVisible)
                        node = buttonList.appendChild(node);
                    else
                        node = buttonList.removeChild(node);
                }
                action.addListener(update, "isVisible");
                update();
                return this;
            };
            /**
             * adds a list of buttons as an nested list to this submenu.
             * The inner array must contain the button on index 0 and a number indicating the hierarchy-level
             * on index 1
             * @param {Array<Array<HTMLElement|Number>>} buttonArray - an array of arrays, that contain an button
             *          and a hierarchy-number
             * @param {?Number} startDepth - the depth to start with (standard: 1)
             * @see Menubar.createButton
             * @see Menubar.createIconButton
             * @see Menubar.createTextButton
             * @memberof Menubar.SubmenuBuilder
             */
            this.addNestedButtons = function (buttonArray, startDepth) {
                buttonList.classList.add("nested");
                var currentList = buttonList;
                var currentDepth = (typeof startDepth === "undefined") ? 1 : startDepth;;
                buttonArray.forEach(function (buttonPair) {
                    var button = buttonPair[0];
                    var depth = buttonPair[1];

                    // we need to go deeper:
                    while (currentDepth < depth) {
                        var subList = document.createElement(type === "ul" ? type :
                            "ol");
                        subList.classList.add("nested");
                        currentList.appendChild(Util.wrapInLi(subList));
                        currentList = subList;
                        currentDepth++;
                    }
                    while (currentDepth > depth) {
                        currentList = currentList.parentNode.parentNode;
                        currentDepth--;
                    }
                    // now we can assume that currentDepth === depth
                    currentList.appendChild(Util.wrapInLi(button));
                    currentList.lastChild.classList.add("nested");
                });
            };
        },
        /**
         * Toggles the visibility of the submenu with the given id fragment.
         * <br>
         * The full id of the submenu and its toggle button are constructed from this fragment as
         * follows:
         * <dl>
         * <dt>submenu</dt><dd><code>"ldjs-menubar-submenu-" + id</code></dd>
         * <dt>button</dt><dd><code>"ldjs-menubar-button-toggle-submenu-" + id</code></dd>
         * </dl>
         * The submenu will be centered horizontally (as far as possible without cutting off parts)
         * over its toggle button. If either element cannot be found, the method fails silently.<br>
         * Showing a submenu this way will make the menu bar visible as well.<br>
         * Hiding a submenu this way will keep the menu bar visible.
         * @see Menubar.SubmenuBuilder
         * @see Menubar.centerHorizontallyOverElement
         * @memberof Menubar
         */
        toggleSubmenu: function (id) {
            return function (event) {
                var submenu = document.getElementById("ldjs-menubar-submenu-" + id);
                var button = document.getElementById(
                    "ldjs-menubar-button-toggle-submenu-" + id);
                if (!submenu || !button) return;
                if (Menubar.isVisible(submenu)) {
                    Menubar.hideSubmenu(submenu, button);
                    var isKeyEvent = event.type.search("^key") === 0;
                    if (Util.isInPresentationMode() && isKeyEvent) {
                        State.update({
                            isMenubarVisible: false
                        });
                    }
                } else {
                    if (id !== "asides" && !Menubar.isVisible()) {
                        State.update({
                            isMenubarVisible: true
                        });
                    }
                    Menubar.hideAllSubmenus();
                    Menubar.centerHorizontallyOverElement(submenu, button);
                    button.classList.add("active");
                    Menubar.show(submenu);
                }
            };
        },
        /**
         * Hides the given submenu and removes the "active" state from its button.
         * @param {HTMLElement} submenu the submenu to hide
         * @param {HTMLElement} button the button that toggles the submenu
         * @memberof Menubar
         */
        hideSubmenu: function (submenu, button) {
            Menubar.hide(submenu);
            if (button.action) {
                button.action.update();
                if (!button.action.isActive) {
                    button.classList.remove("active");
                }
            }
        },
        /**
         * Computes the value for the style attribute left that in conjunction with fixed
         * positioning will result in the menu being centered horizontally over the given
         * element.<br>
         * If the menu would overlap the right edge of the screen, it will be moved far enough
         * to the left so that it will be completely visible (given that its width is less
         * than the screen width).
         * @param {HTMLElement} menu the menu
         * @param {HTMLElement} element the element to center over
         * @memberof Menubar
         */
        centerHorizontallyOverElement: function (menu, element) {
            var left = Util.getOffset(element).left;
            left -= Math.ceil((menu.offsetWidth - element.offsetWidth) / 2);
            var right = left + menu.offsetWidth;
            var difference = Math.ceil(right - document.body.clientWidth + 15);
            if (difference > 0) {
                left -= difference;
            }
            menu.style.left = Math.max(0, left) + "px";
        }
    };

    /**
     * Provides an interface to read from and write to various storage locations.
     * <br>
     * At present, this includes the hash portion of the URL and the browser's local storage.
     * @namespace Storage
     */
    var Storage = {
        storageEnabled: true,
        /**
         * Internally used function to generate appropriate messages and logs
         * if local storage has been enabled or disabled since the last call.
         * @param {Boolean} stillEnabled true if access has been granted,
         * false otherwise.
         * @memberof Storage
         */
        storageAccessGranted: function (stillEnabled) {
            if (stillEnabled == Storage.storageEnabled) return;
            if (!stillEnabled) {
                log("Storage: local storage has been disabled", debug_ERROR);
                var msg = "<p>Your browser's local storage is disabled.</p>" +
                    "<p>LectureDoc stores some information such as the current presentation" +
                    " slide there. If you want to use LectureDoc's full persisting features," +
                    " please enable local storage and allow this site access to it.</p>"
                MessageBox.show("ldjs-storage-hint", "", "Info", msg, null, null, false);
            } else {
                log("Storage: local storage has been re-enabled", debug_ERROR);
            }
            Storage.storageEnabled = stillEnabled;
        },
        /**
         * @return {String} the key for this document's local storage access
         * @memberof Storage
         */
        getLocalStorageKey: function () {
            return "lecturedoc-state-" + window.location.pathname;
        },
        /**
         * Returns, if present, state previously stored in the browser's local storage.
         * @return {Object} a collection of properties to be applied to the State
         * @memberof Storage
         */
        readLocalStorage: function () {
            try {
                var result = JSON.parse(window.localStorage.getItem(
                    Storage.getLocalStorageKey()));
                Storage.storageAccessGranted(true);
                return result;
            } catch (e) {
                Storage.storageAccessGranted(false);
                return {};
            }
        },
        /**
         * Writes the applicationState properties from the given state object to the local storage.
         * <br>Typically the state object will be State, but could be any object that exposes
         * a getApplicationState method.
         * <br>
         * @param {Object} state the state object to read from
         * @memberof Storage
         */
        writeLocalStorage: function (state) {
            try {
                var key = Storage.getLocalStorageKey();
                var data = State.getApplicationState();
                window.localStorage.setItem(key, JSON.stringify(data));
                Storage.storageAccessGranted(true);
            } catch (e) {
                Storage.storageAccessGranted(false);
            }
        },
        /**
         * Returns properties from the hash portion of the URL.
         * <br>
         * The format for properties is the following:
         * <pre>property=value&property2=othervalue</pre>
         * @param {String} hash the hash to read from (typically window.location.hash)
         * @memberof Storage
         */
        readHash: function (hash) {
            log("Storage: reading url hash part");
            if (hash[0] === "#") hash = hash.substring(1);
            var pairs = hash.split("&").map(function (pair) {
                return pair.split("=");
            });
            var result = {};
            pairs.forEach(function (pair) {
                if (!pair || !pair.length || pair.length < 2) {
                    return;
                }
                if (isFinite(pair[1])) { // isFinite implies isNumber
                    pair[1] = parseFloat(pair[1]);
                }
                result[pair[0]] = pair[1];
            });
            return result;
        },
        /**
         * Creates a string that will be parsed correctly by {@link Storage.readHash}.
         * @param {Object} state the state object to read from (typically State)
         * @return {String} the hash string
         * @see Storage.readHash
         * @memberof Storage
         */
        buildHash: function (state) {
            log("Storage: building hash");
            var keys = ["mode"];
            switch (State.get("mode")) {
            case "presentation":
                keys = keys.concat(["currentSlide", "presentationZoomFactor",
                    "currentAnimationStep"]);
                break;
            case "lighttable":
                keys = keys.concat(["lightTableZoomFactor"]);
                break;
            }
            return keys.map(function (k) {
                return k + "=" + State.get(k);
            }).join("&");
        }
    };

    /**
     * @namespace Actions
     */
    var Actions = {

        /**
         * An array of callback function which are executed when initialize is called.
         * @memberof Actions
         */
        switchModeCallBackList: [],

        /**
         * Reads properties from local storage and URL and updates the state object.
         * @type Action
         * @memberof Actions
         */
        StateReader: new ActionBuilder().setExecute(function (state) {
            var local = Storage.readLocalStorage() || {};
            if (document.body.dataset.ldjsLastModified) {
                local["lastModified"] = new Date(Number(document.body.dataset.ldjsLastModified));
            }
            var hash = Storage.readHash(window.location.hash);
            State.update(Util.merge(local, hash));
        }).buildAction(),

        /**
         * Writes properties to the URL (hash) and local storage.
         * @type Action
         * @memberof Actions
         */
        StateWriter: new ActionBuilder().setExecute(function (state, changed) {
            window.location.hash = Storage.buildHash(state);
            Storage.writeLocalStorage(state);
        }).buildAction(),

        /**
         * Initialize other actions; should be called only after the document
         * has been analyzed and state has been fully initialized as some
         * actions depend on it.
         * @memberof Actions
         * @namespace initialize
         */
        initialize: function () {
            /**
             * Returns true if the slide navigation actions
             * (next/prev/jump/first/last) should be enabled.
             * @memberof Actions.initialize
             */

            function isNavigationActionEnabled() {
                return Util.isInPresentationMode();
            }

            var unRegister = [];

            function removeAllSlideListener() {
                unRegister.forEach(function (callBack) {
                    callBack();
                });
                unRegister = [];
            }
            Actions.CreateClientNoteAction = new ActionBuilder("create-client-note").setLabel(
                "create").
            setEnabled(function () {
                return !State.get("isLostClientNotesActive");
            }).setExecute(function () {
                //TODO: We need a possibility to add notes on a touchscreen device

                // remove listener from previous create-click
                removeAllSlideListener();
                var slides = Util.bodyContent().querySelectorAll("section.slide");
                var listenOnSlide = function (index) {
                    var slide = State.get("theSlides")[index];
                    var hash = slide.getAttribute("data-hash");
                    var notes = State.get("clientNotes");
                    var title = Util.getTitleOfSlide(index) || "null";
                    var slideNr = slide.getAttribute("data-nr");
                    var newId = Util.getMaxClientNoteIdOfSlide(index) + 1;

                    var element = slides[index].querySelector(".section-body");
                    var getPosition = function (event) {
                        var scrollPosition = Util.getScrollPosition();
                        if (Util.isInPresentationMode() && State.get("isEnhancedPresentationModeActive")) {
                            var transformation = Util.getTransformation(Util.bodyContent());
                            var scale = parseFloat(State.get(
                                "presentationZoomFactor")) ||
                                1;
                            var scale = parseFloat(State.get("presentationZoomFactor")) || 1;
                            var offSetLeft = ((window.innerWidth - 1024 * parseFloat(transformation.scaleX)) /
                                2);
                            if (scale < 1)
                                offSetLeft = offSetLeft + ((1024 * ((1 - scale) * parseFloat(
                                    transformation.scaleX))) / 2)

                            var x = event.clientX - offSetLeft;
                            var y = event.clientY;
                            x = x / (scale * parseFloat(transformation.scaleX)) + (slides[index].scrollLeft /
                                scale);
                            y = y / (scale * parseFloat(transformation.scaleY)) + (slides[index].scrollTop /
                                scale);
                        } else {
                            var transform = Util.getTransformation(element);
                            var x = (event.pageX - element.parentNode.offsetLeft) / transform
                                .scaleX;
                            var y = (event.pageY - element.parentNode.offsetTop) / transform.scaleY;
                        }
                        removeAllSlideListener();
                        notes.push({
                            hash: hash,
                            slideNr: slideNr,
                            title: title,
                            id: newId,
                            text: "",
                            x: x,
                            y: y
                        });
                        //reset posX and posY in widgets.
                        //if an element was not properly deleted and was moved it is possible that a new element with an old id
                        //has posX != 0 and posY != 0.
                        var widgets = State.get("widgets");
                        //if widgets contains an object
                        if (widgets !== "") {
                            var widgetsProperties = JSON.parse(State.get("widgets"));
                            var widgetId = "ldjs-client-note-" + hash + "-" + slideNr +
                                "-" + title.replace(" ", "_") + "-" + newId;
                            var properties = widgetsProperties[widgetId];
                            if (properties) {
                                properties.posX = 0;
                                properties.posY = 0;
                                properties.visible = "visible";
                                widgetsProperties[widgetId] = properties;
                            }
                            widgets = JSON.stringify(widgetsProperties);
                        }
                        State.update({
                            clientNotes: notes,
                            widgets: widgets,
                            areClientNotesVisible: true
                        });
                        Actions.StateWriter.execute();
                        ClientNotes.addNoteToSlide(State.get("clientNotes")[State.get(
                            "clientNotes").length - 1]);
                        if (Util.isInPresentationMode()) {
                            ClientNotes.show(State.get("currentSlide"));
                        } else {
                            for (var i = 0; i < State.get("theSlides").length; i++) {
                                ClientNotes.show(i);
                            }
                        }
                        event.stopPropagation();
                        event.preventDefault();
                    }
                    unRegister.push(function () {
                        element.removeEventListener("mousedown", getPosition, false);
                        element.classList.remove("ldjs-client-note-target");
                    });
                    element.classList.add("ldjs-client-note-target");
                    element.addEventListener("mousedown", getPosition, false);
                }
                if (State.get("mode") === "presentation") {
                    listenOnSlide(Util.indexOfCurrentSlide());
                } else {
                    var theSlides = Util.bodyContent().querySelectorAll("section.slide");
                    for (var i = 0; i < theSlides.length; i++) {
                        var box = Util.getBox(theSlides[i]);
                        if (Util.isBoxInViewport(box)) {
                            listenOnSlide(i);
                        }
                    }
                }

            }).buildAction();

            Actions.ToggleClientNotesVisibilityAction = new ActionBuilder(
                "visibility-client-note").
            setLabel(function (event) {
                if (State.get("areClientNotesVisible")) {
                    return "hide";
                } else {
                    return "show";
                }
            }).setEnabled(function () {
                return !State.get("isLostClientNotesActive");
            }).setExecute(function () {
                if (State.get("areClientNotesVisible")) {
                    for (var i = 0; i < State.get("theSlides").length; i++) {
                        ClientNotes.hide(i)
                    }
                    State.update({
                        areClientNotesVisible: false
                    });
                } else {
                    for (var i = 0; i < State.get("theSlides").length; i++) {
                        ClientNotes.show(i)
                    }
                    State.update({
                        areClientNotesVisible: true
                    });
                }
            }).buildAction();
            Actions.ToggleLostClientNotesAction = new ActionBuilder("lost-client-notes").
            setLabel(function (event) {
                if (State.get("isLostClientNotesActive")) {
                    return "hide lost notes";
                } else {
                    return "show lost notes";
                }
            }).
            setEnabled(function () {
                return (State.get("isLostClientNotesActive") || Util.getLostClientNotes().length != 0) && !
                    Util.isInLightTableMode();
            }).setExecute(function () {
                State.update({
                    isLostClientNotesActive: !State.get("isLostClientNotesActive")
                });
            }).buildAction();
            /**
             * Action to advance to the next animation-step item.
             * If all animation-step items are visible on the current slide advance to the next slide.
             * (Notice &lt;enter&gt; is being handled by {@link
             * Actions.JumpToSlideAction}.)
             * @memberof Actions
             */
            Actions.NextAnimationStepAction = new ActionBuilder("nextAnimationStepAction").
            setEnabled(isNavigationActionEnabled).setExecute(function (event, combo) {
                var entered = State.get("enteredSlideNumber");
                if ((combo == "enter" && document.querySelector(".ldjs-message-box")) ||
                    entered !== null)
                    return;
                // if not all AnimationStep elements are already shown:
                if (State.get("theAnimationStepElements")[State.get("currentSlide")] >
                    Util.getCurrentAnimationStep() && !State.get("isAllVisible")) {
                    Util.nextCurrentAnimationStep();
                } else {
                    State.update({
                        currentSlide: State.get("currentSlide") + 1
                    });
                }
                event.preventDefault();
            }).setKeyStrokes(["right", "down", "pagedown", "space", "enter"]).buildAction();
            /**
             * Action to hide the current animation-step item. If all animation-step items on the current slide are hidden
             * go to the previous slide.
             * @memberof Actions
             */
            Actions.PreviousAnimationStepAction = new ActionBuilder(
                "previousAnimationStepAction").
            setEnabled(isNavigationActionEnabled).setExecute(function (event, combo) {
                if (Util.getCurrentAnimationStep() >= 0 && !State.get("isAllVisible")) {
                    Util.previousCurrentAnimationStep();
                } else {
                    State.update({
                        currentSlide: State.get("currentSlide") - 1
                    });
                }
                event.preventDefault();
            }).setKeyStrokes(["left", "up", "pageup"]).buildAction();
            /**
             * Action to advance to the next slide.
             * (Notice &lt;enter&gt; is being handled by {@link
             * @memberof Actions
             */
            Actions.NextSlideAction = new ActionBuilder("nextSlide").
            setEnabled(isNavigationActionEnabled).setExecute(function (event, combo) {
                var entered = State.get("enteredSlideNumber");
                if ((combo == "enter" && document.querySelector(".ldjs-message-box")) ||
                    entered !== null)
                    return;
                State.update({
                    currentSlide: State.get("currentSlide") + 1
                });
            }).buildAction();
            /**
             * Action to go back one slide.
             * @memberof Actions
             */
            Actions.PreviousSlideAction = new ActionBuilder("previousSlide").
            setEnabled(isNavigationActionEnabled).setExecute(function (event, combo) {
                State.update({
                    currentSlide: State.get("currentSlide") - 1
                });
            }).buildAction();
            /**
             * Action to jump to the first slide.
             * @memberof Actions
             */
            Actions.FirstSlideAction = new ActionBuilder("firstSlide").
            setEnabled(isNavigationActionEnabled).setExecute(Util.jumpToSlide(0)).
            setKeyStrokes(["f"]).buildAction();
            /**
             * Action to jump to the last (user defined) slide.
             * @memberof Actions
             */
            Actions.LastSlideAction = new ActionBuilder("lastSlide").
            setEnabled(isNavigationActionEnabled).setExecute(function (event) {
                if (Menubar.isVisible("ldjs-menubar-submenu-modes") &&
                    event.type.search("^key") === 0) {
                    return;
                }
                Util.jumpToSlide(State.get("theSlides").length - 1)()
            }).setKeyStrokes(["l"]).buildAction();
            /**
             * Action to toggle the help dialog.
             * @memberof Actions
             */
            Actions.HelpAction = new ActionBuilder("help").setIcon("help").setEnabled(
                true).
            setVisible(true).setKeyStrokes(["h"]).setExecute(function () {
                MessageBox.show("ldjs-help", "", "Help", State.get("helpNode").innerHTML,
                    null, null, true);
            }).buildAction();

            Actions.AnimationStepAction = new ActionBuilder("invisble").setVisible(true)
                .setEnabled(Util.isInPresentationMode)
                .setKeyStrokes(["v"]).setExecute(function () {
                    State.update({
                        isAllVisible: !State.get("isAllVisible")
                    });
                }).buildAction();
            State.addListener(function (state, modifiedProperties) {
                var style = !modifiedProperties.isAllVisible.newValue ?
                    "positive-feedback" : "negative-feedback";
                var text = "Element Animation: " + (!modifiedProperties.isAllVisible.newValue ?
                    "ON" : "OFF");
                Util.showFeedback(text, style);
            }, "isAllVisible");
            /**
             * Action to enable/disable transitions in the presentation mode.
             * @memberof Actions
             */
            Actions.ToggleTransitionsAction = new ActionBuilder("enableTransitions").
            setEnabled(Util.isInPresentationMode).setExecute(function () {
                State.update({
                    isTransitionEnabled: !State.get("isTransitionEnabled")
                });
            }).setKeyStrokes(["z"]).buildAction();
            State.addListener(function (state, modifiedProperties) {
                var style = modifiedProperties.isTransitionEnabled.newValue ?
                    "positive-feedback" : "negative-feedback";
                var text = "Transitions: " + (modifiedProperties.isTransitionEnabled.newValue ?
                    "ON" : "OFF");
                Util.showFeedback(text, style);
            }, "isTransitionEnabled");
            /**
             * Creates an Action with the given id, label, and icon, that
             * toggles the overlay layer with the given color. If the layer is
             * active and has the given color, it will be hidden.
             * Otherwise it will be set to the given color and made visible. The
             * Action will only be visible and enabled in presentation mode.
             * @param {String} color the overlay's color
             * @param {String|Function} id {@link Action}
             * @param {String|Function} label {@link Action}
             * @param {String|Function} icon {@link Action}
             * @memberof Actions.initialize
             */

            function createOverlayAction(color, id, label, icon) {
                return new ActionBuilder(id).setEnabled(Util.isInPresentationMode).
                setLabel(label).setIcon(icon).setKeyStrokes([id[0]]).
                setVisible(Util.isInPresentationMode).setExecute(function () {
                    if (State.get("isOverlayActive") && State.get("overlayLayerColor") ===
                        color) {
                        State.update({
                            isOverlayActive: false
                        });
                    } else {
                        State.update({
                            overlayLayerColor: color,
                            isOverlayActive: true
                        });
                    }
                }).setActive(function () {
                    var result = State.get("isOverlayActive") && State.get(
                        "overlayLayerColor") === color;
                    return result;
                }).buildAction();
            }
            /**
             * @memberof Actions
             */
            Actions.BlackoutAction = createOverlayAction("black", "blackout", "Blackout",
                "blackout");
            /**
             * @memberof Actions
             */
            Actions.WhiteoutAction = createOverlayAction("white", "whiteout", "Whiteout",
                "whiteout");

            Actions.PresentationWindowAction = new ActionBuilder("presentationwindow").
            setEnabled(function () {
                return Util.isInPresentationMode() && (!('ontouchstart' in window));
            }).
            setLabel(function () {
                if (PresentationWindow.isShown()) {
                    return "Presentation Window" + " ✦";
                } else {
                    return "Presentation Window";
                }
            }).setKeyStrokes(["f9"]).
            setVisible(Util.isInPresentationMode).setExecute(function () {
                PresentationWindow.toggle();
            }).setActive(function () {
                return (Util.isInPresentationMode() && PresentationWindow.isShown());
            }).buildAction();

            Actions.PresenterAction = new ActionBuilder("presenter").
            setEnabled(function () {
                return Util.isInPresentationMode() && (!('ontouchstart' in window));
            }).
            setLabel(function () {
                if (State.get("isEnhancedPresentationModeActive")) {
                    return "Presenter Mode" + " ✦";
                } else {
                    return "Presenter Mode";
                }
            }).setKeyStrokes(["f8"]).
            setVisible(Util.isInPresentationMode).setExecute(function () {
                State.update({
                    isEnhancedPresentationModeActive: (!State.get("isEnhancedPresentationModeActive"))
                });
            }).setActive(function () {
                return (Util.isInPresentationMode() && State.get("isEnhancedPresentationModeActive"));
            }).buildAction();
            Actions.ResetPresenterModeLayoutAction = new ActionBuilder("resetpresentermodelayout").
            setEnabled(function () {
                var presentermodelayout = window.document.querySelector("#ldjs-presentermodelayout");
                if (!presentermodelayout)
                    return false;
                var widgets = presentermodelayout.querySelectorAll(".ldjs-widget");
                for (var i = 0; i < widgets.length; i++) {
                    var transformation = Util.getTransformation(widgets[i]);
                    if (transformation.translateY === undefined || transformation.translateX !== "0")
                        return true;
                    if (transformation.translateY === undefined || transformation.translateY !== "0")
                        return true;
                }
                return false;
            }).
            setLabel("Reset PresenterMode-Layout").
            setVisible(function () {
                return (Util.isInPresentationMode() && State.get("isEnhancedPresentationModeActive"));
            }).setExecute(function () {
                Widgets.resetPositions(window.document.querySelector("#ldjs-presentermodelayout"));
            }).setActive(false).buildAction();
            /**
             * Creates an action to switch to a render mode.
             * @param {String} mode the name of the mode
             * @param {String} label the label to use for the action
             * @return an Action that switches to the given mode
             * @memberof Actions.initialize
             */

            function switchModeAction(mode, label) {
                return new ActionBuilder(mode + "Mode").
                setLabel("<span style=\"text-decoration: underline\">" + label[0] +
                    "</span>" +
                    label.substring(1)).
                setExecute(function () {
                    Actions.switchModeCallBackList.forEach(function (callBack) {
                        callBack();
                    });
                    State.update({
                        mode: mode
                    });
                }).setActive(function () {
                    return State.get("mode") === mode;
                }).setEnabled(function () {
                    return State.get("theSlides").length > 0;
                }).buildAction();
            }

            /**
             * @memberof Actions
             */
            Actions.PresentationModeAction = switchModeAction("presentation",
                "Presentation");
            /**
             * @memberof Actions
             */
            Actions.DocumentModeAction = switchModeAction("document", "Document");
            /**
             * @memberof Actions
             */
            Actions.NotesModeAction = switchModeAction("notes", "Notes");
            /**
             * @memberof Actions
             */
            Actions.LightTableModeAction = switchModeAction("lighttable", "Light Table");
            /**
             * @memberof Actions
             */
            Actions.ContinuousModeAction = switchModeAction("continuous", "Continuous");

            Actions.PrintAction = new ActionBuilder("print").setIcon("print").
            setKeyStrokes(["mod+p"]).setExecute(function (event) {
                var memorizeVisible = State.get("isAllVisible");
                var restoreAnimationStep = function () {
                    if (Util.isInPresentationMode()) {
                        State.update({
                            isAllVisible: memorizeVisible
                        });
                    }
                };
                if (Util.isInPresentationMode()) {
                    State.update({
                        isAllVisible: true
                    });
                }
                var title = "Tips for printing in LectureDoc";
                var message = "";
                switch (State.get("mode")) {
                case "document":
                    message +=
                        "<p>Printing in this mode is only supported in <b>portrait</b>" +
                        " <span class=\"portrait\"></span> orientation.</p>";
                    break;
                case "continuous":
                    message +=
                        "<p>Printing in this mode is only supported in <b>landscape</b>" +
                        " <span class=\"landscape\"></span> orientation.</p>";
                    break;
                case "presentation":
                case "lighttable":
                case "notes":
                default:
                    message +=
                        "<p>Printing in this mode is currently <b>not supported</b>.</p>";
                    break;
                }
                message += "<ul>" +

                "<li>To print a <b>script-like document</b>, switch to <b>standard document mode</b>.</li>" +
                    "<li>To print <b>one slide</b> per page, e.g. for creating a PDF, switch to" +
                    "<b> continuous mode</b>.</li>" +
                    "</ul>";
                MessageBox.show("ldjs-print-dialog", "", title, message, {
                    getLabel: "Print",
                    execute: function () {
                        MessageBox.hide("ldjs-print-dialog");
                        window.print();
                        restoreAnimationStep();
                    }
                }, {
                    getLabel: "Abort",
                    execute: function () {
                        restoreAnimationStep();
                    }
                }, true);

                event.preventDefault();
                event.stopPropagation();
            }).buildAction();
        },
        /**
         * Returns the list of switchMode callback functions.
         * @memberof Actions
         */
        getSwitchModeCallBackList: function () {
            return Actions.switchModeCallBackList;
        },
        /**
         * @memberof Actions
         */
        bindKeys: function () {
            Gator(document).on("keyup", function (event) {
                var modes = document.querySelector("#ldjs-menubar-submenu-modes");
                if (!Menubar.isVisible(modes)) return;
                switch (String.fromCharCode(event.keyCode)) {
                case 'P':
                case 'p':
                    Menubar.toggleSubmenu("modes")(event);
                    Actions.PresentationModeAction.execute(event);
                    break;
                case 'D':
                case 'd':
                    Menubar.toggleSubmenu("modes")(event);
                    Actions.DocumentModeAction.execute(event);
                    break;
                case 'N':
                case 'n':
                    Menubar.toggleSubmenu("modes")(event);
                    Actions.NotesModeAction.execute(event);
                    break;
                case 'L':
                case 'l':
                    Menubar.toggleSubmenu("modes")(event);
                    Actions.LightTableModeAction.execute(event);
                    break;
                case 'C':
                case 'c':
                    Menubar.toggleSubmenu("modes")(event);
                    Actions.ContinuousModeAction.execute(event);
                    break;
                }
            });
            Object.keys(Actions).forEach(function (name) {
                Events.bindKeys(Actions[name]);
            });
        }
    };

    function analyzeDocument() {
        log("LectureDoc: analyzing document");
        var slides = initializeSlides();
        var changes = {
            "theAnimationStepElements": initializeAnimationStep(slides),
            "theSlides": slides,
            "theDocumentBody": cloneDocument(),
            "theTableOfContents": initializeTableOfContents(),
            "currentAnimationStep": initializeCurrentAnimationStep(slides.length)
        };
        changes = Util.merge(changes, getSlideMetrics(changes["theSlides"]));
        State.update(changes);
    };

    function initializeCurrentAnimationStep(slidelength) {
        var AnimationStep = new Array(slidelength);
        for (var i = 0; i < AnimationStep.length; i++) {
            AnimationStep[i] = -1;
        }
        return Util.arrayToStateString(AnimationStep);
    }

    function cloneDocument() {
        return document.body.cloneNode(true);
    };

    function initializeTableOfContents() {
        return Util.toArray(document.body.querySelectorAll(
            "#body-content>h1,#body-content>h2,#body-content>h3," +
            "#body-content>h4,#body-content>h5,#body-content>h6," +
            "#body-content > section.slide"
        ));
    }

    function initializeAnimationStep(slides) {
        var elements = {};
        log("Initialize: Animation-Step Elements:");
        slides.map(function (slide, i) {
            var animationStepOnSlide = Util.toArray(slide.querySelectorAll(
                "[data-animation-step]"));
            var max = -1;
            animationStepOnSlide.forEach(function (animationStep, j) {
                var order = parseInt(animationStep.getAttribute(
                    "data-animation-step"));
                order = (isFinite(order)) ? order : j; // order is NaN if data-animation-step has no or a illegal value
                animationStep.setAttribute("data-animation-step", order);
                max = (order > max) ? order : max;
            });
            elements[i] = max;
            log("Initialize: Found " + elements[i].length +
                " animation-step elements on slide " + i);
        });
        return elements;
    };

    function initializeSlides() {
        var slides = Util.toArray(document.body.querySelectorAll("section.slide"));
        addSlideNumbers(slides);
        log("Initialize: Found " + slides.length + " slides");
        return slides;
    };

    /**
     * Adds consecutive slide numbers to the given slides.
     * @param {Array<HTMLElement>} slides the slides to work on
     * @memberof LectureDoc
     */

    function addSlideNumbers(slides) {
        slides.forEach(Util.addSlideNumber);
    };

    /**
     * Retrieves width and height from the given slides and stores the values as
     * State.
     * @param {Array<HTMLElement>} slides the slides to analyze
     * @memberof LectureDoc
     */

    function getSlideMetrics(slides) {
        if (!slides || !Array.isArray(slides) || slides.length === 0) return;
        var aSlideBody = slides[0].querySelector(".section-body");
        return {
            slideWidth: aSlideBody.offsetWidth,
            slideHeight: aSlideBody.offsetHeight,
            slideAspectRatio: aSlideBody.offsetWidth / aSlideBody.offsetHeight
        };
    }

    function loadState() {
        log("LectureDoc: loading state");
        Actions.StateReader.execute(State); // load state
        Actions.StateWriter.execute(State); // update URL and local storage with new state
        var hash = window.location.hash;
        window.setInterval(function () { // periodically poll URL hash for changes
            if (hash != window.location.hash) {
                State.update(Storage.readHash(window.location.hash));
                Actions.StateWriter.execute(State);
            }
            hash = window.location.hash;
        }, 500);
        State.addListener(Actions.StateWriter.execute); // automatically store changes to State
    };

    function setupConstraints() {
        log("LectureDoc: initializing constraints");
        // constrain slide numbers to those that are actually available
        State.addConstraint("currentSlide", function (key, newValue, oldValue) {
            var theSlides = State.get("theSlides");
            return theSlides && Array.isArray(theSlides) && newValue >= 0 && newValue <=
                theSlides.length;
        }, function (key, newValue, oldValue) {
            log("Constraints: no slide #" + newValue + " available", debug_ERROR);
        });
        // constrain slide numbers: no change when a modal dialog is active
        State.addConstraint("currentSlide", function (key, newValue, oldValue) {
            return !State.get("isModalDialogActive");
        }, function (key, newValue, oldValue) {
            log(
                "Constraints: when there is a modal dialog, slide switching " +
                "is not allowed", debug_ERROR
            );
        });
        // constrain modes: only allow defined modes
        State.addConstraint("mode", function (key, newValue, oldValue) {
            return Renderer.modes.hasOwnProperty(newValue);
        }, function (key, newValue, oldValue) {
            log("Constraints: unknown mode " + newValue, debug_ERROR);
        });
        // constrain modes: without slides, only document mode is available
        State.addConstraint("mode", function (key, newValue, oldValue) {
            var theSlides = State.get("theSlides");
            return theSlides && Array.isArray(theSlides) && (theSlides.length > 0 ||
                newValue === "document");
        }, function (key, newValue, oldValue) {
            log(
                "Constraints: only document mode is available when there are no slides",
                debug_ERROR
            );
        });
        // constrain zoom factors: factor must be a real number > 0 (not infinity, nan, ...)

        function validateZoom(key, newValue, oldValue) {
            return isFinite(newValue) && newValue > 0;
        }

        function invalidZoom(key, newValue, oldValue) {
            log("Constraints: zoom factor " + newValue + " is invalid", debug_ERROR);
        }
        State.addConstraint("presentationZoomFactor", validateZoom, invalidZoom);
        State.addConstraint("lightTableZoomFactor", validateZoom, invalidZoom);
    };

    function startUI() {
        log("LectureDoc: initializing UI");
        Events.initialize();
        Actions.initialize();
        State.addListener(function (state, modifiedProperties) {
            Renderer.render(modifiedProperties.mode.newValue);
            State.update({
                isLostClientNotesActive: false
            });
            return {
                "isMenubarVisible": modifiedProperties.mode.newValue !==
                    "presentation"
            };
        }, "mode");
        State.addListener(function (state, modifiedProperties) {
            if (modifiedProperties.isOverlayActive) {
                if (modifiedProperties.isOverlayActive.newValue) {
                    var overlay = document.createElement("div");
                    overlay.id = "ldjs-overlay-layer";
                    overlay.className = State.get("overlayLayerColor") || "black";
                    if (State.get("isModalDialogActive")) {
                        overlay.classList.add("modal");
                    }
                    if (State.get("isEnhancedPresentationModeActive"))
                        Util.bodyContent().appendChild(overlay);
                    else
                        document.body.appendChild(overlay);
                } else {
                    var overlay = document.getElementById("ldjs-overlay-layer");
                    if (overlay) {
                        overlay.parentNode.removeChild(overlay);
                    }
                }
            }
            if (modifiedProperties.overlayLayerColor) {
                if (State.get("isOverlayActive")) {
                    var overlay = document.getElementById("ldjs-overlay-layer");
                    overlay.classList.remove(modifiedProperties.overlayLayerColor.oldValue);
                    overlay.classList.add(modifiedProperties.overlayLayerColor.newValue);
                }
            }
            if (modifiedProperties.isModalDialogActive && modifiedProperties.isModalDialogActive
                .newValue) {
                if (State.get("isOverlayActive")) {
                    var overlay = document.getElementById("ldjs-overlay-layer");
                    overlay.classList.add("modal");
                }
            }
            if (modifiedProperties.isLostClientNotesActive) {
                if (modifiedProperties.isLostClientNotesActive.newValue) {
                    var overlay = document.createElement("div");
                    overlay.id = "ldjs-overlay-layer";
                    overlay.className = "white";
                    var lostNotes = Util.getLostClientNotes();
                    lostNotes.forEach(function (note) {
                        ClientNotes.addNoteToElement(note, overlay);
                    });
                    Widgets.show(overlay);
                    if (State.get("isModalDialogActive")) {
                        overlay.classList.add("modal");
                    }

                    document.body.appendChild(overlay);
                } else {
                    var overlay = document.getElementById("ldjs-overlay-layer");
                    if (overlay) {
                        document.body.removeChild(overlay);
                    }
                }
            }
        }, ["isOverlayActive", "overlayLayerColor", "isModalDialogActive",
            "isLostClientNotesActive"]);
        State.addListener(function (state, modifiedProperties) {
            if (modifiedProperties.isMarionette.newValue) {
                // disable storage to prevent overwriting the "main"-window data
                Storage.storageEnabled = false;
                ClientNotes.hide(State.get("currentSlide"));
            }
        }, "isMarionette");
        State.addListener(function (state, modifiedProperties) {
            if (modifiedProperties.isModalDialogActive.newValue) {
                Events.blockKeyEvents();
            } else {
                Events.permitKeyEvents();
            }
        }, "isModalDialogActive");
        Renderer.initialize();
        Gator(window).on("resize", Util.manageScrollbars);
        Renderer.render(State.get("mode"));
        Menubar.initialize();
        document.body.appendChild(Menubar.element);
        Events.onClick('*', function (event) {
            var target = event.target;
            if (Util.isDescendant(Menubar.element, target)) return;
            var parent = target;
            while (parent) {
                if (parent.classList.contains("ldjs-message-box")) return;
                parent = parent.parentElement;
            }
            Menubar.hideAllSubmenus();
            if (Util.isInPresentationMode()) {
                State.update({
                    isMenubarVisible: false
                });
            }
        });
        Gator(document).on("keyup", function (event) {
            if (event.keyCode == 27) {
                var parent = event.target;
                while (parent) {
                    if (parent.classList.contains("ldjs-message-box")) return;
                    parent = parent.parentElement;
                }
                Menubar.hideAllSubmenus();
                if (Util.isInPresentationMode()) {
                    State.update({
                        isMenubarVisible: false
                    });
                }
            }
        });
        Actions.bindKeys();
        Events.bindKeys(Actions.PrintAction, "keydown");
    };
    /**
     * @namespace LectureDoc
     */
    var object = {
        init: function () {
            if (debug) {
                window.alert("LectureDoc is currently running in debug mode.");
            }
            log("LectureDoc: initializing LectureDoc");
            analyzeDocument();
            window.dispatchEvent(new Event('LDDocumentInitialized'));
            loadState();
            window.dispatchEvent(new Event('LDStateInitialized'));
            setupConstraints();
            window.dispatchEvent(new Event('LDConstraintsInitialized'));
            startUI();
            window.dispatchEvent(new Event('LDUIInitialized'));
            log("LectureDoc: initializing LectureDoc finished");
            window.dispatchEvent(new Event('LDInitialized'));

        },
        /**
         * @typedef HelpData
         * @type object
         * @property {Array<HelpGroup>} content the actual help content
         * @property {String} [footer=LectureDoc © 2013, 2014 Michael Eichberg,
            Marco Jacobasch, Arne Lottmann, Daniel Killer, Simone Wälde, Kerstin Reifschläger,
            David Becker, Tobias Becker, Andre Pacak, Volkan Hacimüftüoglu]
         * the help dialog's footer (may contain HTML). Will be inserted into the dialog's FOOTER element
         */
        /**
         * Properties are pairs of action description and keyboard shortcuts (as
         * an Array)
         * @typedef HelpGroup
         * @type object
         */
        /**
         * Sets LectureDoc's help message.
         * @param {HelpData} the data to show in the help dialog
         * @memberof LectureDoc
         */
        setHelp: function (helpData) {
            Gator(window).on("DOMContentLoaded", function () {
                State.update({
                    helpNode: Util.buildHelpNode(helpData)
                });
            });
        },
        /**
         * Registers a callback function which is executed when Renderer.render is called.
         * @param {Function} the callback function
         * @memberof LectureDoc
         */
        registerRenderCallBack: function (callBack) {
            if (typeof callBack == 'function') {
                Renderer.getRenderCallBackList().push(callBack);
            }
        },
        /**
         * Registers a callback function which is executed when Action.initialize.switchModeAction is called.
         * @param {Function} the callback function
         * @memberof LectureDoc
         */
        registerSwitchModeCallBack: function (callBack) {
            if (typeof callBack == 'function') {
                Actions.getSwitchModeCallBackList().push(callBack);
            }
        },
        /**
         * Updates the state if the changes are valid and this instance of LectureDoc accepts outside state changes
         * Note: Wait for LDStateInitialized-event otherwise updates may or may not will be overwritten while loading the state
         * SideEffect: Disables Storage
         * @see Util#isAllowedStateChange
         * @param {State} the new state
         * @param {ModifiedProperties} the changed properties
         * @memberof LectureDoc
         */
        updateState: function (state, modifiedProperties) {
            if (!Util.isAllowedStateChange(state, modifiedProperties))
                return;
            var changes;
            if (state === undefined) {
                changes = modifiedProperties;
            } else {
                // we ignore the modifiedProperties and enforce the whole state, in case we missed an update
                changes = Util.merge(State.getExchangeState(), state.getExchangeState());
            }
            State.update(changes);
        },
        /**
         * Returns the value of the named property from LectureDocs State.
         * @see State#get
         * @param key the property's name
         * @return the property's value (possibly undefined)
         * @memberof LectureDoc
         */
        get: State.get,
        /**
         * Sends data to this instance of LectureDoc.
         * Currently only used for PresentationWindow, rejects any other data
         * @see PresentationWindow
         * @param {String} an identifier
         * @param {Object} an object containing data
         * @memberof LectureDoc
         */
        sendData: function (ident, data) {
            PresentationWindow.acceptData(ident, data);
        },
    };
    return object;
}();

Gator(window).on("DOMContentLoaded", LectureDoc.init);