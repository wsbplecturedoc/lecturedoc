/*
 * (c) 2013 Michael Eichberg et al.
 * https://bitbucket.org/delors/lecturedoc
 * 
 * See LICENSE and Actuarius-LICENSE for license details. 
 */
package de

import scala.language.reflectiveCalls

import java.io.IOException
import java.io.InputStream
import scala.reflect.ClassTag

package object lecturedoc {

    /**
      * Example usage:
      * {{{
      * read(new java.io.DataInputStream(System.in))((din) => din.readDouble())
      * }}}
      */
    @throws[IOException]
    def read[I <: InputStream, T](f: ⇒ I)(r: I ⇒ T): T = {
        val in = f // this calls the function f
        try {
            r(in)
        }
        finally {
            if (in != null)
                in.close
        }
    }

    /**
      * @param r The resource. `r.close` is called, after executing `f(r)`.
      */
    @throws[IOException]
    def withResource[I <: { def close(): Unit } /* <=> java.io.Closeable*/ , O](r: I)(f: r.type ⇒ O): O = {
        try {
            f(r)
        }
        finally {
            r.close
        }
    }

    def onCatch[E <: Throwable: ClassTag, T](f: ⇒ T)(fEx: ⇒ T): T = {
        try {
            f
        }
        catch {
            case e: E ⇒ fEx /*swallow*/
        }
    }

}