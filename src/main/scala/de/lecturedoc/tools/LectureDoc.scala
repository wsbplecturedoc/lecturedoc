/*
 * (c) 2013 Michael Eichberg et al.
 * https://bitbucket.org/delors/lecturedoc
 *
 * See LICENSE and Actuarius-LICENSE for license details.
 */
package de.lecturedoc
package tools

import java.nio.file.{ Path, Paths}
import java.util.Date

import de.lecturedoc.parser.DefaultLectureDocTransformer

/**
  * Contains a main method that simply reads everything from stdin, parses it as LectureDoc and
  * prints the result to stdout.
  */
object LectureDoc {

    /**
      * The help message. Loaded from the file "LectureDocHelp.txt" when needed.
      */
    private lazy val helpMessage =
        onCatch[Exception, String](
            withResource(this.getClass().getClassLoader().getResourceAsStream("LectureDocHelp.txt"))(
                scala.io.Source.fromInputStream(_).mkString
            )
        )("Help file not found. Please, go to http://bitbucket.org/delors/lecturedoc to report this error.")

    /**
      * Prints the help message and exits LectureDoc with the return code "-1" to indicate that
      * LectureDoc was not able to make sense out of the specified command line arguments.
      */
    private def printUsageAndExit() {
        println(helpMessage)
        System.exit(-1)
    }

    /**
      * Prints the given error message followed by the general help message. Afterwards, LectureDoc returns
      * with the return code "-2" to indicate that LectureDoc was not able to make sense out of the
      * specified command line arguments or was able to read the specified file.
      */
    private def printErrorAndExit(errorMessage: String) {
        System.err.println(errorMessage)
        println(helpMessage)
        System.exit(-2)
    }

    def main(args: Array[String]): Unit = {
        if (args.length > 3) printUsageAndExit();

        val complete = args.contains("--complete")
        val continuous = args.contains("--continuous")

        if (continuous) {
            // continuous mode.
            // I.e., we watch a directory for changes and create the new LectureDoc files when needed

            val path: Path =
                if (complete && args.length == 3) { Paths.get(args(2)); }
                else if (!complete && args.length == 2) { Paths.get(args(1)); }
                else { Paths.get(System.getProperty("user.dir")) }
            watchDirectory(path,complete)
        } else {
            //standard mode
            if (args.length > 1) printErrorAndExit("Unsupported arguments: "+args.mkString(" "))
            process(
                    new java.io.File(System.getProperty("user.dir")),
                    System.in,
                    DefaultLectureDocTransformer,
                    System.out,
                    complete,
                    new Date() /*last-modified*/)
        }
    }

}
