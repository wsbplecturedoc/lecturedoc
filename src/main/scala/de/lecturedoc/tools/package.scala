/*
 * (c) 2013 Michael Eichberg et al.
 * https://bitbucket.org/delors/lecturedoc
 *
 * See LICENSE and Actuarius-LICENSE for license details.
 */
package de.lecturedoc

import java.io.{ InputStream, OutputStream, PrintStream, File }
import java.nio.file.{FileSystems, Files, Path, Paths, WatchKey}
import java.nio.file.StandardWatchEventKinds.{ENTRY_CREATE, OVERFLOW, ENTRY_MODIFY}
import java.util.Date
import java.text.DateFormat
import scala.collection.JavaConversions._

import de.lecturedoc.parser.DefaultLectureDocTransformer
import parser.Transformer

package object tools {

    private val thisClassLoader = this.getClass().getClassLoader()

    private def getHTMLFragment(baseDir: File, name: String): String = {
        Option(thisClassLoader.getResourceAsStream("Library/"+name+".fragment.html")).
            map(in ⇒ { withResource(scala.io.Source.fromInputStream(in))(_.mkString) }).
            getOrElse("<!-- No "+name+" fragment found (\"Library/"+name+".fragment.html\"). -->")
    }


    /**
      * The help message. Loaded from the file "LectureDocHelp.txt" when needed.
      */
    lazy val helpMessage =
        onCatch[Exception, String](
            withResource(thisClassLoader.getResourceAsStream("LectureDocHelp.txt"))(
                scala.io.Source.fromInputStream(_).mkString
            )
        )("Help file not found. Please, go to http://bitbucket.org/delors/lecturedoc to report this error.")

    /**
      * Prints the help message and exits LectureDoc with the return code "-1" to indicate that
      * LectureDoc was not able to make sense out of the specified command line arguments.
      */
    def printUsageAndExit() {
        println(helpMessage)
        System.exit(-1)
    }

    /**
      * Prints the given error message followed by the general help message. Afterwards, LectureDoc returns
      * with the return code "-2" to indicate that LectureDoc was not able to make sense out of the
      * specified command line arguments or was able to read the specified file.
      */
    def printErrorAndExit(errorMessage: String) {
        System.err.println(errorMessage)
        println(helpMessage)
        System.exit(-2)
    }
    /**
      * Reads a LectureDoc document from the given `InputStream` and writes the generated HTML5 file to the
      * `OutputStream`.
      *
      * Neither the InputStream nor the OutputStream is closed.
      *
      * @param complete If true, a complete HTML document is created; otherwise a fragment is created.
      * @param lastModified The date when the underlying `.md` file was last modified.
      */
    def process(
        baseDir: File,
        in: InputStream,
        t: Transformer,
        out: OutputStream,
        complete: Boolean,
        lastModified: Date) {

        val printStream = new java.io.PrintStream(out, true, "UTF-8")
        import printStream.{ println, print }

        val input: String = scala.io.Source.fromInputStream(in)(scala.io.Codec.UTF8).mkString

        try {
            //run that string through the transformer trait's apply method
            val output = t.apply(input)

            if (complete) {
                // TODO [Refine] replace the hard coded HTML using a template language (e.g., velocity)
                // TODO [Refine] make it possible to add additional header fragments
                // TODO [Refine] make it possible to add additional footer fragments
                println("<!DOCTYPE html><html><head><meta charset='utf-8'><meta name=generator content='LectureDoc'><meta name='viewport' content='width=1024, user-scalable=yes, initial-scale=1.0'>")
                println(getHTMLFragment(baseDir, "header"))
                println("</head><body data-ldjs-last-modified=\""+lastModified.getTime()+"\">")
                println("<div id='body-content'>")
                print(output)
                println("</div>")
                println(getHTMLFragment(baseDir, "footer"))
                println("</body></html>")
            }
            else {
                print(output)
            }
        }
        catch {
            case e: Exception ⇒ {
                println("Conversion failed: "+e.getLocalizedMessage())
                System.err.println("Conversion failed (see generated file for details!)")
            }
        }
    }
    /**
     * Notices the changes of files in the directory at the path 'path' and converts the changed file into an html file.
     *
     * @param complete If true, a complete HTML document is created; otherwise a fragment is created.
     */
    def watchDirectory(path: Path ,complete: Boolean){
            if (!(Files.isWritable(path) && Files.isDirectory(path))) printUsageAndExit()

            System.out.println("Watching directory: "+path.toAbsolutePath()+" for changes.")

            val watchService = FileSystems.getDefault().newWatchService();
            path.register(watchService, ENTRY_CREATE, ENTRY_MODIFY);
            var watchKey: WatchKey = null
            do {
                watchKey = try { watchService.take() } catch { case e: InterruptedException => return }
                for {
                    event <- watchKey.pollEvents()
                    if event.kind() ne OVERFLOW
                    file = path.resolve(event.context().asInstanceOf[Path])
                    fileName = file.toString
                    if fileName.endsWith(".md")
                } {
                    // Convert the updated/new lecture doc file
                    val lastModified = Files.getLastModifiedTime(file).toMillis()
                    val date = new java.util.Date(lastModified)
                    System.out.println("Converting file: "+fileName+" last modified: "+DateFormat.getDateTimeInstance().format(date))
                    val newFileName = fileName.substring(0, fileName.length() - 3)+".html"
                    val newFile = Paths.get(newFileName)
                    Files.deleteIfExists(newFile);

                    var in: InputStream = null
                    var out: OutputStream = null

                    try {
                        in = Files.newInputStream(file)
                        out = Files.newOutputStream(newFile)
                        process(path.toFile(),in, DefaultLectureDocTransformer, out, complete, date)
                        Files.setLastModifiedTime(newFile, java.nio.file.attribute.FileTime.fromMillis(lastModified))
                    }
                    catch {
                        case e: java.io.IOException =>
                            System.err.println("Error while converting: "+file+" => "+newFile+" ("+e.getMessage()+")")
                    }
                    finally {
                        if (in != null)
                            in.close()
                        if (out != null)
                            out.close();
                    }
                }
            } while (watchKey.reset())

            System.err.println("Watching the path: "+path+" was aborted.")
    }
}
