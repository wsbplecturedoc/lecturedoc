/*
 * (c) 2013 Michael Eichberg et al.
 * https://bitbucket.org/delors/lecturedoc
 *
 * See LICENSE and Actuarius-LICENSE for license details.
 */
package de.lecturedoc
package tools

import java.io.{ PrintStream, OutputStream }
import java.nio.file.{ Files, Path, Paths}
import java.util.Date
import javafx.application.{ Application, Platform }

import javafx.concurrent.Task
import javafx.event.{ ActionEvent, EventHandler}
import javafx.geometry.Insets

import javafx.scene.Scene
import javafx.scene.text.Font
import javafx.scene.control.{ Button, Label, RadioButton, ScrollPane, ToggleGroup}
import javafx.scene.control.ScrollPane.ScrollBarPolicy
import javafx.scene.image.{ Image, ImageView}
import javafx.scene.layout.{ GridPane, StackPane}

import javafx.stage.{ DirectoryChooser, FileChooser, Stage, WindowEvent}
import javafx.stage.FileChooser.ExtensionFilter

import de.lecturedoc.parser.DefaultLectureDocTransformer

/**
 * Printer for the messages of the parser
 */
class MessagePrintStream(output: OutputStream, label: Label) extends PrintStream(output){

    override def println(text: String){
        Platform.runLater(new Runnable(){
            override def run(){
                label.setText(label.getText() + text + "\n")
            }
        })
    }
}
/**
* A class for the GUI of LectureDoc.
*/
class Launcher extends Application{

    override def start(primaryStage: Stage){
        primaryStage.setTitle("LectureDoc Launcher")
        primaryStage.setResizable(false)
        val grid = new GridPane()
        grid.setHgap(10)
        grid.setVgap(10)
        grid.setPadding(new Insets(25,25,25,25))
        val root = new StackPane()
        root.getChildren().add(grid)
        val scene = new Scene(root,700,250)
        scene.getStylesheets().add("css/caspian.css");

        val logoIS = this.getClass().getClassLoader().getResourceAsStream("images/logo.png")
        val imageView = new ImageView(new Image(logoIS))
        val lectureDocLogo = new Label("",imageView)
        grid.add(lectureDocLogo, 0, 2, 4, 2);

        val option = new ToggleGroup()

        val noneRB = new RadioButton()
        noneRB.setText("None")
        noneRB.setUserData(false)
        noneRB.setToggleGroup(option)
        noneRB.setSelected(true)
        val completeRB = new RadioButton()
        completeRB.setText("Complete")
        completeRB.setUserData(true)
        completeRB.setToggleGroup(option)
        grid.add(noneRB,0,4,4,2)
        grid.add(completeRB,0,6,4,2)
        val convertFileBtn = new Button("Convert .md file")
        convertFileBtn.setMinWidth(200)
        grid.add(convertFileBtn,0,8,4,2)
        val feedbackSP = new ScrollPane()
        feedbackSP.setMinWidth(425)
        grid.add(feedbackSP,5,0,5,12)
        feedbackSP.setVbarPolicy(ScrollBarPolicy.AS_NEEDED)
        feedbackSP.setHbarPolicy(ScrollBarPolicy.NEVER)
        val feedbackLabel = new Label()
        feedbackLabel.setWrapText(true)
        feedbackLabel.setFont(Font.font("Verdana",10))
        feedbackSP.setContent(feedbackLabel)
        feedbackSP.setFitToWidth(true)

        val out = new MessagePrintStream(System.out,feedbackLabel)
        System.setOut(out)
        System.setErr(new MessagePrintStream(System.err,feedbackLabel))

        var isWatchingDirectory = false

        convertFileBtn.setOnAction(new EventHandler[ActionEvent](){
            override def handle(e: ActionEvent){
                if(!isWatchingDirectory){
                    val complete = option.getSelectedToggle().getUserData().asInstanceOf[Boolean]

                    val chooser = new FileChooser()
                    chooser.getExtensionFilters().add(new ExtensionFilter("Markdown FILES (*.md)","*.md"))
                    val file = chooser.showOpenDialog(primaryStage)
                    if(file != null){
                        val path = file.getAbsolutePath()
                        val inputFile =  Paths.get(path)
                        val outputFile = Paths.get(path.replace(".md",".html"))
                        Files.deleteIfExists(outputFile)
                        process(new java.io.File(System.getProperty("user.dir")),
                                Files.newInputStream(inputFile),
                                DefaultLectureDocTransformer,
                                Files.newOutputStream(outputFile),
                                complete,
                                new Date())
                        out.println("Converted file: "+file.getName())
                    }
                }
            }
        })
        val watchDirectoryBtn = new Button("Watch directory")
        watchDirectoryBtn.setMinWidth(200)
        grid.add(watchDirectoryBtn,0,10,4,2)
        var pTask:Option[Task[Unit]] = None
        watchDirectoryBtn.setOnAction(new EventHandler[ActionEvent](){
            override def handle(e: ActionEvent){
                if(isWatchingDirectory){
                    watchDirectoryBtn.setText("Watch directory")
                    out.println("Stop watching directory")
                    isWatchingDirectory = false
                    if(pTask.isDefined){
                        pTask.get.cancel()
                    }
                } else {
                    val chooser = new DirectoryChooser()
                    val file = chooser.showDialog(primaryStage)
                    val complete = option.getSelectedToggle().getUserData().asInstanceOf[Boolean]
                    if(file != null){
                        val path = file.getAbsolutePath()
                        pTask = Some(new Task[Unit](){
                            override def call(){
                                watchDirectory(Paths.get(path),complete)
                            }
                        })
                        val pThread = new Thread(pTask.get)
                        pThread.setDaemon(true)
                        pThread.start()
                        watchDirectoryBtn.setText("Stop")
                        isWatchingDirectory = true
                    }
                }
            }
        })
        primaryStage.setOnCloseRequest(new EventHandler[WindowEvent](){
            override def handle(event: WindowEvent){
                System.exit(0)
            }
        })
        primaryStage.setScene(scene)
        primaryStage.show()
    }
}

/**
 * Entrypoint for the GUI version of the LectureDoc-parser
 */
object Launcher {
    def main(args: Array[String]): Unit = {
        //starts the UI
        Application.launch(classOf[Launcher], args: _*)
    }
}
