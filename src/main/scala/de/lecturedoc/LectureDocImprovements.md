#Changes/Additions when compared with Actuarius 0.2.4

 * If the language of a fenced code block (\`\`\`LANGUAGE) is specified that information is stored in the code's class attribute (``<code class=LANGUAGE>...``). Three backticks (\`\`\`) are also used to end the code block
 * Added support for specifying sections using +~[TITLE]CLASS{DataAttributes} as the syntax for 
 specifying the section and class; ~+ ends a block; the TITLE, CLASS and DATAATTRIBUTES is optional. If the class identifies one of the HTML5 section types (e.g., aside,nav,footer,header,...) than the corresponding HTML tag is used instead of the section tag which is used otherwise.
 * Sections can be nested.
 * Added support for multi-file documents resembling the syntax used by Marked.app. I.e., a file's 
 content can be included in a document using the syntax ``<<[FILENAME]``. The other file's
 content will then seamlessly be included in the document. After that, the whole document is parsed and marked up.
 * By default the input is expected to be UTF-8 encoded and the output will also be UTF-8 encoded
 * We can create complete HTML documents.